<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cFirma.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oObject   	= new Firma($db);
// set values
if(($_POST['nombre'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
    die();
}

try{
    $oObject->nombre			= $_POST['nombre'];
    $oObject->estado    		= $_POST['estado'];

    if($_POST['opt']=='nfirma'){
        if(($_FILES['logoFirma']['name'])==''){
            echo json_encode(array("title" => "Alerta", "text" => "Seleccione un Logo", "type" => "warning"),JSON_UNESCAPED_UNICODE);
            die();
        }
        $imgFile 	= $_FILES['logoFirma']['name'];
        $tmp_dir 	= $_FILES['logoFirma']['tmp_name'];
        $imgSize 	= $_FILES['logoFirma']['size'];

        $upload_dir 		= '../IMG/'; // upload directory
        $imgExt 			= strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
        // // valid image extensions

        $valid_extensions 	= array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        // // rename uploading image
        $logo  		 	= "logofirma_" . time() . "." . $imgExt;
        // // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){
            // // Check file size '5MB'
            if($imgSize < 5000000)				{
                move_uploaded_file($tmp_dir,$upload_dir.$logo);
                $oObject->logofirma =  "IMG/".$logo;
            }	else {
                echo json_encode(array("title" => "Alerta", "text" => "Imagen muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
                die();
            }
        }	else {
            echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imágenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
            die();
        }
        $r = $oObject->create_firma();
        if($r){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Firma creada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }

    }elseif($_POST['opt']=='mfirma'){
        $oObject->idfirmas   = $_POST['id'];

        if($_FILES['logoFirma']['name']){
            $imgFile = $_FILES['logoFirma']['name'];
            $tmp_dir = $_FILES['logoFirma']['tmp_name'];
            $imgSize = $_FILES['logoFirma']['size'];
            $upload_dir = '../IMG/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            // valid image extensions

            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $img = "logofirma_" . time() . "." . $imgExt;
            // allow valid image file formats
            if(in_array($imgExt, $valid_extensions)){
                // Check file size '5MB'
                if($imgSize < 5000000)				{
                    move_uploaded_file($tmp_dir,$upload_dir.$img);
                }	else {
                    echo json_encode(array("title" => "Alerta", "text" => "Imagen muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
                    die();
                }
            }	else {
                echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imágenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
                die();
            }
            if(!isset($errMSG)) {
                $oObject->logofirma = "IMG/".$img;
                //unlink("../".$_POST['oldimg']);
            } else {
                echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
                die();
            }
        }else{
            $oObject->logofirma = $_POST['oldimg'];
        }
        if($oObject->modify_firma()){
            echo json_encode(array("title" => "Operación realizada", "text" => "Examen modificado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }
    }
}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}