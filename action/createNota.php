<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cNotaria.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oNotaria        	= new Notaria($db);
// set values
if(($_POST['nombre'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Escriba un nombre", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}
if(($_POST['detalle'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Escriba un detalle", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

if(($_POST['latitud'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese una latitud", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

if(($_POST['longitud'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese una longitud", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

try{
    $oNotaria->nombre	 		= $_POST['nombre'];
    $oNotaria->detalle  	= $_POST['detalle'];
    $oNotaria->estado    		= $_POST['estado'];
    $oNotaria->latitud    = $_POST['latitud'];
    $oNotaria->longitud    = $_POST['longitud'];


    if($_POST['opt']=='nnota'){
        if($oNotaria->create_nota()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Notaria creada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }

    }elseif($_POST['opt']=='mnota'){
        $oNotaria->idnotaria   = $_POST['id'];
        if($oNotaria->modify_nota()){
            echo json_encode(array("title" => "Operación realizada", "text" => "Notaria modificada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }
    }


}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}