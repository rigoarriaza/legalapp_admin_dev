<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cEspecialidad.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oEspecialidad   	= new Especialidad($db);
// set values
	if(($_POST['nombre'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
		die();
	}
	if(($_POST['descripcion'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Escriba una descripción", "type" => "warning"),JSON_UNESCAPED_UNICODE);
		die();
	}

	try{
	$oEspecialidad->nombre	 		= $_POST['nombre'];
	$oEspecialidad->descripcion  	= $_POST['descripcion'];
	$oEspecialidad->estado    			= $_POST['estado'];

	if($_POST['opt']=='nespe'){
        if(($_FILES['imagen']['name'])==''){
            echo json_encode(array("title" => "Alerta", "text" => "Seleccione una imagen", "type" => "warning"),JSON_UNESCAPED_UNICODE);
            die();
        }
        $imgFile 	= $_FILES['imagen']['name'];
        $tmp_dir 	= $_FILES['imagen']['tmp_name'];
        $imgSize 	= $_FILES['imagen']['size'];

        $upload_dir 		= '../IMG/'; // upload directory
        $imgExt 			= strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
        // // valid image extensions

        $valid_extensions 	= array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        // // rename uploading image
        $logo  		 	= "EspeIMG_" . time() . "." . $imgExt;
        // // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){
            // // Check file size '5MB'
            if($imgSize < 5000000)				{
                move_uploaded_file($tmp_dir,$upload_dir.$logo);
                $oEspecialidad->imagen =  "IMG/".$logo;
            }	else {
                echo json_encode(array("title" => "Alerta", "text" => "Imagen muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
                die();
            }
        }	else {
            echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imágenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
            die();
        }
			if($oEspecialidad->create_espe()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Especialidad creada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
			
	}elseif($_POST['opt']=='mespe'){
		$oEspecialidad->idespecialidad   = $_POST['id'];

        if($_FILES['imagen']['name']){
            $imgFile = $_FILES['imagen']['name'];
            $tmp_dir = $_FILES['imagen']['tmp_name'];
            $imgSize = $_FILES['imagen']['size'];
            $upload_dir = '../IMG/'; // upload directory

            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            // valid image extensions

            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            // rename uploading image
            $img = "EspeIMG_" . time() . "." . $imgExt;
            // allow valid image file formats
            if(in_array($imgExt, $valid_extensions)){
                // Check file size '5MB'
                if($imgSize < 5000000)				{
                    move_uploaded_file($tmp_dir,$upload_dir.$img);
                }	else {
                    echo json_encode(array("title" => "Alerta", "text" => "Imagen muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
                    die();
                }
            }	else {
                echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imágenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
                die();
            }
            if(!isset($errMSG)) {
                $oEspecialidad->imagen = "IMG/".$img;
                //unlink("../".$_POST['oldimg']);
            } else {
                echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
                die();
            }
        }else{
            $oObject->imagen = $_POST['oldimg'];
        }
			if($oEspecialidad->modify_espe()){
				echo json_encode(array("title" => "Operación realizada", "text" => "Especialidad modificada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
	}
	
	
	}catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}