<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cAprobarCalificacion.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oAprob         	= new AprobarC($db);
// set values
if(($_POST['id'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ocurrió un error, inténtelo de nuevo", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

if(($_POST['estado'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ocurrió un error, inténtelo de nuevo", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}


try{
    $oAprob->idcalificacion_pregunta_express	 		= $_POST['id'];
    $oAprob->estado                                  	= $_POST['estado'];

        if($oAprob->modify_estadoCali()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Estado de calificación modificado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }




}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}