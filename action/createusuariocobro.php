<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuarioCobro.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oUsuarioCobro   = new UsuarioCobro($db);
// set values
if(($_POST['user'])=='' && $_POST['opt'] == 'nuser'){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre de usuario", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}
if(($_POST['secret'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Por favor genere un código para este usuario", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}
if(($_POST['org'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre de una organización", "type" => "warning"));
    die();
}

try{


    $oUsuarioCobro->secret    	 	= $_POST['secret'];
    $oUsuarioCobro->org    	 	    = $_POST['org'];
    $oUsuarioCobro->estado    	 	= $_POST['estado'];

    if($_POST['opt']=='nuser'){
        $oUsuarioCobro->user    	 	= $_POST['user'];
        if($oUsuarioCobro->create_usuariocobro()){
            echo json_encode(array("title" => "Éxito!", "text" => "Usuario creado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => "Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);

        }

    }elseif($_POST['opt']=='muser'){
        $oUsuarioCobro->idusuario	 	= $_POST['id'];

        if($oUsuarioCobro->modify_usuariocobro()){
            echo json_encode(array("title" => "Éxito!", "text" => "Usuario modificado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => "Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }


    }

}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}