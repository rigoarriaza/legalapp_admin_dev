<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oPlan      = new Plan($db);
// set values
if(($_POST['firma'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Seleccione Firma", "type" => "warning"));
    die();
}
if(($_POST['pro'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Seleccione un profesional", "type" => "warning"));
    die();
}


try{


    $oPlan->idfirmas                        	= $_POST['firma'];
    $oPlan->idusuarioprofesional            	= $_POST['pro'];

    if($_POST['opt']=='nprofirma'){

        if($oPlan->create_asignarProAFirma()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Firma asignado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }

    }

}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}