<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cInventario.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oData   	= new Inventario($db);
// set values

$oData->cantidad	= $_POST['cantidad'];
$oData->idproducto  = $_POST['id'];
$oData->fecha  		= date('Y-m-d');
$oData->precio		= 0;
$verificacion 		= $oData->verificar_inventario_fecha();
if (!$verificacion) {
	if($oData->create_inventario()){
		if($oData->update_stock_total()){
			$cantidad = $oData->get_productos_cantidad();
			echo json_encode(array("title" => "Operacion realizada", "text" => "inventario actualizado.", "type" => "success", "cantidad" => $cantidad));
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}
	}else{
		echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
	}
} else {
	$oData->idinventario 	= $verificacion;
	$canv					= $oData->verificar_inventario_fecha2();
	$cann					= $_POST['cantidad'];
	$total 					= $cann - $canv;
	$oData->cantidad		= $total;
	if($oData->update_stock_inventario()){
		$oData->update_stock_total();
		$cantidad = $oData->get_productos_cantidad();
		echo json_encode(array("title" => "Operacion realizada", "text" => "inventario actualizado.", "type" => "success", "cantidad" => $cantidad));
	}else{
		echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
	}
}		
