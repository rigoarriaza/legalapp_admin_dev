<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cRuta.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oData   	= new Ruta($db);
// set values


if($_POST['opt']=='nRuta'){
	$oData->idempresa	= $_POST['empresa'];
	$oData->idusuario  	= $_POST['vendedores'];
	$verificacion 		= $oData->verificar_ruta_usuario();
	if (!$verificacion) {
		$idruta 			= $oData->create_rutas();
		$oData->idruta  	= $idruta;
		if($idruta){
			$idelm	= $oData->verificar_ruta_empresa();
			if(!$idelm){
				if($oData->create_empresa_ruta()){
					echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa agregada a la ruta del vendedor.", "type" => "success", "id" => $idelm));
				}else{
					echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
				}
			}else{
				echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa agregada a la ruta del vendedor.", "type" => "success", "id" => $idelm));
			}
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}
	} else {
		$oData->idruta = $verificacion;
		$idelm	= $oData->verificar_ruta_empresa();
		if(!$idelm){
			if($oData->create_empresa_ruta()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa agregada a la ruta del vendedor.", "type" => "success", "id" => $idelm));
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
			}
		}else{
			echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa agregada a la ruta del vendedor.", "type" => "success", "id" => $idelm));
		}
	}		
}elseif($_POST['opt']=='eRuta'){
	$oData->iddetalle_empresa_ruta   = $_POST['id'];
	if($oData->delete_empresa_ruta()){
		echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa eliminada de la ruta del vendedor.", "type" => "success"));
	}else{
		echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
	}
}
	