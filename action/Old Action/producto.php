<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cProducto.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oData   = new Producto($db);
// set values
if(($_POST['nombre'])==''){
	echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre del producto", "type" => "warning"));
	die();
}
if(($_POST['codigo'])==''){
	echo json_encode(array("title" => "Alerta", "text" => "Ingrese el codigo del producto", "type" => "warning"));
	die();
}
if(($_POST['cantidad'])==''){
	echo json_encode(array("title" => "Alerta", "text" => "Ingrese la cantidad de huevos", "type" => "warning"));
	die();
}
$oData->nombre	 	= $_POST['nombre'];
$oData->codigo  	= $_POST['codigo'];
$oData->cantidad    = $_POST['cantidad'];
$oData->estado    = $_POST['estado'];
$verificacion 		= $oData->get_verificacion_product();
if($_POST['opt']=='nProducto'){
	if (!$verificacion) {
		if($oData->create_product()){
			echo json_encode(array("title" => "Operacion realizada", "text" => "Producto creado con exito.", "type" => "success"));
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}
	} else {
		echo json_encode(array("title" => "Alerta", "text" => "Ya existe un producto con ese nombre", "type" => "warning"));
	}		
}elseif($_POST['opt']=='mProducto'){
	$oData->idproducto   = $_POST['id'];
	if($oData->modify_product()){
		echo json_encode(array("title" => "Operacion realizada", "text" => "Producto modificado con exito.", "type" => "success"));
	}else{
		echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
	}
}
	