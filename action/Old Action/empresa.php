<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuario.php';
include_once '../clases/cEmpresa.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oUsuario   = new Usuario($db);
$oEmpresa   = new Empresa($db);
// set values
	if(($_POST['nombre_empresa'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre de la empresa", "type" => "warning"));
		die();
	}
	if(($_POST['direccion'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese la direcci�n de la empresa", "type" => "warning"));
		die();
	}
	if(($_POST['departamento'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el departamento", "type" => "warning"));
		die();
	}
	if(($_POST['municipio'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el municipio", "type" => "warning"));
		die();
	}
	if(($_POST['nregistro'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el numero de registro", "type" => "warning"));
		die();
	}
	if(($_POST['tdoc'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Seleccione el tipo de documento a expedir", "type" => "warning"));
		die();
	}
	if(($_POST['giro'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el giro de la empresa", "type" => "warning"));
		die();
	}
	if(($_POST['nit'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el numero de NIT", "type" => "warning"));
		die();
	}
	if(($_POST['telefono1'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el numero de telefono principal", "type" => "warning"));
		die();
	}
	$oEmpresa->nombre_empresa	= $_POST['nombre_empresa'];
	$oEmpresa->direccion  		= $_POST['direccion'];
	$oEmpresa->departamento    	= $_POST['departamento'];
	$oEmpresa->municipio    	= $_POST['municipio'];
	$oEmpresa->telefono1    	= $_POST['telefono1'];
	$oEmpresa->telefono2    	= $_POST['telefono2'];
	$oEmpresa->nit    			= $_POST['nit'];
	$oEmpresa->numero_registro  = $_POST['nregistro'];
	$oEmpresa->giro    			= $_POST['giro'];
	$oEmpresa->tipo_documento   = $_POST['tdoc'];
	
	if($_POST['opt']=='nempresa'){
		if(($_POST['nombre'])==''){
			echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
			die();
		}
		if(($_POST['apellido'])==''){
			echo json_encode(array("title" => "Alerta", "text" => "Ingrese el apellido", "type" => "warning"));
			die();
		}
		if(($_POST['mail'])==''){
			echo json_encode(array("title" => "Alerta", "text" => "Ingrese el correo electronico", "type" => "warning"));
			die();
		}
		if($_POST['opt']=='nempresa'){
			if(($_POST['pass'])==''){
				echo json_encode(array("title" => "Alerta", "text" => "Ingrese el password", "type" => "warning"));
				die();
			}
			if($_POST['pass'] != $_POST['passconfirm']){
				echo json_encode(array("title" => "Alerta", "text" => "Contrase�a y verificacion no coinciden", "type" => "warning"));
				die();
			}
		}
		if(($_POST['telefono'])==''){
			echo json_encode(array("title" => "Alerta", "text" => "Ingrese el numero de telefono", "type" => "warning"));
			die();
		}
		$oUsuario->nombre	 	= $_POST['nombre'];
		$oUsuario->apellido  	= $_POST['apellido'];
		$oUsuario->mail    		= $_POST['mail'];
		$oUsuario->telefono    	= $_POST['telefono'];
		$oUsuario->idrol    	= 3;
		$oUsuario->estado    	= $_POST['estado'];
		$verificacion 			= $oUsuario->get_verificacion_mail();
		$oUsuario->password    	= sha1($_POST['pass']); 
	
		if (!$verificacion) {
			$idUsuario = $oUsuario->create_user();
			if($idUsuario){
				
				$oEmpresa->idusuario    	= $idUsuario;
				if($oEmpresa->create_empresa()){
					echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa creada con exito.", "type" => "success"));
				}else{
					echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
				}
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Alerta", "text" => " Cuenta de correo electronico ya usada", "type" => "warning"));
		}		
	}elseif($_POST['opt']=='mempresa'){
		$oEmpresa->idempresa   = $_POST['id'];
		if($oEmpresa->modify_empresa()){
			echo json_encode(array("title" => "Operacion realizada", "text" => "Empresa modificado con exito.", "type" => "success"));
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}
	}
	