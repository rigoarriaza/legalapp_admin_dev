<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cInventario.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oData   	= new Inventario($db);
// set values

$oData->cantidad	= $_POST['cantidad'];
$oData->idproducto  = $_POST['id'];
$oData->idusuario  	= $_POST['idvendedor'];
$oData->fecha  		= date('Y-m-d');
$oData->estado  	= 1;
$verificacion 		= $oData->get_asignacion_vendedor();
$verificacionC 		= $oData->get_productos_cantidad();

if($verificacionC==0){
	echo json_encode(array("title" => "Error", "text" => "No hay suficiente producto para poder asignar", "type" => "error"));
	exit();
}
if(($verificacionC-$_POST['cantidad'])<0){ 
	echo json_encode(array("title" => "Error", "text" => "No hay suficiente producto para poder asignar", "type" => "error"));
	exit();
}
if (!$verificacion) {
	$idasignacion = $oData->create_asignacion_vendedor();
	if($idasignacion){
		$oData->idasignacion_vendedor 	= $idasignacion;
		$oData->precio 					= 0;
		if($oData->create_asignacion_detalle()){
			$oData->update_stock_total2();
			$cantidad = $oData->get_productos_cantidad();
			echo json_encode(array("title" => "Operacion realizada", "text" => "inventario actualizado.", "type" => "success", "cantidad" => $cantidad));
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}
	}else{
		echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
	}
} else {
	$oData->idasignacion_vendedor 	= $verificacion;
	$v2 = $oData->get_verificacion_detalle_prod_ven();
	if($v2){
		$canAv							= $oData->get_productos_cantidad_v();
		$cann							= $_POST['cantidad'];
		$total 							= $cann-$canAv;
		$oData->cantidad				= $total;
		if($oData->update_stock_producto_vendedor()){
			$oData->update_stock_total2();
			$cantidad = $oData->get_productos_cantidad();
			echo json_encode(array("title" => "Operacion realizada", "text" => "inventario actualizado.", "type" => "success", "cantidad" => $cantidad));
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}
	}else{
		$oData->precio 					= 0;
		if($oData->create_asignacion_detalle()){
			$oData->update_stock_total2();
			$cantidad = $oData->get_productos_cantidad();
			echo json_encode(array("title" => "Operacion realizada", "text" => "inventario actualizado.", "type" => "success", "cantidad" => $cantidad));
		}else{
			echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
		}

	}
	
}		
