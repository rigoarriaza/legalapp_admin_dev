<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cRefill.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oData   	= new Refil($db);
// set values

$oData->idrefill	= $_POST['id'];
$oData->estado  	= $_POST['estado'];
if($oData->update_estado_refil()){
	echo json_encode(array("title" => "Operacion realizada", "text" => "Accion aplicada correctamente.", "type" => "success"));
}else{
	echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
}
		
