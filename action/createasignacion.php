<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oPlan      = new Plan($db);
// set values
if(($_POST['firma'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Seleccione Firma", "type" => "warning"));
    die();
}
if(($_POST['plan'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Seleccione un plan", "type" => "warning"));
    die();
}
if(($_POST['fecInicio']) == ''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese un fecha de inicio.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}
if(($_POST['fecFin'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese un fecha de finalización.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

try{
    $fecInicio      = $_POST['fecInicio'];
    $fecFin         = $_POST['fecFin'];

    $fec1 = explode('/',$fecInicio);
    $fec2 = explode('/',$fecFin);

    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';


    $oPlan->idfirmas        	= $_POST['firma'];
    $oPlan->idplan            	= $_POST['plan'];
    $oPlan->fechacompra        	= $fecInicio;
    $oPlan->fechavencimiento  	= $fecFin;
    $oPlan->estado    	        = $_POST['estado'];

    if($_POST['opt']=='nasignacion'){

        if($oPlan->create_asignacion()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Firma asignado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }

    }elseif($_POST['opt']=='masignacion'){
        $oPlan->idmembresiafirma  = $_POST['id'];
        if($oPlan->mod_asignacion()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Firma modificado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }
    }

}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}