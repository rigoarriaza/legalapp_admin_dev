<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
include_once '../clases/cUsuariopro.php';
include_once '../PHPmailer/PHPMailerAutoload.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oPlan              = new Plan($db);
$oUsuarioPro        = new Usuariopro($db);

//in case there is a modification, send message to user
$successMod = false;
// set values

if(($_POST['plan'])!=''){
    if(($_POST['fecInicio']) == ''){
        echo json_encode(array("title" => "Alerta", "text" => "Ingrese un fecha de inicio.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }
    if(($_POST['fecFin'])==''){
        echo json_encode(array("title" => "Alerta", "text" => "Ingrese un fecha de finalización.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }


}

try{

    if(($_POST['plan'])!='') {



        $fecInicio = $_POST['fecInicio'];
        $fecFin = $_POST['fecFin'];

        $fec1 = explode('/', $fecInicio);
        $fec2 = explode('/', $fecFin);

        $fecInicio = $fec1[2] . '-' . $fec1[0] . '-' . $fec1[1] . ' 00:00:01';
        $fecFin = $fec2[2] . '-' . $fec2[0] . '-' . $fec2[1] . ' 23:59:59';

        $oPlan->idusuarioprofesional = $_POST['userpro'];
        $oPlan->idplan = $_POST['plan'];
        $oPlan->fechacompra = $fecInicio;
        $oPlan->fechavencimiento = $fecFin;
        $oPlan->estado = $_POST['estado'];
        $oPlan->webName = $_POST['webName'];
        $oPlan->nombrePago = $_POST['nombrePago'];

        if($_POST['optP']=='nplan'){
            $setTransaccion = $oPlan->setTransaction();
            if(!$setTransaccion){
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar el cambio de plan.", "type" => "error"),JSON_UNESCAPED_UNICODE);
                die();
            } else {
                $oPlan->idlogtransaccional = $setTransaccion;
            }

            if(!$oPlan->create_asignacionPro()){
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información del plan", "type" => "error"),JSON_UNESCAPED_UNICODE);
                die();
            }

            $successMod = true;

        } else if ($_POST['optP']=='mplan') {
            $oPlan->idmembresia  = $_POST['idmembresia'];
            $currentPlan = $_POST['currentPlan'];

            $curFecInicio   = $_POST['curFecInicio'];
            $curFecFin      = $_POST['curFecFin'];

            $curFec1 = explode('/', $curFecInicio);
            $curFec2 = explode('/', $curFecFin);

            $curFecInicio = $curFec1[2] . '-' . $curFec1[0] . '-' . $curFec1[1] . ' 00:00:01';
            $curFecFin = $curFec2[2] . '-' . $curFec2[0] . '-' . $curFec2[1] . ' 23:59:59';

            if ($currentPlan !== '') {
                if ($currentPlan !== $_POST['plan'] || ($curFecInicio !== $fecInicio && $curFecFin !== $fecFin)){
                    $setTransaccion = $oPlan->setTransaction();
                    if(!$setTransaccion){
                        echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar el cambio de plan.", "type" => "error"),JSON_UNESCAPED_UNICODE);
                        die();
                    } else {
                      $oPlan->idlogtransaccional = $setTransaccion;
                    }
                }
            }

            if(!$oPlan->mod_asignacionPro()){
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información del plan", "type" => "error"),JSON_UNESCAPED_UNICODE);
                die();
            }
            $successMod = true;
        }
    }

    if($_POST['opt']=='mpro'){

        $oUsuarioPro->idusuarioprofesional = $_POST['userpro'];
        $oUsuarioPro->destacado = $_POST['destacado'];
        $oUsuarioPro->estadoUsuario = $_POST['estadoUsuario'];
        $oUsuarioPro->estadoVerificacion = $_POST['estadoVerificacion'];

        if($oUsuarioPro->mod_pro()){
            $successMod = true;
            $tempEstado = $_POST['estadoVerificacion'];

            if ($tempEstado == '1'){
                $mail = new PHPMailer;
                $mail->setFrom('no-reply@legalbo.com', 'Admin de Legal App');
                $mail->addAddress($_POST['mailPro'], $_POST['nombreproVALUE']);
                $mail->Subject = 'Bienvenido a Legal App.';
                $mail->isHTML(true);
                $mail->Body = '<h1>Bienvenido a Legal App!</h1>
                                <br><br>
                                <h2>Tu cuenta está Habilitada</h2> 
                                <br><br> 
                                <p>El equipo de Legal App, te damos la Bienvenida nuevamente a la comunidad más grande de abogados de Bolivia.
                                <br>Ya puedes empezar a ganar con tu negocio en casa. Elíje el plan que se adapte a tus necesidades y contestale a tus clientes.<br>
                                <br>
                                Nuestros mejores deseos,
                                </p>
                                <br><br>
                                <b>El equipo de Legal App.</b>
                                ';
                if(!$mail->send()) {
                    //echo 'Message was not sent.';
                    //echo 'Mailer error: ' . $mail->ErrorInfo;
                } else {
                    //echo 'Message has been sent.';
                }
            }

        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($successMod) {
        echo json_encode(array("title" => "Operacion realizada", "text" => "Cambios para usuario profesional agregados con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
    }

}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}