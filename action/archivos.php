<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cArchivos.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oObject   	= new Archivo($db);
// set values
	if(($_POST['nombre'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
		die();
	}

	try{
		$oObject->nombre			= $_POST['nombre'];
		$oObject->idespecialidad    = $_POST['especialidad'];
		$oObject->idtipoarchivo    	= $_POST['tipo'];
		$oObject->estado    		= $_POST['estado'];

		if($_POST['opt']=='nArchivo'){
			if(($_FILES['archivo']['name'])==''){
				echo json_encode(array("title" => "Alerta", "text" => "Ingrese un archivo", "type" => "warning"),JSON_UNESCAPED_UNICODE);
				die();
			}
			$imgFile 	= $_FILES['archivo']['name'];
			$tmp_dir 	= $_FILES['archivo']['tmp_name'];
			$fileSize 	= $_FILES['archivo']['size'];
			
			$upload_dir = '../FILES/'; // upload directory 
			$imgExt 	= strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			// // valid image extensions

			//$valid_extensions 	= array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			// // rename uploading image
			 $file  	= "archivo" . time() . "." . $imgExt;
			// // allow valid image file formats
			//if(in_array($imgExt, $valid_extensions)){
				// // Check file size '5MB'
				if($fileSize < 5000000)				{
					move_uploaded_file($tmp_dir,$upload_dir.$file);
					$oObject->url =  "FILES/".$file;
				}	else {
					echo json_encode(array("title" => "Alerta", "text" => "Archivo muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
					die();
				 }
			//}	else {
			//	echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imagenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
			//	die();
			//}
			$r = $oObject->create_archivo();
			if($r){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Archivo creado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
				
		}elseif($_POST['opt']=='mArchivo'){
			$oObject->idarchivo   = $_POST['id'];
			
			if($_FILES['archivo']['name']){
				$imgFile = $_FILES['archivo']['name'];
				$tmp_dir = $_FILES['archivo']['tmp_name'];
				$fileSize = $_FILES['archivo']['size']; 
				$upload_dir = '../FILES/'; // upload directory
				
				$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
				// valid image extensions

				//$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
				// rename uploading image
				$file = "tipoarchivo_" . time() . "." . $imgExt;
				// allow valid image file formats
				//if(in_array($imgExt, $valid_extensions)){
					// Check file size '5MB'
					if($fileSize < 5000000)				{
						move_uploaded_file($tmp_dir,$upload_dir.$file);
					}	else {
						echo json_encode(array("title" => "Alerta", "text" => "Archivo muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
						die();
					}
				//}	else {
				//	echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imagenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
				//	die();
				//}			
				if(!isset($errMSG)) {
					$oObject->url = "FILES/".$file;
					//unlink("../".$_POST['oldimg']);
				} else {
					echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
					die();
				}
			}else{
				$oObject->url = $_POST['oldfile'];
			}
			if($oObject->modify_archivo()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Archivo modificado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
		}
	}catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}