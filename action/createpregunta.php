<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cExamen.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oObject   	= new Examen($db);
// set values
	if(($_POST['pregunta'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese la pregunta", "type" => "warning"));
		die();
	}
	if(($_POST['respuesta'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Escriba una respuesta", "type" => "warning"),JSON_UNESCAPED_UNICODE);
		die();
	}
	if(($_POST['examen'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Seleccione un examen", "type" => "warning"),JSON_UNESCAPED_UNICODE);
		die();
	}


	try{
		$oObject->idexamenpro	= $_POST['examen'];
		$oObject->pregunta  	= $_POST['pregunta'];
		$oObject->respuesta  	= $_POST['respuesta'];
		$oObject->estado    	= $_POST['estado'];

		if($_POST['opt']=='nPregunta'){
			$r = $oObject->create_pregunta();
			if($r){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Examen creado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
				
		}elseif($_POST['opt']=='mPregunta'){
			$oObject->idpreguntasexamen   = $_POST['id'];
			if($oObject->modify_pregunta()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Examen modificado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
		}
	}catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}