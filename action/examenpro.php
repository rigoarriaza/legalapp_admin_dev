<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cExamen.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oObject   	= new Examen($db);
// set values
	if(($_POST['nombre'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
		die();
	}
	if(($_POST['descripcion'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Escriba una descripción", "type" => "warning"),JSON_UNESCAPED_UNICODE);
		die();
	}
	if(($_POST['especialidad'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Seleccione una especialidad", "type" => "warning"),JSON_UNESCAPED_UNICODE);
		die();
	}


	try{
		$oObject->nombre			= $_POST['nombre'];
		$oObject->idespecialidad  	= $_POST['especialidad'];
		$oObject->descripcion  		= $_POST['descripcion'];
		$oObject->estado    		= $_POST['estado'];

		if($_POST['opt']=='nExamen'){
			if(($_FILES['icono']['name'])==''){
				echo json_encode(array("title" => "Alerta", "text" => "Ingrese un icono para el examen", "type" => "warning"),JSON_UNESCAPED_UNICODE);
				die();
			}
			$imgFile 					= $_FILES['icono']['name'];
			$tmp_dir 					= $_FILES['icono']['tmp_name'];
			$imgSize 					= $_FILES['icono']['size'];
			
			$upload_dir 		= '../IMG/'; // upload directory 
			$imgExt 			= strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			// // valid image extensions

			$valid_extensions 	= array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			// // rename uploading image
			 $logo  		 	= "examenpro_" . time() . "." . $imgExt;
			// // allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){
				// // Check file size '5MB'
				if($imgSize < 5000000)				{
					move_uploaded_file($tmp_dir,$upload_dir.$logo);
					$oObject->icono =  "IMG/".$logo;
				}	else {
					echo json_encode(array("title" => "Alerta", "text" => "Imagen muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
					die();
				 }
			}	else {
				echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imagenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
				die();
			}
			$r = $oObject->create_examen();
			if($r){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Examen creado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
				
		}elseif($_POST['opt']=='mExamen'){
			$oObject->idexamenpro   = $_POST['id'];
			if($_FILES['icono']['name']){
				$imgFile = $_FILES['icono']['name'];
				$tmp_dir = $_FILES['icono']['tmp_name'];
				$imgSize = $_FILES['icono']['size']; 
				$upload_dir = '../IMG/'; // upload directory
				
				$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
						// valid image extensions

				$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
				// rename uploading image
				$img = "examenpro_" . time() . "." . $imgExt;
				// allow valid image file formats
				if(in_array($imgExt, $valid_extensions)){
					// Check file size '5MB'
					if($imgSize < 5000000)				{
						move_uploaded_file($tmp_dir,$upload_dir.$img);
					}	else {
						echo json_encode(array("title" => "Alerta", "text" => "Imagen muy pesada.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
						die();
					}
				}	else {
					echo json_encode(array("title" => "Alerta", "text" => "Formato no permitido, solo imagenes JPG, JPEG, PNG & GIF soportados.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
					die();
				}			
				if(!isset($errMSG)) {
					$oObject->icono = "IMG/".$img;
					//unlink("../".$_POST['oldimg']);
				} else {
					echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
					die();
				}
			}else{
				$oObject->icono = $_POST['oldimg'];
			}
			if($oObject->modify_examen()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Examen modificado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"),JSON_UNESCAPED_UNICODE);
			}
		}
	}catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}