<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cTransac.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oTransac        	= new Transac($db);
// set values
if(($_POST['montoSaldo'])=='' || ($_POST['montoSaldo']) =='' ){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese una cantidad de saldo!", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

try{
    $oTransac->montoSaldo	 	= $_POST['montoSaldo'];
    $oTransac->idusuarioapp	 	= $_POST['id'];


    if($oTransac->updateSaldoUser()){
        echo json_encode(array("title" => "Operación realizada", "text" => "Saldo actualizado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
    }else{
        echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
    }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}