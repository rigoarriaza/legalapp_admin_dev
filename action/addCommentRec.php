<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cTransac.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oTransac        	= new Transac($db);
// set values
if(($_POST['comentario'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Escriba una comentario para poder guardarlo!", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

try{
    $oTransac->comentario	 	= $_POST['comentario'];
    $oTransac->idtransac	 	= $_POST['idtransac'];
    $oTransac->type	 	        = $_POST['type'];


    if($oTransac->updateComment()){
        echo json_encode(array("title" => "Operación realizada", "text" => "Comentario actualizado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
    }else{
         echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
    }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}