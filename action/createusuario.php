<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuario.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oUsuario   = new Usuario($db);
// set values
	if(($_POST['nombre'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
		die();
	}
	if(($_POST['apellido'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el apellido", "type" => "warning"));
		die();
	}
	if(($_POST['mail'])==''){
		echo json_encode(array("title" => "Alerta", "text" => "Ingrese el correo electronico", "type" => "warning"));
		die();
	}
	if($_POST['opt']=='nuser'){
		if(($_POST['pass'])==''){
			echo json_encode(array("title" => "Alerta", "text" => "Ingrese el password", "type" => "warning"));
			die();
		}
		if($_POST['pass'] != $_POST['passconfirm']){
			echo json_encode(array("title" => "Alerta", "text" => "Clave y verificacion no coinciden", "type" => "warning"));
			die();
		}
	}
	try{
	$oUsuario->nombre	 	= $_POST['nombre'];
	$oUsuario->apellido  	= $_POST['apellido'];
	$oUsuario->mail    		= $_POST['mail'];
	$oUsuario->idrol    	= $_POST['rol'];
	$oUsuario->estado    	= $_POST['estado'];
	$verificacion 			= $oUsuario->get_verificacion_mail();
	if($_POST['opt']=='nuser'){
		$oUsuario->password    	= sha1($_POST['pass']);
		if (!$verificacion) {
			if($oUsuario->create_user()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Usuario creado con exito.", "type" => "success"));
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Alerta", "text" => " Cuenta de correo electronico ya usada", "type" => "warning"));
		}		
	}elseif($_POST['opt']=='muser'){
		$oUsuario->idusuario   = $_POST['id'];
		if($_POST['pass']!= ''){
			if($_POST['pass'] != $_POST['passconfirm']){
				echo json_encode(array("title" => "Alerta", "text" => "Clave y verificacion no coinciden", "type" => "warning"));
				die();
			}else{
				$oUsuario->password    	= sha1($_POST['pass']);
				if($oUsuario->modify_user2()){
					echo json_encode(array("title" => "Operacion realizada", "text" => "Usuario modificado con exito.", "type" => "success"));
				}else{
					echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
				}
			}
		}else{
			if($oUsuario->modify_user()){
				echo json_encode(array("title" => "Operacion realizada", "text" => "Usuario modificado con exito.", "type" => "success"));
			}else{
				echo json_encode(array("title" => "Error", "text" => " Ocurrio un problema al intentar almacenar la informacion", "type" => "error"));
			}
		}
	}
	
	}catch (Exception $e) {
    	echo 'Caught exception: ',  $e->getMessage(), "\n";
	}