<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCobros.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oCobro        	    = new Cobros($db);
// set values
if(($_POST['comentario'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Escriba una comentario para poder guardarlo!", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

try{
    $oCobro->comentario	 	= $_POST['comentario'];
    $oCobro->idusuarioprofesional	 	= $_POST['id'];
    $oCobro->idWeb	 	= $_SESSION['legalapp']['idusuario'];


    if($oCobro->executePayment()){
        echo json_encode(array("title" => "Operación realizada", "text" => "Pago entregado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
    }else{
        echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar entregar el pago", "type" => "error"),JSON_UNESCAPED_UNICODE);
    }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}