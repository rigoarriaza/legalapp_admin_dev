<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
include_once '../clases/cUsuarioApp.php';
$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuarioApp        = new UsuarioApp($db);
// set values

    if(($_POST['id']) == ''){
        echo json_encode(array("title" => "Alerta", "text" => "Ocurrió un error, inténtelo de nuevo por favor.", "type" => "error"),JSON_UNESCAPED_UNICODE);
        die();
    }
    if(($_POST['estadoUsuario'])==''){
        echo json_encode(array("title" => "Alerta", "text" => "Seleccione un estado para este usuario.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }



try{

    if($_POST['opt']=='mapp'){

        $oUsuarioApp->idusuario = $_POST['id'];
        $oUsuarioApp->estado = $_POST['estadoUsuario'];

        if($oUsuarioApp->modify_user()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Cambios para usuario de aplicación agregados con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }
    }else{
        echo json_encode(array("title" => "Alerta", "text" => "Ocurrió un error, inténtelo de nuevo por favor.", "type" => "error"),JSON_UNESCAPED_UNICODE);
        die();
    }

}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}