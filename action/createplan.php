<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oPlan      = new Plan($db);
// set values
if(($_POST['nombre'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
    die();
}
if(($_POST['costo'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese el costo", "type" => "warning"));
    die();
}
if(($_POST['costo']) != '' && is_double(($_POST['costo']))){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese un valor numérico para el campo de costo.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}
if(($_POST['descPlan'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese la descripción del plan", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

if(($_POST['numEspecialidad'])==''){
        echo json_encode(array("title" => "Alerta", "text" => "Ingrese una cantidad de especialidades disponibles", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }
    if(($_POST['numEspecialidad']) != '' && is_double($_POST['numEspecialidad'])){
        echo json_encode(array("title" => "Alerta", "text" => "Ingrese un valor numérico para el campo de cantidad de especialidades.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }

    if(($_POST['numCuentas']) != '' && is_double(($_POST['numCuentas']))){
        echo json_encode(array("title" => "Alerta", "text" => "Ingrese un valor numérico para el campo de cantidad de cuentas.", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }

if($_POST['tipoPlan']=='2'){
    
    if(($_POST['numCuentas'])==''){
        echo json_encode(array("title" => "Alerta", "text" => "Ingrese una cantidad de cuentas", "type" => "warning"),JSON_UNESCAPED_UNICODE);
        die();
    }
    
}
try{
    $oPlan->nombre	        	= $_POST['nombre'];
    $oPlan->costo            	= $_POST['costo'];
    $oPlan->mesesActivo      	= $_POST['mesesActivo'];
    $oPlan->descPlan         	= $_POST['descPlan'];
    $oPlan->tipoPlan    	    = $_POST['tipoPlan'];
    $oPlan->numEspecialidad       = $_POST['numEspecialidad'];
    $oPlan->ubicacion           = $_POST['ubicacion'];
    $oPlan->biblioteca          = $_POST['biblioteca'];

    if($oPlan->tipoPlan == '2'){
      
        $oPlan->numCuentas    	    = $_POST['numCuentas'];
        
    }
    $oPlan->estado    	        = $_POST['estado'];
    if($_POST['opt']=='nplan'){
        if ($_POST['tipoPlan'] == '2') {
            if($oPlan->create_planFirma()){
                echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Firma creado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
            }else{
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
            }
        } else {
            if($oPlan->create_planPro()){
                echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Profesional creado con exito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
            }else{
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información del plan profesional.", "type" => "error"),JSON_UNESCAPED_UNICODE);
            }
        }
    }elseif($_POST['opt']=='mplan'){
        $oPlan->idplan  = $_POST['id'];
        if ($_POST['tipoPlan'] == '2') {
            if($oPlan->mod_planFirma()){
                echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Firma modificado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
            }else{
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar modificar la información del plan firma", "type" => "error"),JSON_UNESCAPED_UNICODE);
            }
        } else {
            if($oPlan->mod_planPro()){
                echo json_encode(array("title" => "Operacion realizada", "text" => "Plan para Profesional modificado con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
            }else{
                echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar modificar la información del plan profesional", "type" => "error"),JSON_UNESCAPED_UNICODE);
            }
        }
    }

}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}