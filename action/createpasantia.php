<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPasantia.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oPasantia        	= new Pasantia($db);
// set values
if(($_POST['nombre'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Ingrese el nombre", "type" => "warning"));
    die();
}
if(($_POST['descripcion'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Escriba una descripción", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

if(($_POST['firma'])==''){
    echo json_encode(array("title" => "Alerta", "text" => "Seleccione una firma", "type" => "warning"),JSON_UNESCAPED_UNICODE);
    die();
}

try{
    $oPasantia->nombre	 		= $_POST['nombre'];
    $oPasantia->descripcion  	= $_POST['descripcion'];
    $oPasantia->estado    		= $_POST['estado'];
    $oPasantia->firma    		= $_POST['firma'];
    $oPasantia->idusuarioweb    = $_SESSION['legalapp']['idusuario'];


    if($_POST['opt']=='npasa'){
        if($oPasantia->create_pasa()){
            echo json_encode(array("title" => "Operacion realizada", "text" => "Pasantía creada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }

    }elseif($_POST['opt']=='mpasa'){
        $oPasantia->idpasantia   = $_POST['id'];
        if($oPasantia->modify_pasa()){
            echo json_encode(array("title" => "Operación realizada", "text" => "Pasantía modificada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }
    }


}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}