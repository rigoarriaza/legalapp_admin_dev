<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuario.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oUsuario   = new Usuario($db);
$loginAttempt = true;

if (!isset($_REQUEST['mail'])){
  echo json_encode(array("title" => "Alerta", "text" => "Necesita ingresar el correo.", "type" => "warning"));
  $loginAttempt = false;
}
if (!isset($_REQUEST['pass'])){
  echo json_encode(array("title" => "Alerta", "text" => "Necesita ingresar la contraseņa.", "type" => "warning"));
  $loginAttempt = false;
}
/* ---- Login ---- */
	try{
	
		//parametros
		$mail  			= $_REQUEST['mail'];
		$pass   		= sha1($_REQUEST['pass']);
		$oUsuario->mail = $mail;
		$oUsuario->pass = $pass;
		$login 			= $oUsuario->doLogin();

		if ( $login ) {
			echo json_encode(array("title" => "Acceso correcto", "text" => "Bienvenido.", "type" => "success"));
		} else {
			echo json_encode(array("title" => "", "text" => "Revisa la informaci&oacute;n proporcionada no coinciden con nuestros registros.", "type" => "warning"));
		}
	}catch (Exception $e){
		echo json_encode(array("title" => "Error del sistema", "text" => "Problemas de conexion a la base de datos.", "type" => "error"));
	}

/* ---- /Login ---- */


?>