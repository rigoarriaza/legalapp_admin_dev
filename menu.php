<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Home</a>
  <ul>
  <li id="1"><a href="index.php"><i class="icon icon-home"></i> <span>Home</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-list"></i> <span>Usuarios</span> <span class="label label-important">3</span></a>
        <ul id="usuarioAccor" style="display: none;">
            <li id="1A"><a href="usuarioapp.php"><i class="icon icon-group"></i> <span>Usuario App</span></a> </li>
            <li id="1B"><a href="usuariocobro.php"><i class="icon icon-group"></i> <span>Usuarios Para Cobro</span></a> </li>
            <li id="2"><a href="usuarios.php"><i class="icon icon-group"></i> <span>Usuarios Admin Web</span></a> </li>
        </ul>
      </li>
  <li class="submenu"> <a href="#"><i class="icon icon-list"></i> <span>Configuraciones</span> <span class="label label-important">8</span></a>
      <ul id="configAccor" style="display: none;">
          <li id="3"><a href="especialidades.php"><i class="icon-folder-close"></i> <span>Especialidades</span></a> </li>
          <li id="6A"><a href="tipoarchivo.php"><i class="icon-folder-close"></i> <span>Tipo de Archivo</span></a> </li>
          <li id="6B"><a href="archivos.php"><i class="icon-folder-close"></i> <span>Archivos</span></a> </li>
          <li id="9"><a href="pasantia.php"><i class="icon-plane"></i> <span>Pasantías</span></a> </li>
          <li id="10"><a href="firmas.php"><i class="icon-building"></i> <span>Firmas</span></a> </li>
          <li id="4"><a href="examenpro.php"><i class="icon-folder-close"></i> <span>Examenes</span></a> </li>
          <li id="5"><a href="createpregunta.php"><i class="icon-folder-close"></i> <span>Preguntas</span></a> </li>
          <li id="17"><a href="notaria.php"><i class="icon-folder-close"></i> <span>Notarias</span></a> </li>
      </ul>
  </li>
  <li class="submenu"> <a href="#"><i class="icon icon-list"></i> <span>Profesionales y Planes</span> <span class="label label-important">4</span></a>
      <ul id="proAccor" style="display: none;">
          <li id="11"><a href="usuariopro.php"><i class="icon-building"></i> <span>Usuario Profesional</span></a> </li>
          <li id="12"><a href="asignarProFirma.php"><i class="icon-exchange"></i> <span>Asignar Pro. a Firma</span></a> </li>
          <li id="7"><a href="plan.php"><i class="icon-folder-close"></i> <span>Configuración de Plan</span></a> </li>
          <li id="8"><a href="asignarPlan.php"><i class="icon-exchange"></i> <span>Asignar Planes a firmas</span></a> </li>
      </ul>
  </li>
  <li class="submenu"> <a href="#"><i class="icon icon-list"></i> <span>Transacciones</span> <span class="label label-important">6</span></a>
      <ul id="transacAccor" style="display: none;">
          <li id="13"><a href="aprobarCalificacion.php"><i class="icon-folder-close"></i> <span>Aprobar calificación</span></a> </li>
          <li id="19"><a href="insertSaldo.php"><i class="icon-folder-close"></i> <span>Asignar saldo a usuario</span></a> </li>
          <li id="14"><a href="transacRecargas.php"><i class="icon-folder-close"></i> <span>Transacciones de Recarga</span></a> </li>
          <li id="15"><a href="transacDebitos.php"><i class="icon-folder-close"></i> <span>Transacciones de Débitos</span></a> </li>
          <li id="16"><a href="cobroPro.php"><i class="icon-folder-close"></i> <span>Retiro de Efectivo</span></a> </li>
          <li id="18"><a href="visorPreguntas.php"><i class="icon-folder-close"></i> <span>Ver Preguntas</span></a> </li>
      </ul>
  </li>
    <!-- <li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Home</span></a> </li> -->
    </ul>
</div>
<!--sidebar-menu-->