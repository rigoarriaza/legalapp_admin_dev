<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once 'clases/cConexion.php';
include_once 'clases/cAprobarCalificacion.php';
include_once 'clases/cUsuario.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oUsuario   		= new Usuario($db);
$oAprob          	= new AprobarC($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//$estados 	= $oUsuario->getEstadosUsuario();
//$roles 	= $oUsuario->getRoles();


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Retiro de efectivo <?=date('Y-m-d')?></title>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
            color: blue !important;
        }
        .control-label{
            font-weight: bold;
            color: black !important;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }

        .swal-modal {
            overflow: scroll;
        }
    </style>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .ellipsis {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="cobroPro.php">Cobro de saldos</a> <a href="#" class="current">Entrega de pago a profesional</a>
        </div>
        <h1>Retiro de efectivo
            <br> 
            <a href="historialCobro.php" class="btn btn-warning">Consultar Histórico</a>
        </h1>
    </div>
    <div class="container-fluid conceal"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Consulta de profesionales</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formaretiro" id="formaretiro"
                              novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="mretiro"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">ID del Profesional: </label>
                                <div class="controls">
                                    <p id="idpro" style="font-style: italic"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Nombre del profesional: </label>
                                <div class="controls">
                                    <p id="nombre"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Usuario(Correo): </label>
                                <div class="controls">
                                    <p id="usuario"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Carnet: </label>
                                <div class="controls">
                                    <p id="carnet"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Foto Personal</label>
                                <div class="controls">
                                    <img id="fotoPer" name="fotoPer" src="" style="height: 150px; width: 150px;" />
                                    <p id="messageImg" name="messageImg" style="display: none; font-style: italic; font-size: smaller">No hay imagen para mostrar</p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Creación del Profesional: </label>
                                <div class="controls">
                                    <p id="creacion"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Total Pendiente de Pago: </label>
                                <div class="controls">
                                    <p id="pendiente"></p>
                                </div>
                            </div>
                            <div class="control-group hide3">
                                <label class="control-label">Total Histórico Entregado: </label>
                                <div class="controls">
                                    <p id="totalHistorico"></p>
                                </div>
                            </div>
                            <label class="control-label">Comentarios sobre entrega de ganancias</label>
                            <div class="controls">
                                <textarea rows="8" name="comentario" id="comentario" class="span6"></textarea>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="get_historial();" id="btnaction" value="Ver Historial" class="btn btn-warning">
                                <input type="button" onclick="get_payment_details(document.getElementById('id').value);" id="btnaction" value="Ver Pago Actual" class="btn btn-warning">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                            <div class="widget-box hide2">
                                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                                    <h5>Preguntas pendientes de Cobrar</h5>
                                </div>
                                <div class="widget-content nopadding">
                                    <table class="table table-bordered data-table" id="table2">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tipo Pregunta</th>
                                            <th>Título Pregunta</th>
                                            <th>Contenido</th>
                                            <th>Fecha creación (aaaa-mm-dd hh:mm:ss)</th>
                                            <th>Usuario App</th>
                                            <th>Valor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="widget-box hide3">
                                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                                    <h5>Histórico de cobros</h5>
                                </div>
                                <div class="widget-content nopadding">
                                    <table class="table table-bordered data-table" id="table3">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tipo Pregunta</th>
                                            <th>Título pregunta</th>
                                            <th>Usuario App</th>
                                            <th>Admin que entrega</th>
                                            <th>Fecha Entrega (aaaa-mm-dd hh:mm:ss)</th>
                                            <th>Valor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Usuarios elegibles para cobrar</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Carnet</th>
                                <th>Fecha creación (aaaa-mm-dd hh:mm:ss)</th>
                                <th>Ganancias Totales</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/moment.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;
    var tableDetail;
    var tableHistory;
    $(document).ready(function(){
        $('.conceal').hide();
        // ADD active state to current option
        var currentSel = $('#16');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#transacAccor').show();

        getProfesionales(false);
        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );


        tableDetail = $('#table2').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });


        tableHistory = $('#table3').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        $(document).on('submit', '#formaretiro', function() {
            if ( confirm('¿Realmente desea enviar el formulario? si hace esto se ejecutará el pago para este usuario. Esta acción' +
                ' NO puede ser revertida!')){

                $.post("action/executePayment.php", $(this).serialize())
                    .done(function(data) {
                        var parsed = JSON.parse(data);
                        swal({
                            title: parsed.title,
                            text: parsed.text,
                            type: parsed.type,
                            confirmButtonText: "Ok"
                        });
                        if(parsed.type=='success'){
                            cancelaction();
                        }
                    });
            }

            return false;
        });
    });


    function getProfesionales(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getProfesionales.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idusuarioprofesional'];
            var txt ='<a onclick="get_info_user('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;" class="text-data">Detalle</a>';
            table.fnAddData( [
                id,
                e['nombreCompleto'],
                e['correo'],
                e['carnet'],
                e['creacion'],
                e['ganancia'],
                txt
            ]);
        });
    }

    function get_info_user(id){
        $('.conceal').show();
        var fotopersonal;
        $.post("json/getDetailPro.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //console.log(data);
                    data = JSON.parse(data);

                    fotopersonal = 'http://legalbo.com/legalapp/IMGUSERS/'+data['fotopersonal'];

                    if(data['fotopersonal'] !== ""){
                        $('#fotoPer').attr('src',fotopersonal);
                        $('#fotoPer').css('display','block');
                        $('#messageImg').css('display','none');
                    }else{
                        $('#fotoPer').css('display','none');
                        $('#messageImg').css('display','block');
                    }

                    $('#opt').val('mretiro');
                    $('#id').val(data['idusuarioprofesional']);
                    $('#idpro').text(data['idusuarioprofesional']);
                    $('#nombre').text(data['nombreCompleto']);
                    $('#usuario').text(data['correo']);
                    $('#carnet').text(data['carnet']);
                    $('#creacion').text(data['creacion']);
                    $('#btnaction').val('Realizar Pago');

                    var comentario = data['comentariosPago'];
                    var currentDate = moment().format();
                    if(comentario === null || comentario === undefined){
                        $('#comentario').val('------'+currentDate+'------');
                    }else{
                        $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------'+'\n\n');
                    }



                    /* Need to add a comment field per professional, in order to keep track of money handed */
                    // var comentario = data['comentario'];
                    // var currentDate = moment().format();
                    // if(comentario === null || comentario === undefined){
                    //     $('#comentario').val('------'+currentDate+'------');
                    // }else{
                    //     $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------');
                    // }

                    get_payment_details(id);
                }
            }
        });
        return false;
    }

    // function get_payment_details(id){
    //     tableDetail.fnClearTable();
    //     $('.conceal').show();
    //     $('.hide3').hide();
    //     var fotopersonal;
    //     $.post("json/getDetailPayment.php",{
    //         id:id
    //     }, function (data,status){
    //         if(status =='success'){
    //             if (data!='ndata' &&  data !=''){
    //                 //console.log(data);
    //                 data = JSON.parse(data);
    //                 var cuentaPago = 0.00;
    //
    //                 data.forEach(function(e){
    //                     var id 	= e['idlog_ganancia_profesional'];
    //                     console.log("VALOR",parseFloat(e['valor']))
    //                     cuentaPago += parseFloat(e['valor']);
    //
    //                     tableDetail.fnAddData( [
    //                         id,
    //                         e['tipoPregunta'],
    //                         e['titulo'],
    //                         e['contenido'],
    //                         e['creacion'],
    //                         e['usuarioApp'],
    //                         e['valor'],
    //                     ]);
    //                 });
    //                 $('#pendiente').text(cuentaPago);
    //
    //             }
    //         }
    //     });
    //     return false;
    // }

    function get_payment_details(id){
        tableDetail.fnClearTable();

        $('.hide2').show();
        $('.hide3').hide();

        $.post("json/getDetailPayment.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //console.log(data);
                    data = JSON.parse(data);
                    var cuentaPago = 0.00;

                    data.forEach(function(e){
                        var id 	= e['idlog_ganancia_profesional'];
                        cuentaPago += parseFloat(e['valor']);

                        tableDetail.fnAddData( [
                            id,
                            e['tipoPregunta'],
                            e['titulo'],
                            e['contenido'],
                            e['creacion'],
                            e['usuarioApp'],
                            e['valor'],
                        ]);
                    });
                    $('#pendiente').text(cuentaPago);

                }
            }
        });

        tableDetail.api().buttons().container()
            .insertBefore( '#table2_filter' );


        return false;
    }

    function get_historial(){
        tableHistory.fnClearTable();

        var id = document.getElementById('id').value;

        $('.hide2').hide();
        $('.hide3').show();

        $.post("json/getHistoricPayment.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //console.log(data);
                    data = JSON.parse(data);
                    var cuentaPago = 0.00;

                    data.forEach(function(e){
                        var id 	= e['idlog_ganancia_profesional'];
                        cuentaPago += parseFloat(e['valor']);

                        tableHistory.fnAddData( [
                            id,
                            e['tipoPregunta'],
                            e['titulo'],
                            e['usuarioApp'],//Usuario App
                            e['nombreWeb']+' ('+e['correoWeb']+')',//Web User
                            e['entregado'],//Fecha Entrega
                            e['valor'],
                        ]);
                    });
                    $('#totalHistorico').text(cuentaPago);

                }
            }
        });
        tableHistory.api().buttons().container()
            .insertBefore( '#table3_filter' );
        return false;
    }

    function cancelaction(){
        $('.conceal').hide();
        getProfesionales(true);
    }
</script>
</body>
</html>
