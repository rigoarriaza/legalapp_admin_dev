<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cUsuario.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//Enter modifications

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title> Historial de Cobros </title>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
        }
        .control-label{
            font-weight: bold;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }

        .swal-modal {
            overflow: scroll;
        }
    </style>
    <?php
    require_once('headerHTML.php');
    ?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Transacciones</a> <a href="#" class="current">Cobros</a> </div>
        <h1>Historial de Cobros</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Historial de Cobros</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formbusqueda" id="formbusqueda" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nplan"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Correo Electrónico</label>
                                <div class="controls">
                                    <input type="text" name="mail" id="mail" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de inicio de Transacciones (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/01/Y')?>" class="input-append date datepicker">
                                        <input id="fecInicio" name="fecInicio" type="text" value="<?=date('m/01/Y')?>"  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de fin de Transacciones (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/t/Y')?>" class="input-append date datepicker">
                                        <input id="fecFin" name="fecFin" type="text" value="<?=date('m/t/Y')?>"  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Buscar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Resultados de la búsqueda</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Usuario</th>
                                <th>Correo Usuario</th>
                                <th>Fecha de Cobro</th>
                                <th>Monto</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="container-fluid">
        <div class="widget-box">
            <div class="conceal widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                <h5>Detalle de Pregunta/Transacción</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="conceal row-fluid">
                    <h3>Datos de Transacción</h3>
                    <div id="firstBlock">
                        <div class="control-group">
                            <label class="control-label">Pregunta realizada por:</label>
                            <div class="controls">
                                <span id="correoUsuario" name="correoUsuario" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Fecha creacion:</label>
                            <div class="controls">
                                <span id="creacion" name="creacion" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Título de pregunta:</label>
                            <div class="controls">
                                <span id="tituloPregunta" name="tituloPregunta" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Pregunta realizada:</label>
                            <div class="controls" style="width: 250px">
                                <span id="contenido" name="contenido" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Respuesta recibida:</label>
                            <div class="controls" style="width: 250px">
                                <span id="respuestaContenido" name="respuestaContenido" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Monto cobrado:</label>
                            <div class="controls">
                                <span id="cantidad" name="cantidad" class="text-data"></span>
                            </div>
                        </div>

                    </div>
                    <div id="secondBlock">
                        <div class="control-group">
                            <label class="control-label">Respondida por:</label>
                            <div class="controls">
                                <span id="correoPro" name="correoPro" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Calificación:</label>
                            <div class="controls">
                                <span id="calificacion" name="calificacion" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado Pregunta:</label>
                            <div class="controls">
                                <span id="estadoPregunta" name="estadoPregunta" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado Pago:</label>
                            <div class="controls">
                                <span id="estadoPago" name="estadoPago" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado Pago usuario:</label>
                            <div class="controls">
                                <span id="pago_usuario" name="pago_usuario" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Privacidad:</label>
                            <div class="controls">
                                <span id="privacidad" name="privacidad" class="text-data"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <form class="conceal form-horizontal" method="post" name="formComment" id="formComment" novalidate="novalidate">
                    <input type="hidden" id="idtransac" name="idtransac" value="" />
                    <input type="hidden" id="type" name="type" value="" />
                    <div class="control-group">
                        <label class="control-label">Comentario</label>
                        <div class="controls">
                            <textarea rows="8" name="comentario" id="comentario" class="span6"></textarea>
                        </div>
                        <div class="form-actions" style="padding-left: 180px;">
                            <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                            <input type="button" onclick="cancelComment();" value="Cancelar" class="btn btn-danger">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>-->
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="~/DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    $(document).ready(function(){
        $.noConflict();
        
        $('.conceal').hide();

        // ADD active state to current option
        var currentSel = $('#16');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#transacAccor').show();

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });


        $(document).on('submit', '#formbusqueda', function() {
            $.post("json/getCobrosHistorico.php", $(this).serialize())
                .done(function(data,status) {
                    //console.log(data);
                    if (status == 'success') {
                        data = data.replace(/^\s*|\s*$/g, "");
                        if(data!='ndata' && data!='error'){
                            //alert(data);
                            data = JSON.parse(data);
                            imprimirResultadosBusqueda(data);
                        }else{
                            swal({
                                title: 'Sin resultados',
                                text: 'No se encontraron resultados',
                                type: 'error',
                                confirmButtonText: "Ok"
                            });
                        }
                    }

                });
            return false;
        });

        $(document).on('submit', '#formComment', function() {
            $.post("action/addCommentDeb.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();

                    }
                });
            return false;
        });

    });

    function cancelComment(){
        var id = $('#idtransac').val();
        var type = $('#type').val();
        var loadType;
        if(type === 'PE'){
            loadType = true;
        }else{
            loadType = false;
        }
        get_info_transac(id,loadType)
    }



    function imprimirResultadosBusqueda(data){

        table.fnClearTable();
        $('.conceal').hide();

        var id;
        var txt;
        var nombre;

            data.forEach(function(e){
                id 	= e['idlog_ganancias_entregadas_profesional'];
                txt ='<a onclick="get_info_pregunta('+id+',true)" style="padding: 0px 5px 0px 0px; cursor: pointer;">Ver Info</a>';
                nombre = e['nombre']+ ' ' + e['apellido']; 
                table.fnAddData( [
                    id,
                    nombre,
                    e['correo'],
                    e['creacion'],
                    e['monto']
                ]);
            });


        table.api().buttons().container()
            .insertBefore( '#table1_filter' );
    }

    function get_info_pregunta(id,type){
        $('.conceal').show();

        if(type){
            $.post("json/getDebitDetailExpress.php",{
                id:id,
            }, function (data,status){
                if(status =='success'){
                    if (data !== 'ndata' &&  data !== ''){
                        //alert(data);
                        data = JSON.parse(data);
                        $('#idtransac').val(id);
                        $('#type').val('PE');

                        //correoUsuario
                        $('#correoUsuario').text(data['correoUsuario']);
                        //creacion
                        $('#creacion').text(data['creacion']);
                        //tituloPregunta
                        $('#tituloPregunta').text(data['titulo']);
                        //contenido
                        $('#contenido').text(data['contenido']);
                        //respuestaContenido
                        $('#respuestaContenido').text(data['respuestacontenido']);
                        //correoPro
                        $('#correoPro').text(data['correoPro']);
                        //calificacion
                        $('#calificacion').text(data['calificacion']);
                        //privacidad
                        var privacidad;

                        if(data['privacidad'] == '0'){
                            privacidad = 'Público';
                        }else if (data['privacidad'] == '1'){
                            privacidad = 'Privado';
                        }

                        $('#privacidad').text(privacidad);
                        //estadoPregunta
                        var estadoPregunta;

                        switch (data['estadoPregunta']) {
                            case '0':
                                estadoPregunta = "Cancelada";
                                break;
                            case '1':
                                estadoPregunta = "Por Pagar";
                                break;
                            case '2':
                                estadoPregunta = "Por Responder";
                                break;
                            case '3':
                                estadoPregunta = "Sin Respuesta";
                                break;
                            case '4':
                                estadoPregunta = "Recibiendo Respuesta";
                                break;
                            case '5':
                                estadoPregunta = "Contestada";
                                break;
                            case '6':
                                estadoPregunta = "Por Responder Apelacion";
                                break;
                            case '7':
                                estadoPregunta = "Finalizada Completamente";
                                break;
                            default:
                                estadoPregunta = "Estado no encontrado";
                                break
                        }




                        $('#estadoPregunta').text(estadoPregunta);

                        //estadoPago

                        var estadoPago;

                        if(data['estadoPago'] == '0'){
                            estadoPago = 'No Pagada';
                        }else if (data['estadoPago'] == '1'){
                            estadoPago = 'Pagado';
                        }

                        $('#estadoPago').text(estadoPago);

                        //pago_usuario
                        var pago_usuario;

                        if(data['pago_usuario'] == '0'){
                            pago_usuario = 'No Completado';
                        }else if (data['pago_usuario'] == '1'){
                            pago_usuario = 'Completado';
                        }

                        $('#pago_usuario').text(pago_usuario);

                        //cantidad
                        $('#cantidad').text(data['cantidad']);

                        //$('#comentario').val(data['comentario']);
                        var comentario = data['comentario'];
                        var currentDate = moment().format();
                        if(comentario === null || comentario === undefined){
                            $('#comentario').val('------'+currentDate+'------');
                        }else{
                            $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------');
                        }

                    }
                }
            });

        } else {
            $.post("json/getDebitDetailDirect.php",{
                id:id
            }, function (data,status){
                if(status =='success'){
                    if (data!='ndata' &&  data !=''){
                        //alert(data);
                        data = JSON.parse(data);
                        $('#idtransac').val(id);
                        $('#type').val('PD');

                        //correoUsuario
                        $('#correoUsuario').text(data['correoUsuario']);
                        //creacion
                        $('#creacion').text(data['creacion']);
                        //tituloPregunta
                        $('#tituloPregunta').text(data['titulo']);
                        //contenido
                        $('#contenido').text(data['contenido']);
                        //privacidad
                        var privacidad;

                        if(data['privacidad'] == '0'){
                            privacidad = 'Público';
                        }else if (data['privacidad'] == '1'){
                            privacidad = 'Privado';
                        }

                        $('#privacidad').text(privacidad);
                        //respuestaContenido
                        var txt;
                        txt ='<a onclick="getExchange('+data['idpreguntadirecta']+')" style="padding: 0px 5px 0px 0px; cursor: pointer; color: blue;">Ver Intercambio</a>';
                        $('#respuestaContenido').html(txt);
                        //correoPro
                        $('#correoPro').text(data['correoPro']);
                        //calificacion
                        $('#calificacion').text('N/A');
                        //estadoPregunta
                        var estadoPregunta;

                        switch (e['estadoPregunta']) {
                            case '0':
                                estadoPregunta = "Cancelada";
                                break;
                            case '1':
                                estadoPregunta = "Por Pagar";
                                break;
                            case '2':
                                estadoPregunta = "Por Responder";
                                break;
                            case '3':
                                estadoPregunta = "Sin Respuesta";
                                break;
                            case '4':
                                estado = "Recibiendo Respuesta";
                                break;
                            case '5':
                                estadoPregunta = "Contestada";
                                break;
                            case '6':
                                estadoPregunta = "Por Responder Apelacion";
                                break;
                            case '7':
                                estadoPregunta = "Finalizada Completamente";
                                break;
                            case '8':
                                estadoPregunta = "Rechazada";
                                break;
                            default:
                                estadoPregunta = "Estado no encontrado";
                                break
                        }

                        $('#estadoPregunta').text(estadoPregunta);

                        //estadoPago

                        var estadoPago;

//                        if(data['estadoPago'] == '0'){
//                            estadoPago = 'No Pagada';
//                        }else if (data['estadoPago'] == '1'){
//                            estadoPago = 'Pagado';
//                        }
                        estadoPago = 'N/A';

                        $('#estadoPago').text(estadoPago);

                        //pago_usuario
                        var pago_usuario;

                        if(data['pago_usuario'] == '0'){
                            pago_usuario = 'No Completado';
                        }else if (data['pago_usuario'] == '1'){
                            pago_usuario = 'Completado';
                        }

                        $('#pago_usuario').text(pago_usuario);

                        //cantidad
                        $('#cantidad').text(data['cantidad']);

                        //$('#comentario').val(data['comentario']);
                        var comentario = data['comentario'];
                        var currentDate = moment().format();
                        if(comentario === null || comentario === undefined){
                            $('#comentario').val('------'+currentDate+'------');
                        }else{
                            $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------');
                        }


                    }
                }
            });
        }
        return false;
    }
    function cancelaction(){
        setTimeout(function(){ window.location.reload(false); }, 2000);

    }

    function getExchange(idpreguntadirecta){
        var text = '';
        var fecha = '';
        var tipo_respuesta = '';
        var tipo_respuesta_text = '';
        var contenido = '';
        $.post("json/getExchange.php",{
            id:idpreguntadirecta
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //alert(data);
                    data = JSON.parse(data);
                    data.forEach(function(e){
                        fecha	= e['fecha'];
                        tipo_respuesta	= e['tipo_respuesta'];
                        contenido	= e['contenido'];

                        if (tipo_respuesta == '1') {
                            tipo_respuesta_text = 'Usuario';
                        } else {
                            tipo_respuesta_text = 'Profesional';
                        }

                        text = text+" Fecha: "+fecha+" --- Contesta:"+tipo_respuesta_text+". \n Respuesta: '"+contenido+"' \n --------------------- \n";

                    });
                    alert(text);
//                    swal({
//                        title: 'Respuestas',
//                        text: text,
//                        type: 'info',
//                        confirmButtonText: "Ok"
//                    });
                }
            }
        });

    }
</script>
</body>
</html>
