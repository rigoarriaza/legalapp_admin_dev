<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cPlan.php';
include_once 'clases/cUsuario.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);
$oPlan             	= new Plan($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//Enter modifications

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Transacciones de recarga <?=date('Y-m-d')?></title>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
        }
        .control-label{
            font-weight: bold;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }
    </style>
    <?php
    require_once('headerHTML.php');
    ?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Recargas</a> <a href="#" class="current">Transacciones</a> </div>
        <h1>Transacciones de recarga</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Transacciones de recarga Usuario App y Profesional</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formbusqueda" id="formbusqueda" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nplan"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Correo Electrónico</label>
                                <div class="controls">
                                    <input type="text" name="mail" id="mail" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de inicio de Transacciones (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/01/Y')?>" class="input-append date datepicker">
                                        <input id="fecInicio" name="fecInicio" type="text" value="<?=date('m/01/Y')?>"  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de fin de Transacciones (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/t/Y')?>" class="input-append date datepicker">
                                        <input id="fecFin" name="fecFin" type="text" value="<?=date('m/t/Y')?>"  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tipo de transaccion</label>
                                <div class="controls">
                                    <select name="tipoTrans" id="tipoTrans">
                                        <option value="0">Cualquiera</option>
                                        <option value="B">Banco</option>
                                        <option value="W">Web</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado Transaccion</label>
                                <div class="controls">
                                    <select name="estadoTrans" id="estadoTrans">
                                        <option value="na">Cualquiera</option>
                                        <option value="0">Cancelada</option>
                                        <option value="1">Pendiente</option>
                                        <option value="2">Completada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Buscar en...</label>
                                <div class="controls">
                                    <select name="tipoUsuario" id="tipoUsuario">
                                        <option value="1">Usuario App</option>
                                        <option value="2">Usuario Profesional</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Buscar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Resultados de la búsqueda</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Correo</th>
                                <th>Nombre Usuario</th>
                                <th>Referencia</th>
                                <th>Valor</th>
                                <th>Tipo Transacción</th>
                                <th>Estado</th>
                                <th>Fecha de Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Detalle de Transacción</h5>
        </div>
        <div class="widget-content nopadding">
               <div class="conceal row-fluid">
                    <h3>Datos de Transacción</h3>
                    <div id="firstBlock">
                    <div class="control-group">
                        <label class="control-label">Correo:</label>
                        <div class="controls">
                            <span id="correo" name="correo" class="text-data"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Nombre del Usuario:</label>
                        <div class="controls">
                            <span id="nombreCompleto" name="nombreCompleto" class="text-data"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Número de referencia:</label>
                        <div class="controls">
                            <span id="referencia" name="referencia" class="text-data"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Valor adquirido:</label>
                        <div class="controls">
                            <span id="valortransac" name="valortransac" class="text-data"></span>
                        </div>
                    </div>
                   <div class="control-group">
                       <label class="control-label">Tipo de Transaccion:</label>
                       <div class="controls">
                           <span id="tipoTransac" name="tipoTransac" class="text-data"></span>
                       </div>
                   </div>
                        </div>
                   <div id="secondBlock">
                    <div class="control-group">
                        <label class="control-label">Estado:</label>
                        <div class="controls">
                            <span id="estado" name="estado" class="text-data"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Creación:</label>
                        <div class="controls">
                            <span id="creacion" name="creacion" class="text-data"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Pago realizado por:</label>
                        <div class="controls">
                            <span id="personapago" name="personapago" class="text-data"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lugar de pago:</label>
                        <div class="controls">
                            <span id="lugar" name="lugar" class="text-data"></span>
                        </div>
                    </div>
                   <div class="control-group">
                        <label class="control-label">Agencia de pago:</label>
                        <div class="controls">
                            <span id="agencia" name="agencia" class="text-data"></span>
                        </div>
                    </div>
                   <div class="control-group">
                       <label class="control-label">Operador del pago:</label>
                       <div class="controls">
                           <span id="operador" name="operador" class="text-data"></span>
                       </div>
                   </div>
                 </div>

                   <form class="form-horizontal" method="post" name="formComment" id="formComment" novalidate="novalidate">
                       <input type="hidden" id="idtransac" name="idtransac" value="" />
                       <input type="hidden" id="type" name="type" value="" />
                       <div class="control-group">
                           <label class="control-label">Comentario</label>
                           <div class="controls">
                               <textarea rows="8" name="comentario" id="comentario" class="span6"></textarea>
                           </div>
                           <div class="form-actions" style="padding-left: 180px;">
                               <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                               <input type="button" onclick="cancelComment();" value="Cancelar" class="btn btn-danger">
                           </div>
                       </div>

                   </form>

                </div>

        </div>
    </div>
        </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    $(document).ready(function(){

        $('.conceal').hide();

        // ADD active state to current option
        var currentSel = $('#14');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#transacAccor').show();

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });


        $(document).on('submit', '#formbusqueda', function() {
            $.post("json/getTransacData.php", $(this).serialize())
                .done(function(data,status) {
                    //console.log(data);
                    if (status == 'success') {
                        data = data.replace(/^\s*|\s*$/g, "");
                        if(data!='ndata' && data!='error'){
                            //alert(data);
                            data = JSON.parse(data);
                            imprimirResultadosBusqueda(data);
                        }
                    }

                });
            return false;
        });

        $(document).on('submit', '#formComment', function() {
            $.post("action/addCommentRec.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();

                    }
                });
            return false;
        });

    });

    function cancelComment(){
        var id = $('#idtransac').val();
        var type = $('#type').val();
        var loadType;
        if(type === 'app'){
            loadType = true;
        }else{
            loadType = false;
        }
        get_info_transac(id,loadType)
    }



    function imprimirResultadosBusqueda(data){

        table.fnClearTable();
        $('.conceal').hide();

        var userTypeSel = document.getElementById('tipoUsuario');
        var userType = userTypeSel.options[userTypeSel.selectedIndex].value;



        var id;
        var txt;
        var estado;
        var tipoTransac;

        if (userType === '1') {
            data.forEach(function(e){
                id 	= e['idlogtransaccionalapp'];
                txt ='<a onclick="get_info_transac('+id+',true)" style="padding: 0px 5px 0px 0px; cursor: pointer;">Ver Info</a>';
                if(e['estado'] == '0'){
                    estado = 'Cancelada';
                }else if (e['estado'] == '1'){
                    estado = 'Pendiente';
                }else if (e['estado'] == '2'){
                    estado = 'Completada';
                }

                if(e['tipotrasac'] == 'B'){
                    tipoTransac = 'Bancaria';
                }else if (e['tipotrasac'] == 'W'){
                    tipoTransac = 'Web';
                }

                table.fnAddData( [
                    id,
                    e['correo'],
                    e['nombre']+' '+e['apellido'],
                    e['idreferencia'],
                    e['valortransac'],
                    tipoTransac,
                    estado,
                    e['creacion'],
                    txt
                ]);
            });

        } else {
            data.forEach(function(e){
                id 	= e['idlogtransaccional'];
                txt ='<a onclick="get_info_transac('+id+',false)" style="padding: 0px 5px 0px 0px; cursor: pointer;">Ver Info</a>';
                if(e['estado'] == '0'){
                    estado = 'Cancelada';
                }else if (e['estado'] == '1'){
                    estado = 'Pendiente';
                }else if (e['estado'] == '2'){
                    estado = 'Completada';
                }

                if(e['tipotransac'] == 'B'){
                    tipoTransac = 'Bancaria';
                }else if (e['tipotransac'] == 'W'){
                    tipoTransac = 'Web';
                }

                table.fnAddData( [
                    id,
                    e['correo'],
                    e['nombre']+' '+e['apellido'],
                    e['idreferencia'],
                    e['valortransac'],
                    tipoTransac,
                    estado,
                    e['creacion'],
                    txt
                ]);
            });
        }

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );

    }

    function get_info_transac(id,type){

        $('.conceal').show();

        if(type){
            $.post("json/getTransactionApp.php",{
                id:id
            }, function (data,status){
                if(status =='success'){
                    if (data!='ndata' &&  data !=''){
                        //alert(data);
                        data = JSON.parse(data);
                        $('#idtransac').val(data['idlogtransaccionalapp']);
                        $('#type').val('app');
                        $('#idtransac').text(data['idlogtransaccionalapp']);
                        $('#correo').text(data['correo']);
                        $('#nombreCompleto').text(data['nombreCompleto']);
                        $('#referencia').text(data['idreferencia']);
                        $('#valortransac').text(data['valortransac']);

                        var tipoTransac;
                        var estado;

                        if(data['estado'] == '0'){
                            estado = 'Cancelada';
                        }else if (data['estado'] == '1'){
                            estado = 'Pendiente';
                        }else if (data['estado'] == '2'){
                            estado = 'Completada';
                        }

                        if(data['tipotrasac'] == 'B'){
                            tipoTransac = 'Bancaria';
                        }else if (data['tipotrasac'] == 'W'){
                            tipoTransac = 'Web';
                        }

                        $('#tipoTransac').text(tipoTransac);
                        $('#estado').text(estado);

                        $('#creacion').text(data['creacion']);


                        $('#personapago').text(data['personapago']);
                        $('#lugar').text(data['lugar']);
                        $('#agencia').text(data['agencia']);
                        $('#operador').text(data['operador']);
                        //$('#comentario').val(data['comentario']);

                        var comentario = data['comentario'];
                        var currentDate = moment().format();
                        if(comentario === null){
                            $('#comentario').val('------'+currentDate+'------');
                        }else{
                            $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------');
                        }


                    }
                }
            });

        } else {
            $.post("json/getTransactionPro.php",{
                id:id
            }, function (data,status){
                if(status =='success'){
                    if (data!='ndata' &&  data !=''){
                        //alert(data);
                        data = JSON.parse(data);
                        $('#idtransac').val(data['idlogtransaccional']);
                        $('#type').val('pro');
                        $('#idtransac').text(data['idlogtransaccional']);
                        $('#correo').text(data['correo']);
                        $('#nombreCompleto').text(data['nombreCompleto']);
                        $('#referencia').text(data['idreferencia']);
                        $('#valortransac').text(data['valortransac']);

                        var tipoTransac;
                        var estado;

                        if(data['estado'] == '0'){
                            estado = 'Cancelada';
                        }else if (data['estado'] == '1'){
                            estado = 'Pendiente';
                        }else if (data['estado'] == '2'){
                            estado = 'Completada';
                        }

                        if(data['tipotrasac'] == 'B'){
                            tipoTransac = 'Bancaria';
                        }else if (data['tipotrasac'] == 'W'){
                            tipoTransac = 'Web';
                        }

                        $('#tipoTransac').text(tipoTransac);
                        $('#estado').text(estado);

                        $('#creacion').text(data['creacion']);


                        $('#personapago').text(data['personapago']);
                        $('#lugar').text(data['lugar']);
                        $('#agencia').text(data['agencia']);
                        $('#operador').text(data['operador']);
                        //$('#comentario').val(data['comentario']);
                        var comentario = data['comentario'];
                        var currentDate = moment().format();
                        if(comentario === null){
                            $('#comentario').val('------'+currentDate+'------');
                        }else{
                            $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------');
                        }


                    }
                }
            });
        }
        return false;
    }
    function cancelaction(){
        setTimeout(function(){ window.location.reload(false); }, 2000);

    }
</script>
</body>
</html>
