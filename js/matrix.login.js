
$(document).ready(function(){

	var recover = $('#recoverform');
	var speed = 400;

	$('#to-recover').click(function(){
		
		$("#loginform").slideUp();
		$("#recoverform").fadeIn();
	});
    
    if($.browser.msie == true && $.browser.version.slice(0,3) < 10) {
        $('input[placeholder]').each(function(){ 
		   
			var input = $(this);       
		   
			$(input).val(input.attr('placeholder'));
				   
			$(input).focus(function(){
				 if (input.val() == input.attr('placeholder')) {
					 input.val('');
				 }
			});
		   
			$(input).blur(function(){
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.val(input.attr('placeholder'));
				}
			});
		}); 
    }
  $("#btn-login").click(function() {
		var data = $("#loginform").serialize();
		$.ajax({
			type : 'POST',
			url  : 'action/login.php',
			data : data,
			beforeSend: function(){
				if ($("#mail").val() == "" || $("#pass").val() == "") {
					var title="Faltan datos";
					var text="Necesita ingresar todos los campos para poder continuar";
					var type="warning";
					setmsg(title,text,type);
					return false;
				}
			},
			success :  function(response)	{
				//alert(response);
				var parsed = JSON.parse(response);
				if(parsed.type=="success"){
					setmsg(parsed.title,parsed.text,parsed.type);
					window.location.href ='index.php';
				}else{
					setmsg(parsed.title,parsed.text,parsed.type);
				}
			}
		});
		return false;
	});
	
});

function setmsg(title,text,type){
	swal({   
		title: title,   
		text: text,   
		type: type,   
		confirmButtonText: "Ok" 
	});
}