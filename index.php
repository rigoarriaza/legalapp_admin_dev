<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  session_start();
  include_once 'clases/cConexion.php';
  include_once 'clases/cUsuario.php';
  $database   = new Database();
  $db     = $database->getConnection();
  $oUsuario   = new Usuario($db);
  
  if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
  }
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Legal App</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/fullcalendar.css" />
<link rel="stylesheet" href="css/matrix-style.css" />
<link rel="stylesheet" href="css/matrix-media.css" />
<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<style type="text/css">
    .bg_lh{
        background-color: #925959;
    }

</style>
</head>
<body>
  <?php
require_once('header.php');
?>

 <?php
require_once('menu.php');
?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
        <ul class="site-stats">
          <li class="bg_lh"><i class="icon-user"></i> <strong><span id="UsuariosProPendientes"></span> </strong> <small>Total de usuarios profesionales en estado Pendiente</small></li>
          <li class="bg_lh"><i class="icon-plus"></i> <strong><span id="UsuariosAppActivos"></span> </strong> <small>Total de usuarios "persona natural"</small></li>
          <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong><span id="UsuariosAppPendientes"></span> </strong> <small>Total de usuarios "persona natural" en estado pendiente</small></li>
          <a href="visorPreguntas.php?p=1"><li class="bg_lh" style="background-color: cornflowerblue"><i class="icon-tag"></i> <strong><span id="PreguntasExpPendientes"></span> </strong> <small>Total de preguntas Pendientes este mes</small></li></a>
            <a href="visorPreguntas.php?p=2"><li class="bg_lh"  style="background-color: cornflowerblue"><i class="icon-repeat"></i> <strong><span id="PreguntasDirPendientes"></span> </strong> <small>Total de preguntas directas Pendientes este mes</small></li></a>
          <li class="bg_lh"><i class="icon-globe"></i> <strong><span id="cotiPendiente"></span> </strong> <small>Total de cotizaciones pendientes de contestar</small></li>
          <li class="bg_lh"><i class="icon-globe"></i> <strong><span id="gananciaApp"></span> </strong> <small>Monto total recaudado al mes actual.</small></li>
            <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong><span id="gananciaMember"></span> </strong> <small>Monto ganancias por membresias este mes</small></li>
            <li class="bg_lh"><i class="icon-question-sign"></i> <strong><span id="gananciaPreguntas"></span> </strong> <small>Monto ganancias por preguntas de los usuarios este mes</small></li>
        </ul>
    </div>
<!--End-Action boxes-->    
    </div>
  </div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> <?echo date('Y');?> &copy; Legal App.</div>
</div>

<!--end-Footer-part-->

  <!--end-Footer-part-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.ui.custom.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.uniform.js"></script>
  <script src="js/select2.min.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <script src="js/matrix.js"></script>
  <script src="js/matrix.form_common.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/moment.js"></script>


  <script src="js/sweetalert.min.js"></script>

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}

document.getElementById('1').classList.add('active');

  function  getDashboardData() {

    $.post("json/getDashboard.php", function (data,status){

      //console.log("NDATA", data);
      if(status =='success'){
        if (data!='ndata' &&  data !=''){
          data = JSON.parse(data);

          $("#UsuariosProPendientes").text(data['UsuariosProPendientes']);
          $("#UsuariosAppActivos").text(data['UsuariosAppActivos']);
          $("#UsuariosAppPendientes").text(data['UsuariosAppPendientes']);
          $("#PreguntasExpPendientes").text(data['PreguntasExpPendientes']);
          $("#PreguntasDirPendientes").text(data['PreguntasDirPendientes']);
          $("#cotiPendiente").text(data['cotiPendiente']);

          if(data['gananciaApp'] == null || data['gananciaApp'] == undefined){
            $("#gananciaApp").text('N/A');
          }else {
            $("#gananciaApp").text(data['gananciaApp']);
          }
            if(data['gananciaMember'] == null || data['gananciaMember'] == undefined){
                $("#gananciaMember").text('N/A');
            }else {
                $("#gananciaMember").text(data['gananciaMember']);
            }

            if(data['gananciaPreguntas'] == null || data['gananciaPreguntas'] == undefined){
                $("#gananciaPreguntas").text('N/A');
            }else {
                $("#gananciaPreguntas").text(data['gananciaPreguntas']);
            }


        }
      }
    });

  }

  $(document).ready(function(){

    getDashboardData();

  });

</script>
</body>
</html>
