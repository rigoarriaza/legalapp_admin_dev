<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cPlan.php';
include_once 'clases/cUsuario.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);
$oPlan             	= new Plan($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//Enter modifications

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Planes <?=date('Y-m-d')?></title>
    <?php
    require_once('headerHTML.php');
    ?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Plan</a> <a href="#" class="current">Tipos de plan</a> </div>
        <h1>Planes</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Planes</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formplan" id="formplan" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nplan"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Nombre del plan</label>
                                <div class="controls">
                                    <input type="text" name="nombre" id="nombre" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Costo</label>
                                <div class="controls">
                                    <input type="text" name="costo" id="costo" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Meses Activo</label>
                                <div class="controls">
                                    <select name="mesesActivo" id="mesesActivo">
                                        <option value="1">1 Mes</option>
                                        <option value="12">12 Meses</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Descripción del Plan</label>
                                <div class="controls">
                                    <input type="text" name="descPlan" id="descPlan" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Tipo de Plan</label>
                                <div class="controls">
                                    <select name="tipoPlan" id="tipoPlan">
                                        <option value="1">Para Profesionales</option>
                                        <option value="2">Para Firma</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Cantidad de Especialidades</label>
                                <div class="controls">
                                    <input type="number" name="numEspecialidad" id="numEspecialidad" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group conceal">
                                <label class="control-label">Cantidad de Cuentas</label>
                                <div class="controls">
                                    <input type="number" name="numCuentas" id="numCuentas" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Permite Ubicación</label>
                                <div class="controls">
                                    <select name="ubicacion" id="ubicacion">
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Permite Biblioteca</label>
                                <div class="controls">
                                    <select name="biblioteca" id="biblioteca">
                                        <option value="1">Si</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado</label>
                                <div class="controls">
                                    <select name="estado" id="estado">
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Planes Existentes</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Costo</th>
                                <th>Tipo</th>
                                <th>Meses Activo</th>
                                <th>Estado</th>
                                <th>Fecha de Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;


    //FUNCTION TO SHOW THE REST OF THE SCRIPT

    $('#tipoPlan').on('change', function() {
        var tpVal = $('#tipoPlan').val();

        if(tpVal == '1'){
            $('.conceal').css('display','none');
        }else{
            $('.conceal').css('display','block');
        }
    });

    $(document).ready(function(){

        $('.conceal').css('display','none');

        // ADD active state to current option
        var currentSel = $('#7');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#proAccor').show();

        getplanes(false);

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );


        $(document).on('submit', '#formplan', function() {
            $.post("action/createplan.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        $('#formplan').trigger("reset");
                        $('#opt').val('nplan');
                        $('#btnaction').val('Guardar');

                        var tpVal = $('#tipoPlan').val();
                        if(tpVal == '1'){
                            $('.conceal').css('display','none');
                        }else{
                            $('.conceal').css('display','block');
                        }
                    }
                    getplanes(true);
                });
            return false;
        });
    });


    function getplanes(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getPlanes.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idplan'];
            var txt ='<a onclick="get_info_plan('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
            table.fnAddData( [
                id,
                e['nombre'],
                e['costo'],
                e['aplicacion_plan'],
                e['mesesactivo'],
                e['estado'],
                e['creacion'],
                txt
            ]);
        });
    }

    function get_info_plan(id){
        $.post("json/getPlan.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //alert(data);
                    data = JSON.parse(data);
                    $('#opt').val('mplan');
                    $('#id').val(data['idplan']);
                    $('#nombre').val(data['nombreplan']);
                    $('#costo').val(data['costo']);
                    $('#descPlan').val(data['tiempo_cobro']);

                    var aplicacion_plan = data['aplicacion_plan'];
                    document.getElementById("tipoPlan").value  = aplicacion_plan;


                    var mesesactivo = data['mesesactivo'];
                        document.getElementById("mesesActivo").value  = mesesactivo;
                        $('#numEspecialidad').val(data['especialidades']);
                        
                        var ubicacion = data['ubicacion'];
                        document.getElementById("ubicacion").value  = ubicacion;
                        var biblioteca = data['biblioteca'];
                        document.getElementById("biblioteca").value  = biblioteca;


                    if(aplicacion_plan == '2'){
                        $('#numCuentas').val(data['numero_cuentas']);

                        
                    }

                    var estado = data['estado'];
                    document.getElementById("estado").value  = estado;

                    $('#btnaction').val('Modificar');

                    var tpVal = $('#tipoPlan').val();

                    if(tpVal == '1'){
                        $('.conceal').css('display','none');
                    }else{
                        $('.conceal').css('display','block');
                    }

                }
            }
        });
        return false;
    }
    function cancelaction(){
        $('#formplan').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#opt').val('nplan');

        var tpVal = $('#tipoPlan').val();
        if(tpVal == '1'){
            $('.conceal').css('display','none');
        }else{
            $('.conceal').css('display','block');
        }
    }
</script>
</body>
</html>
