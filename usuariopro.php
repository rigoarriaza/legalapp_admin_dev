<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cUsuariopro.php';
include_once 'clases/cUsuario.php';
include_once 'clases/cPlan.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);
$oUsuariopro       	= new Usuariopro($db);
$oPlan             	= new Plan($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//TODO: Add log when a user change something or add something in here

//Enter modifications
$usuariosPro    = $oUsuariopro->get_all_userpro();
$planes 	= $oPlan->get_all_planespro();
$webName = $_SESSION['legalapp']['nombreCompleto'].' ('.$_SESSION['legalapp']['correo'].')';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
            color: blue !important;
        }
        .control-label{
            font-weight: bold;
            color: black !important;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }

        .swal-modal {
            overflow: scroll;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Regresar" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Manejo de usuario profesional</a> <a href="#" class="current">Revisión de usuario</a> </div>
        <h1>Asignación de planes y aprobación Para usuarios profesionales</h1>
        <!---->
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Vista de usuarios profesionales</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre de usuario</th>
                                <th>Correo</th>
                                <th>Carnet</th>
                                <th>Estado Verificacion</th>
                                <th>Estado Usuario</th>
                                <th>Fecha de Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Manejar usuario profesional</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formpro" id="formpro" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value=""/>
                            <input type="hidden" id="optP" name="optP" value="nplan"/>
                            <input type="hidden" id="idmembresia" name="idmembresia" value=""/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <input type="hidden" id="currentPlan" name="currentPlan" value=""/>
                            <input type="hidden" id="webName" name="webName" value="<?=$webName?>"/>
                            <input type="hidden" id="nombrePago" name="nombrePago" value=""/>
                            <input type="hidden" id="curFecInicio" name="curFecInicio" value=""/>
                            <input type="hidden" id="curFecFin" name="curFecFin" value=""/>
                            <input type="hidden" id="mailPro" name="mailPro" value=""/>
                            <input type="hidden" id="nombreproVALUE" name="nombreproVALUE" value=""/>
                            <div class="control-group">
                                <label class="control-label">Seleccione Usuario Profesional</label>
                                <div class="controls">
                                    <select id="userpro" name="userpro">
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        if($usuariosPro){
                                            forEach($usuariosPro as $val1){
                                                $nombreCompleto = $val1['nombre'].' '.$val1['apellido'];
                                                echo '<option value="'.$val1['idusuarioprofesional'].'">'.$nombreCompleto.' - '.$val1['correo'].'</option>';
                                            }
                                        }else{
                                            echo '<option value="--">NO HAY DATOS</option>';
                                        }

                                        ?>
                                    </select>
                                   </div>
                            </div>
                            <div class="conceal">
                                <h3>Datos de aprobación de usuario</h3>
                                <div class="control-group">
                                    <label class="control-label">ID Profesional</label>
                                    <div class="controls">
                                        <span id="idprodisplay" name="idprodisplay" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Nombre de profesional</label>
                                    <div class="controls">
                                        <span id="nombrepro" name="nombrepro" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Correo</label>
                                    <div class="controls">
                                         <span id="correo" name="correo" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Teléfono Móvil</label>
                                    <div class="controls">
                                        <span id="celular" name="celular" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Dirección</label>
                                    <div class="controls">
                                        <span id="direccion" name="direccion" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Carnet</label>
                                    <div class="controls">
                                        <span id="carnet" name="carnet" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Referencia</label>
                                    <div class="controls">
                                        <span id="referencia" name="referencia" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Foto Personal</label>
                                    <div class="controls">
                                        <img id="fotoPer" name="fotoPer" src="" style="height: 150px; width: 150px;" />
                                        <p id="messageImg" name="messageImg" style="display: none; font-style: italic; font-size: smaller">No hay imagen para mostrar</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Descripción</label>
                                    <div class="controls">
                                        <span id="descripcion" name="descripcion" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Creación</label>
                                    <div class="controls">
                                        <span id="creacion" name="creacion" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Título</label>
                                    <div class="controls">
                                        <span id="titulo" name="titulo" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Documento presentado</label>
                                    <div class="controls">
                                        <img id="fotoDoc" name="fotoDoc" src="" style="height: 150px; width: 150px;" />
                                        <p id="messageImgDoc" name="messageImgDoc" style="display: none; font-style: italic; font-size: smaller">No hay imagen para mostrar</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Número de Registro</label>
                                    <div class="controls">
                                        <span id="registro" name="registro" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Destacado</label>
                                    <div class="controls">
                                        <select name="destacado" id="destacado">
                                            <option value="0">No</option>
                                            <option value="1">Si</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Estado de Usuario</label>
                                    <div class="controls">
                                        <select name="estadoUsuario" id="estadoUsuario">
                                            <option value="1">Pendiente</option>
                                            <option value="2">Activo</option>
                                            <option value="3">Inactivo</option>
                                            <option value="4">Restringido</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Verificación de Usuario</label>
                                    <div class="controls">
                                        <select name="estadoVerificacion" id="estadoVerificacion">
                                            <option value="0">No Verificado</option>
                                            <option value="1">Verificado</option>
                                        </select>
                                    </div>
                                </div>
                                <h3>Datos de plan</h3>
                                <p id="messageFirma" name="messagefirma" style="display: none; font-style: italic; font-size: smaller"></p>
                            <div class="control-group">
                                <label class="control-label">Seleccione Plan</label>
                                <div class="controls">
                                    <select id="plan" name="plan">
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        //TODO: hacer que no use meses activo, pero un post jquery para sacar los datos que necesita
                                        if($planes){
                                            forEach($planes as $val1){
                                                echo '<option value="'.$val1['idplan'].'">'.$val1['nombreplan'].' ['.$val1['mesesactivo'].' Mes(es) Bs. '.$val1['costo'].']</option>';
                                            }
                                        }else{
                                            echo '<option value="">NO HAY DATOS</option>';
                                        }

                                        ?>
                                    </select>
                                    <span id="planFirmaText"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de Compra (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/d/Y')?>" class="input-append date datepicker">
                                        <input id="fecInicio" name="fecInicio" type="text" value=""  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de finalización (Se calcula automáticamente)</label>
                                <div class="controls">
                                    <input type="text" name="fecFin" id="fecFin" class="inputformtext" readonly="readonly">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado del Plan</label>
                                <div class="controls">
                                    <select name="estado" id="estado">
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>


<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    function conceal(id){

        if (id == 0){
            var tpVal = $("#userpro").val();
        } else {
            var tpVal = id;
            $('#userpro').select2('val', id);
        }

        if(tpVal == ''){
            $('.conceal').css('display','none');
        }else{
            $('.conceal').css('display','block');
        }
    }

    function  calculateDate() {
        var dateIni = $("#fecInicio").val();
        if ($('#curFecInicio').val() == '') $("#curFecInicio").val(dateIni);

        var parts =dateIni.split('/');
        //please put attention to the month (parts[0]), Javascript counts months from 0:
        // January - 0, February - 1, etc
        var mydate = new Date(parts[2],parts[0]-1,parts[1]);
        var lafecha = moment(mydate);
        var idplanTemp = $("#plan").val();
        var meses = '';
        // GET the amount of months in the plan
        $.post("json/getMes.php",{
            id:idplanTemp
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //alert(data);
                    data = JSON.parse(data);

                    meses = data['mesesactivo'];
                    meses = parseInt(meses);

                    var dateFin = lafecha.add(meses,'months');
                    dateFin = moment(dateFin).format('MM/DD/YYYY');
                    $('#fecFin').val(dateFin);
                    if ($('#curFecFin').val() == '') $("#curFecFin").val(dateFin);
                }
            }
        });


    }

    function  getAsignacion(id) {

        if(id == 0){
            var userpro = $("#userpro").val();
        } else {
            var userpro = id;
            $('#userpro').select2('val', id);
        }

        var plan            = '';
        var id              = '';
        var fecInicio       = '';
        var estado          = '';
        var creacion        = '';

        var firma = '';
        var nombrePlan = '';
        var costo = '';
        var mesesActivo = '';
        var newMessage = false;

        //check si tiene plan con alguna firma
        $.post("json/getFirmaAndPro.php",{
            id:userpro
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){

                    data = JSON.parse(data);

                    firma = data['nombre'];
                    costo = data['costo'];
                    mesesActivo = data['mesesactivo'];
                    nombrePlan = data['nombreplan'];
                    newMessage=true;
                    //Actual Asignación
                    $.post("json/getAsignacionPro.php",{
                        id:userpro
                    }, function (data2,status2){
                        if(status2 =='success'){
                            if (data2!='ndata' &&  data2 !=''){


                                data2 = JSON.parse(data2);
                                //alert(data2['idplan']);
                                $("#idmembresia").val(data2['idmembresia']);

                               // $("#plan").select2('val', data2['idplan']);
                                $("#plan").select2("readonly",true);


                                fecInicio = data2['fechacompra'];
                                fecFin = data2['fechavencimiento'];

                                //var partsInicio = fecInicio.split('-');
                                //please put attention to the month (parts[0]), Javascript counts months from 0:
                                // January - 0, February - 1, etc
                                //var fecInicio = new Date(partsInicio[0],partsInicio[1]-1,partsInicio[2]);
                                var fecInicio = new Date(fecInicio);

                                fecInicio = moment(fecInicio).format('MM/DD/YYYY');

                                var fecFin = new Date(fecFin);

                                fecFin = moment(fecFin).format('MM/DD/YYYY');


                                $("#fecInicio").val(fecInicio);
                                $("#fecFin").val(fecFin);
                                $(".add-on").hide();
                                creacion = data2['creacion'];

                                calculateDate();

                                $("#estado").select2('val', data2['estado']);
                                $("#estado").prop("disabled",true);

                                $('#messageFirma').css('display','block');
                                $("#messageFirma").text('Plan: Usuario profesional pertenece a Firma:'+firma+', con un plan tipo '+nombrePlan+' (Bs.'+costo+') por un período de '+mesesActivo+' mes(es),por lo que NO se puede modificar hasta que sea removido de esa Firma.');

                                $('#optP').val('planfirma');


                                $('#btnaction').val('');
                                $('#btnaction').val('Modificar');

                            }else {
                                $('#btnaction').val('');
                                $('#optP').val('nplan');
                                $('#btnaction').val('Guardar');
                                $('#idmembresia').val('');
                                $('#plan ').select2('val', '');
                                $('#fecInicio').val('');
                                $('#fecFin').val('');
                                $('#estado').val('1');
                                $('#messageFirma').css('display','block');
                                $("#messageFirma").text('Plan: Nuevo plan para este usuario profesional.');
                            }
                        }
                    });

                }else {
                    //Actual Asignación
                    $.post("json/getAsignacionPro.php",{
                        id:userpro
                    }, function (data,status){
                        if(status =='success'){
                            if (data!='ndata' &&  data !=''){

                                data = JSON.parse(data);

                                $("#idmembresia").val(data['idmembresia']);
                                $("#currentPlan").val(data['idplan']);
                                $("#plan").select2('val', data['idplan']);
                                $("#plan").select2("readonly",false);
                                fecInicio = data['fechacompra'];

                                //var partsInicio = fecInicio.split('-');
                                //please put attention to the month (parts[0]), Javascript counts months from 0:
                                // January - 0, February - 1, etc
                                //var fecInicio = new Date(partsInicio[0],partsInicio[1]-1,partsInicio[2]);
                                var fecInicio = new Date(fecInicio);

                                fecInicio = moment(fecInicio).format('MM/DD/YYYY');

                                $("#fecInicio").val(fecInicio);
                                $("#curFecInicio").val(fecInicio);
                                $(".add-on").show();
                                creacion = data['creacion'];

                                calculateDate();

                                $("#estado").select2('val', data['estado']);
                                $("#estado").prop("disabled",false);

                                $('#messageFirma').css('display','block');
                                $("#messageFirma").text('Plan: Plan existente para este usuario profesional, creado el: '+creacion);

                                $('#optP').val('mplan');


                                $('#btnaction').val('');
                                $('#btnaction').val('Modificar');

                            }else {
                                $('#btnaction').val('');
                                $('#optP').val('nplan');
                                $('#btnaction').val('Guardar');
                                $('#idmembresia').val('');
                                $('#plan ').select2('val', '');
                                $('#fecInicio').val('');
                                $('#fecFin').val('');
                                $('#estado').val('1');
                                $('#messageFirma').css('display','block');
                                $("#messageFirma").text('Plan: Nuevo plan para este usuario profesional.');
                            }
                        }
                    });
                }
            }
        });




    }

    function  getUserInfo(id) {
        if(id == 0){
            var userpro                 = $("#userpro").val();
        } else {
            var userpro = id;
            $('#userpro').select2('val', id);
        }
        var nombre                  = "";
        var apellido                = "";
        var nombrecompleto          = "";
        var fotopersonal            = "";
        var documento               = "";

        $.post("json/getUserPro.php",{
            id:userpro
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    data = JSON.parse(data);

                    $("#opt").val('mpro');

                    $("#id").val(data['idusuarioprofesional']);
                    nombre      = data['nombre'];
                    apellido    = data['apellido'];
                    nombrecompleto = nombre+' '+apellido;
                    $("#idprodisplay").text(data['idusuarioprofesional']);
                    $("#nombrepro").text(nombrecompleto);
                    $("#nombrePago").val(nombrecompleto);
                    $("#correo").text(data['correo']);
                    $("#mailPro").val(data['correo']);
                    $("#nombreproVALUE").val(nombrecompleto);
                    $("#celular").text(data['celular']);
                    $("#direccion").text(data['direccion']);
                    $("#carnet").text(data['carnet']);
                    $("#referencia").text(data['referencia']);

                    fotopersonal = 'http://legalbo.com/legalapp/IMGUSERS/'+data['fotopersonal'];
                    documento = 'http://legalbo.com/legalapp/docprofesional/'+data['documento'];

                    if(data['fotopersonal'] !== ""){
                        $('#fotoPer').attr('src',fotopersonal);
                        $('#fotoPer').css('display','block');
                        $('#messageImg').css('display','none');
                    }else{
                        $('#fotoPer').css('display','none');
                        $('#messageImg').css('display','block');
                    }

                    if(data['documento'] !== "" && data['documento'] !== null){
                        $('#fotoDoc').attr('src',documento);
                        $('#fotoDoc').css('display','block');
                        $('#messageImgDoc').css('display','none');
                    }else{
                        $('#fotoDoc').css('display','none');
                        $('#messageImgDoc').css('display','block');
                    }


                    $("#descripcion").text(data['descripcionprofesional']);
                    $("#creacion").text(data['creacion']);
                    $("#titulo").text(data['titulo']);
                    $("#registro").text(data['numero_registro']);
                    $("#destacado").select2('val', data['destacado']);
                    $("#estadoUsuario").select2('val', data['idestadousuario']);
                    $("#estadoVerificacion").select2('val', data['estadoverificacion']);

                    $('#btnaction').val('');
                    $('#btnaction').val('Modificar');

                }
            }
        });

    }

    $(document).ready(function(){

        $('.conceal').css('display','none');

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );


        var iduser = <?=isset($_REQUEST['id']) ? $_REQUEST['id'] : 0 ; ?>;

        if ( iduser != 0 ){
            getUserInfoTotal(iduser);
        }else{
            getusuarios(false);
        }



        // table.page(2).draw( 'page' );



        // var table = $('#example').DataTable();
        
        // $('#next').on( 'click', function () {
        //     table.page( 'next' ).draw( 'page' );
        // } );
        
        // $('#previous').on( 'click', function () {
        //     table.page( 'previous' ).draw( 'page' );
        // } );
        //add the option to load the plan right on the change of firm

        $('#userpro').on('change', function() {

            conceal(0);
            var sel = $('#userpro').val();

            if(sel !== ""){

                getUserInfo(0);
                getAsignacion(0);
            }else{
                cancelaction();
            }
        });
        calculateDate();

        $('.datepicker').on('hide', function() {
            calculateDate();
        });

        $('.datepicker').on("changeDate", function(e) {
            calculateDate();
        });

        $('#plan').on('change', function() {
            var fechaIni = $('#fecInicio').val();
            if(fechaIni !== ''){
                calculateDate();
            }
        });

        // ADD active state to current option
        var currentSel = $('#11');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#proAccor').show();


        //Submitting

        $(document).on('submit', '#formpro', function(e) {

            e.preventDefault();
            var form = $(this).parents('form');

            swal({
                title: "¿Está seguro de guardar este cambio?",
                text: "Si se cambia el plan o se guarda uno nuevo se agregará una transacción, sujeta a revisión contable. ¿Desea continuar?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No, Cancelar.",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo continuar.",
                closeOnConfirm: true
            }, function(isConfirm){
                if(isConfirm){
                    $.post("action/createUsuarioPro.php", $('#formpro').serialize())
                        .done(function(data) {
                            console.log(data);
                            var parsed = JSON.parse(data);
                            swal({
                                title: parsed.title,
                                text: parsed.text,
                                type: parsed.type,
                                confirmButtonText: "Ok"
                            });
                            if(parsed.type=='success'){
                                cancelaction();

                            }
                    });

                }
            });


            return false;
        });
    });

    function getusuarios(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getUsuariosPro.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idusuarioprofesional'];
            var txt ='<a onclick="getUserInfoTotal('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;" class="text-data">Revisar</a>';

            var nombreEstado;
            var estadoVerificacion;

            switch (e['idestadousuario']) {
                case '0':
                    nombreEstado = 'Pendiente';
                    break;
                case '1':
                    nombreEstado = 'Pendiente';
                    break;
                case '2':
                    nombreEstado = 'Activo';
                    break;
                case '3':
                    nombreEstado = 'Inactivo';
                    break;
                case '4':
                    nombreEstado = 'Restringido';
                    break;
                default:
                    nombreEstado = 'N/A';
            }

            if(e['estadoVerificacion'] == '0'){
                estadoVerificacion = 'No Verificado';
            } else {
                estadoVerificacion = 'Verificado';
            }

            table.fnAddData( [
                id,
                e['nombre']+' '+e['apellido'],
                e['correo'],
                e['carnet'],
                estadoVerificacion,
                nombreEstado,
                e['creacion'],
                txt
            ]);
        });
    }

    function getUserInfoTotal (id){
        conceal(id);
        var sel = id;
            getUserInfo(sel);
            getAsignacion(sel);
    }

    function prueba(){
        table.page(2).draw( 'page' );
    }
    function cancelaction(){
        $('#formpro').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#id').val('');
        $("#firma").select2('val', '');
        $('#plan ').select2('val', '');
        $('#fecInicio').val('');
        $('#fecFin').val('');
        $('#estado ').select2('val', '');
        $('#opt').val('');
        $('#messageFirma').css('display','none');
        $('#userpro').select2('val', '');
        conceal(0);
    }
</script>
</body>
</html>
