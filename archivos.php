<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	session_start();
	include_once 'clases/cConexion.php';
	include_once 'clases/cEspecialidad.php';
	include_once 'clases/cUsuario.php';
	$database 			= new Database();
	$db 				= $database->getConnection();
	$oUsuario   		= new Usuario($db);
	$oEspecialidad   	= new Especialidad($db);
	
  if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
  }

  $estados 	= $oUsuario->getEstadosUsuario();
  $roles 	= $oUsuario->getRoles();

  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Archivos Descargables <?=date('Y-m-d')?></title>
 <?php
require_once('headerHTML.php');
?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="especialidades.php">Especialidades</a> <a href="#" class="current">Agregar nueva especialidad</a> </div>
    <h1>Archivos descargables</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Archivos</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" name="form" id="form" novalidate="novalidate">
              <input type="hidden" id="opt" name="opt" value="nArchivo"/>
              <input type="hidden" id="id" name="id" value=""/>
              <input type="hidden" id="oldfile" name="oldfile" value=""/>
			  <div class="control-group">
                <label class="control-label">Nombre </label>
                <div class="controls">
                  <input type="text" name="nombre" id="nombre" class="span9">
                </div>
              </div>	
				 <div class="control-group" id="div_imagen2">
					<label class="control-label">Archivo</label>
					<div class="controls">
						<input name="archivo" id="archivo" type="file"  class="span9"/>
					</div>
				</div>
				<div class="control-group">
                <label class="control-label">Especialidad</label>
                <div class="controls">
                  <select class="form-control" name='especialidad' id='especialidad' class="span9">
						<option value ='0'>Seleccione</option>
					</select>
                </div>
              </div>
			   <div class="control-group">
                <label class="control-label">Tipo de archivo</label>
                <div class="controls">
                 <select class="form-control" name='tipo' id='tipo' class="span9">
						<option value ='0'>Seleccione</option>
					</select>
                </div>
              </div>
               <div class="control-group">
                <label class="control-label">Estado</label>
                <div class="controls">
                  <select name="estado" id="estado">
						<option value="1" selected="selected">Activo</option>
						<option value="0" >Inactivo</option>
					</select>
                </div>
              </div>
			   <div class="form-actions">
                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid"><hr>
	  <div class="row-fluid">
		  <div class="span12">
			  <div class="widget-box">
				  <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
					<h5>Usuarios registrados</h5>
				  </div>
				  <div class="widget-content nopadding">
					<table class="table table-bordered data-table" id="table1">
					  <thead>
						<tr>
						  <th>ID</th>
						  <th>Nombre</th>
						  <th>Especialidad</th>
						  <th>Tipo</th>
						  <th>Estado</th>
						  <th>Opciones</th>
						</tr>
					  </thead>
					  <tbody>
					  </tbody>
					</table>
				  </div>
				</div>
		  </div>
	  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script> 

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
var table;
$(document).ready(function(){
 // ADD active state to current option
 var currentSel = $('#6B');
 if(!currentSel.hasClass('active')){
 		currentSel.addClass('active');
	}
    $('#configAccor').show();
	getTiposA();
	getTiposE();
	getData(false);


    table = $('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "sDom": '<""l>t<"F"fp>',
        "aaSorting": [],
        "buttons": [
            'csv', 'excel'
        ]
    });

    table.api().buttons().container()
        .insertBefore( '#table1_filter' );

	$(document).on('submit', '#form', function() {
		$.ajax({
			  url: "action/archivos.php",
			  type: "POST",
			  data:  new FormData(this),
			  contentType: false,
			  cache: false,
			  processData:false,
			  beforeSend : function(){
			  },
			  success: function(data) {
				  var parsed = JSON.parse(data);
				  swal({
				   title: parsed.title,
				   text: parsed.text,
				   type: parsed.type,
				   confirmButtonText: "Ok"
				  });
				  if(parsed.type=='success'){
					$('#form').trigger("reset");
					$('#opt').val('nArchivo');
					$('#btnaction').val('Guardar');
				}
					getData(true);
				},
				error: function(e) {
				  swal({
				   title: "Error!",
				   text: e,
				   type: "error",
				   confirmButtonText: "Ok"
				  });
				}
			});
			return false;
	});
});

function getTiposE(){
	$.post("json/getEspecialidades2.php", {
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			data = JSON.parse(data);
			create_tiposE(data);
		}
	});
}
function create_tiposE(data){
	data.forEach(function (element) {
		$("#especialidad").append($('<option>', {
			value: element.idespecialidad,
			text: element.nombre
		}));
	});
}
function getTiposA(){
	$.post("json/getTiposarchivo.php", {
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			data = JSON.parse(data);
			create_tiposA(data);
		}
	});
}
function create_tiposA(data){
	data.forEach(function (element) {
		$("#tipo").append($('<option>', {
			value: element.idtipoarchivo,
			text: element.nombre
		}));
	});
}
function getData(v){
	if(v){
		table.fnClearTable();
	}
	$.post("json/getArchivos.php", {
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			if(data!='ndata' && data!='error'){
				//alert(data);
				data = JSON.parse(data);
				imprimirtabla(data);
			}
		}
	});
}

function imprimirtabla(data){
	data.forEach(function(e){
		var id 	= e['idarchivo'];
		var estadoLabel = '';
		if(e['estado'] === '1'){
			estadoLabel = 'Activo';
		}else{
			estadoLabel = 'Inactivo';
		}
		var txt ='<a onclick="get_info_espe('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
		table.fnAddData( [ 
			id,
			e['nombre'],
			e['especialidad'],
			e['tipoarchivo'],
			estadoLabel,
			txt
		]); 
	});
}

function get_info_espe(id){
	$.post("json/getArchivo.php",{
		id:id
	}, function (data,status){
		if(status =='success'){
			if (data!='ndata' &&  data !=''){
				//console.log(data);
				data = JSON.parse(data);
				$('#opt').val('mArchivo');
				$('#id').val(data['idarchivo']);
				$('#nombre').val(data['nombre']);
				$('#oldfile').val(data['url']);
				$('#btnaction').val('Modificar');
				var estado = data['estado'];
				document.getElementById("estado").value  = estado;
				var especialidad = data['idespecialidad'];
				document.getElementById("especialidad").value  = estado;
				var tipo = data['idtipoarchivo'];
				document.getElementById("tipo").value  = estado;
			}
		}
	});
	return false;
}
function cancelaction(){
	$('#form').trigger("reset");
	$('#btnaction').val('');
	$('#btnaction').val('Guardar');
	$('#opt').val('nArchivo');
}
</script>
</body>
</html>
