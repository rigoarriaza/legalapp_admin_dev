<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cTransac.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oTransac   	= new Transac($db);


// set values
if(($_POST['mail'])!=''){
    $oTransac->mail = $_POST['mail'];;
}

if(($_POST['fecInicio'])!=''){
    $oTransac->fecInicio = $_POST['fecInicio'];;
}

if(($_POST['fecFin'])!=''){
    $oTransac->fecFin = $_POST['fecFin'];;
}

if(($_POST['tipoTrans'])!=''){
    $oTransac->tipoTrans = $_POST['tipoTrans'];;
}

if(($_POST['estadoTrans'])!=''){
    $oTransac->estadoTrans = $_POST['estadoTrans'];;
}

if(($_POST['tipoUsuario'])!=''){
    $oTransac->tipoUsuario = $_POST['tipoUsuario'];;
}

try{
    $results = $oTransac->getSearch();
        if($results){
            echo json_encode($results);
        }else{
            echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar almacenar la información", "type" => "error"),JSON_UNESCAPED_UNICODE);
        }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}