<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 		                    = new Database();
$db 			                    = $database->getConnection();
$oPlan       	                    = new Plan($db);
$oPlan->idusuarioprofesional        = $_POST['id'];
$checkPlan                          = $oPlan->get_planFirmaAndPro();
if($checkPlan){
    echo json_encode($checkPlan);
}else{
    echo 'ndata';
}