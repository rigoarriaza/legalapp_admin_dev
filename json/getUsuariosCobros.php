<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuarioCobro.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oUsuarioCobro   	= new UsuarioCobro($db);
$object 		= $oUsuarioCobro->get_all_userCobro();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idusuario']			= $element['idusuario_cobro'];
        $array[$i]['user']			    = $element['username'];
        $array[$i]['llave']			    = $element['secret_key'];
        $array[$i]['org']			    = $element['organizacion'];
        $array[$i]['creacion']			= $element['fecha_creacion'];
        $array[$i]['estado']			= $element['estado'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}