<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuario.php';
include_once '../clases/cDashboard.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oDash   	= new Dashboard($db);
$object 		= $oDash->getStatistcs();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}