<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cAprobarCalificacion.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oCalificacion 	= new AprobarC($db);
$object 		= $oCalificacion->get_all_cali();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idcalificacion_pregunta_express']	= $element['idcalificacion_pregunta_express'];
        $array[$i]['idrespuestapregunta']		        = $element['idrespuestapregunta'];
        $array[$i]['calificacion']		                = $element['calificacion'];
        $array[$i]['comentario']		                = $element['comentario'];
        $array[$i]['creacion']		                    = $element['creacion'];
        $array[$i]['estado']			                = $element['estado'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}