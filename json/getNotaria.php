<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cNotaria.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oNotaria       	= new Notaria($db);
$oNotaria->idnotaria = $_POST['id'];
$object 		= $oNotaria->get_nota();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}