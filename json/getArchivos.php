<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cArchivos.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oData   		= new Archivo($db);
$object 		= $oData->get_all_archivo();
if($object){
	echo json_encode($object);
}else{
	echo 'ndata';
}