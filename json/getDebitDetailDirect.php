<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cTransac.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oTransac       	            = new Transac($db);
$oTransac->idlog_debito_usuarioapp_pd  = $_POST['id'];
$object 		                = $oTransac->getDetailDirecta();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}