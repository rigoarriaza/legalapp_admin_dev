<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cInventario.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oInventario  	= new Inventario($db);

$array			= array();
if(!isset($_POST['id'])){
	echo 'ndata';
	exit();
}
if(!isset($_POST['date'])){
	$fecha 		= date('Y-m-d');
}else{
	$fecha 		= $_POST['date'];
}

$oInventario->idusuario = $_POST['id'];
$object 				= $oInventario->get_productos();

if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idproducto']	= $element['idproducto'];
		$array[$i]['cantidad']		= $element['cantidad'];
		$array[$i]['nombre']		= $element['nombre'];
		$array[$i]['codigo']		= $element['codigo'];
		$oInventario->fecha			= $fecha;
		$oInventario->idproducto	= $element['idproducto'];
		$object2 	= $oInventario->get_inventario_vendedor();
		if($object2){
			$array[$i]['cantidad_dia'] = $object2;
		}else{
			$array[$i]['cantidad_dia'] = 0;
		}
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}