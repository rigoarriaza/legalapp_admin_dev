<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cInventario.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oInventario  	= new Inventario($db);
$object 		= $oInventario->get_productos();
$array			= array();
if(!isset($_POST['fecha'])){
	$fecha = date('Y-m-d');
}else{
	$fecha = $_POST['fecha'];
}
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idproducto']	= $element['idproducto'];
		$array[$i]['cantidad']		= $element['cantidad'];
		$array[$i]['nombre']		= $element['nombre'];
		$array[$i]['codigo']		= $element['codigo'];
		$oInventario->fecha			= $fecha;
		$oInventario->idproducto	= $element['idproducto'];
		$object2 	= $oInventario->verificar_inventario_fecha2();
		if($object2){
			$array[$i]['cantidad_dia'] = $object2;
		}else{
			$array[$i]['cantidad_dia'] = 0;
		}
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}