<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPedidos.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oObject  		= new Pedidos($db);

$array			= array();
if(!isset($_POST['id'])){
	echo json_encode(array("title" => "Error", "text" => "Problema para obtener la informacion del pedido", "type" => "error"));
	exit();
}

$oObject->idventa 	= $_POST['id'];
$object 			= $oObject->get_detalle_venta();

if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['iddetalle_compra_producto']	= $element['iddetalle_compra_producto'];
		$array[$i]['cantidad']					= $element['cantidad'];
		$array[$i]['idventa']					= $element['idventa'];
		$array[$i]['idproducto']				= $element['idproducto'];
		$array[$i]['nombre']					= $element['nombre'];
		$array[$i]['precio']					= $element['precio'];
		$array[$i]['tamanio']					= $element['tamanio'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo json_encode(array("title" => "Error", "text" => "No hay informacion del detalle de la compra", "type" => "error"));
}