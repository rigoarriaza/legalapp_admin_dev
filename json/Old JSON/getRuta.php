<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cRuta.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oData   			= new Ruta($db);
$oData->idusuario 	= $_POST['id'];
$object 			= $oData->get_detalle_vendedor_ruta();
if($object){
	echo json_encode($object);
}else{
	echo 'ndata';
}