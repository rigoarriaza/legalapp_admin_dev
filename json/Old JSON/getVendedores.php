<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cRuta.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oData   		= new Ruta($db);
$object 		= $oData->get_usuarios();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idusuario']		= $element['idusuario'];
		$array[$i]['nombre']		= $element['nombre'];
		$array[$i]['apellido']		= $element['apellido'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}