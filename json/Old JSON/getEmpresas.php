<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cEmpresa.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oEmpresa  		= new Empresa($db);
$object 		= $oEmpresa->get_all_empresa();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idempresa']			= $element['idempresa'];
		$array[$i]['nombre_empresa']	= $element['nombre_empresa'];
		$array[$i]['direccion']			= $element['direccion'];
		$array[$i]['departamento']		= $element['departamento'];
		$array[$i]['municipio']			= $element['municipio'];
		$array[$i]['telefono1']			= $element['telefono1'];
		$array[$i]['telefono2']			= $element['telefono2'];
		$array[$i]['nit']				= $element['nit'];
		$array[$i]['numero_registro']	= $element['numero_registro'];
		$array[$i]['giro']				= $element['giro'];
		$array[$i]['tipo_documento']	= $element['tipo_documento'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}