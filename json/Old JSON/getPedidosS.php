<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPedidos.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oData  		= new Pedidos($db);
if(!isset($_POST['date'])){
	$fecha = date('Y-m-d');
}else{
	$fecha = $_POST['date'];
}
$oData->fecha	= $fecha;
$object 		= $oData->get_peticiones_venta2();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idventa']			= $element['idventa'];
		$array[$i]['fecha']				= $element['fecha'];
		$array[$i]['tipo_documento']	= $element['tipo_documento'];
		$array[$i]['estado']			= $element['estado'];
		$array[$i]['idusuario']			= $element['idusuario'];
		$array[$i]['nombre']			= $element['nombre'];
		$array[$i]['apellido']			= $element['apellido'];
		$array[$i]['telefono']			= $element['telefono'];
		$array[$i]['correo_electronico']= $element['correo_electronico'];
		$array[$i]['estado']			= $element['estado'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}