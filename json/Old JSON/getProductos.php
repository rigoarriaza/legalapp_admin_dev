<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cProducto.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oData   		= new Producto($db);
$object 		= $oData->get_all_product();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idproducto']	= $element['idproducto'];
		$array[$i]['nombre']		= $element['nombre'];
		$array[$i]['codigo']		= $element['codigo'];
		$array[$i]['cantidad']		= $element['cantidad'];
		$array[$i]['estado']		= $element['estado'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}