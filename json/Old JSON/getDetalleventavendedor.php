<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cVentas.php';
$database 				= new Database();
$db 					= $database->getConnection();
$oObject   				= new Venta($db);
if(!isset($_POST['id'])){
	echo json_encode(array("title" => "Error", "text" => "Problema para obtener la informacion de la compra", "type" => "error"));
	exit();
}
$oObject->idventa_vendedor 	= $_POST['id'];
$object 			= $oObject->getDetalleVentaProductoVendedor();  
if($object){
	echo json_encode($object);
}else{
	echo json_encode(array("title" => "Error", "text" => "No hay informacion del detalle de la compra", "type" => "error"));
}