<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cRuta.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oEmpresa  		= new Ruta($db);
$object 		= $oEmpresa->get_all_empresa();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idempresa']			= $element['idempresa'];
		$array[$i]['nombre_empresa']	= $element['nombre_empresa'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}