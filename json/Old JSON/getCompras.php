<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCompra.php';
$database 				= new Database();
$db 					= $database->getConnection();
$oObject   				= new Compra($db);
$oObject->idusuario 	= $_POST['id'];
$object 				= $oObject->getAllComprasUsuario();
if($object){
	echo json_encode($object);
}else{
	echo 'ndata';
}