<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cEmpresa.php';
$database 				= new Database();
$db 					= $database->getConnection();
$oEmpresa   			= new Empresa($db);
$oEmpresa->idempresa 	= $_POST['id'];
$object 				= $oEmpresa->get_empresa();
if($object){
	echo json_encode($object);
}else{
	echo 'ndata';
}