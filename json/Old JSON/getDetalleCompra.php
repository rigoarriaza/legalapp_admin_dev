<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCompra.php';
$database 				= new Database();
$db 					= $database->getConnection();
$oObject   				= new Compra($db);
if(!isset($_POST['id'])){
	echo json_encode(array("title" => "Error", "text" => "Problema para obtener la informacion de la compra", "type" => "error"));
	exit();
}
$oObject->idcompra 	= $_POST['id'];
$object 			= $oObject->getDetalleCompraProductoVendedor();  
if($object){
	echo json_encode($object);
}else{
	echo json_encode(array("title" => "Error", "text" => "No hay informacion del detalle de la compra", "type" => "error"));
}