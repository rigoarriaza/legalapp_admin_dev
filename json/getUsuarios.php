<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuario.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oUsuario   	= new Usuario($db);
$object 		= $oUsuario->get_all_user();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idusuario']			= $element['idusuarioweb'];
		$array[$i]['nombre']			= $element['nombre'];
		$array[$i]['apellido']			= $element['apellido'];
		$array[$i]['creacion']			= $element['creacion'];
		$array[$i]['correo']			= $element['correo'];
		$array[$i]['estado']			= $element['idestado'];
		$array[$i]['nombreEstado']		= $element['estado'];
		$array[$i]['rol']				= $element['rol'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}