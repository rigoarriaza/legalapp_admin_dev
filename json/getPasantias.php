<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPasantia.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oPasantia   	= new Pasantia($db);
$object 		= $oPasantia->get_all_pasas();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idpasantia']	= $element['idpasantia'];
        $array[$i]['nombre']			= $element['nombre'];
        $array[$i]['descripcion']		= $element['descripcion'];
        $array[$i]['estado']			= $element['estado'];
        $array[$i]['creacion']			= $element['creacion'];
        $array[$i]['nombreFirma']		= $element['nombreFirma'];
        $array[$i]['idusuarioweb']		= $element['idusuarioweb'];
        $nombreWeb              		= $oPasantia->get_usuarioweb($element['idusuarioweb']);
        //var_dump($nombreWeb);
        $array[$i]['nombreWeb']     	= $nombreWeb['nombreWeb'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}