<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cExamen.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oObject   	= new Examen($db);
$object 	= $oObject->get_all_especialidades();
$array		= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idespecialidad']	= $element['idespecialidad'];
		$array[$i]['nombre']			= $element['nombre'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}