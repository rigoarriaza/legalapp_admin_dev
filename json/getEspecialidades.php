<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cEspecialidad.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oEspecialidad 	= new Especialidad($db);
$object 		= $oEspecialidad->get_all_espe();
$array			= array();
if($object){
	$i=0;
	foreach ($object AS $row => $element) {
		$array[$i]['idespecialidad']	= $element['idespecialidad'];
		$array[$i]['nombre']			= $element['nombre'];
		$array[$i]['descripcion']		= $element['descripcion'];
		$array[$i]['estado']			= $element['estado'];
		$i++;
	}
	echo json_encode($array);
}else{
	echo 'ndata';
}