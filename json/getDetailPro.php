<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCobros.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oCobro   	= new Cobros($db);
$oCobro->idusuarioprofesional = $_POST['id'];
$object 		= $oCobro->get_detail_pro();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}