<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cAprobarCalificacion.php';
$database 		                = new Database();
$db 			                = $database->getConnection();
$oAprob       	                = new AprobarC($db);
$oAprob->idcalificacion_pregunta_express    = $_POST['id'];
$oAprob->estado    = '1';
$object 		                = $oAprob->modify_estadoCali();
if($object){
    echo json_encode(array("title" => "Operación realizada", "text" => "Calificación del cliente aprobada con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
}else{
    echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al realizar este proceso.", "type" => "error"),JSON_UNESCAPED_UNICODE);
}