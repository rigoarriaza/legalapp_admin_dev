<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCobros.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oCobros   	= new Cobros($db);


// set values
if(($_POST['mail'])!=''){
    $oCobros->mail = $_POST['mail'];
}

if(($_POST['fecInicio'])!=''){
    $oCobros->fecInicio = $_POST['fecInicio'];
}

if(($_POST['fecFin'])!=''){
    $oCobros->fecFin = $_POST['fecFin'];
}

try{
    $results = $oCobros->getSearchCobros();
    if($results){
        echo json_encode($results);
    }else{
        echo 'ndata';
        //echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar devolver la información ", "type" => "error"),JSON_UNESCAPED_UNICODE);
    }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}