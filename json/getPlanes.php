<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oPlan       	= new Plan($db);
$object 		= $oPlan->get_all_planes();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idplan']        	    = $element['idplan'];
        $array[$i]['nombre']			    = $element['nombreplan'];
        $array[$i]['costo']     		    = $element['costo'];
        if($element['estado'] == '1'){
            $array[$i]['estado']		= 'Activo';
        }else{
            $array[$i]['estado']		= 'Inactivo';
        }
        $array[$i]['mesesactivo']	    	= $element['mesesactivo'];
        if($element['aplicacion_plan'] == '1'){
            $array[$i]['aplicacion_plan']		= 'Profesional';
        }else{
            $array[$i]['aplicacion_plan']		= 'Firma';
        }

        $array[$i]['especialidades']		= $element['especialidades'];
        $array[$i]['numero_cuentas']		= $element['numero_cuentas'];
        $array[$i]['tiempo_cobro']	    	= $element['tiempo_cobro'];
        $array[$i]['ubicacion']	        	= $element['ubicacion'];
        $array[$i]['biblioteca']        	= $element['biblioteca'];
        $array[$i]['creacion']	    	    = $element['creacion'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}