<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cExamen.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oData   			= new Examen($db);
$oData->idpreguntasexamen 	= $_POST['id'];
$object 			= $oData->get_one_pregunta();
if($object){
	echo json_encode($object);
}else{
	echo 'ndata';
}