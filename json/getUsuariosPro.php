<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuariopro.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oUsuariopro   	= new Usuariopro($db);
$object 		= $oUsuariopro->get_all_userpro();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idusuarioprofesional']			= $element['idusuarioprofesional'];
        $array[$i]['nombre']			= $element['nombre'];
        $array[$i]['apellido']			= $element['apellido'];
        $array[$i]['creacion']			= $element['creacion'];
        $array[$i]['correo']			= $element['correo'];
        $array[$i]['carnet']			= $element['carnet'];
        $array[$i]['idestadousuario']			= $element['idestadousuario'];
        $array[$i]['estadoVerificacion']			= $element['estadoverificacion'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}