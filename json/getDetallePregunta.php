<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cTransac.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oTransac   	= new Transac($db);


// set values
if(($_POST['fecInicio'])!=''){
    $oTransac->fecInicio = $_POST['fecInicio'];
}

if(($_POST['fecFin'])!=''){
    $oTransac->fecFin = $_POST['fecFin'];
}

if(($_POST['tipoPre'])!=''){
    $oTransac->tipoPre = $_POST['tipoPre'];
}


try{
    $results = $oTransac->getSearchPreguntas();
    $array			= array();
    $i = 0;
    if($results){
        if($_POST['tipoPre'] == '1'){
            foreach ($results AS $row => $element){
                $array[$i]['idpregunta'] = $element['idpregunta'];
                $array[$i]['titulo'] = $element['titulo'] == ' ' ? 'No Disponible' : $element['titulo'];
                //var_dump($array[$i]['titulo']);
                $array[$i]['idusuarioApp'] = $element['idusuarioApp'];
                $objUser = $oTransac->getDetailsUser($element['idusuarioApp']);
                //var_dump($objUser);
                $array[$i]['nombreUsuario'] = $objUser[0]['nombreUsuario'] == ' ' ? 'No Disponible' : $objUser[0]['nombreUsuario'];
                $array[$i]['correoUsuario'] = $objUser[0]['correo'] == ' ' ? 'No Disponible' : $objUser[0]['correo'];
                $array[$i]['idusuarioprofesional'] = $element['idusuarioprofesional'];
                if($array[$i]['idusuarioprofesional'] !== '0'){
                    $objPro = $oTransac->getDetailsPro($element['idusuarioprofesional']);
                    $array[$i]['nombrePro'] = $objPro[0]['nombrePro'];
                    $array[$i]['correoPro'] = $objPro[0]['correo'];
                } else {
                    $array[$i]['nombrePro'] = 'No Disponible';
                    $array[$i]['correoPro'] = 'No Disponible';
                }
                $array[$i]['creacion'] = $element['creacion'];
                $array[$i]['estado'] = $element['estado'];
                $i++;
            }
            $results = $array;
        } else {

                foreach ($results AS $row => $element){
                    $array[$i]['idpreguntadirecta'] = $element['idpreguntadirecta'];
                    $array[$i]['titulo'] = $element['titulo'];
                    $array[$i]['idusuarioApp'] = $element['idusuarioapp'];
                    $objUser = $oTransac->getDetailsUser($element['idusuarioapp']);
                    $array[$i]['nombreUsuario'] = $objUser[0]['nombreUsuario'];
                    $array[$i]['correoUsuario'] = $objUser[0]['correo'];
                    $array[$i]['idusuarioprofesional'] = $element['idusuarioprofesional'];
                    if($array[$i]['idusuarioprofesional'] !== '0'){
                        $objPro = $oTransac->getDetailsPro($element['idusuarioprofesional']);
                        $array[$i]['nombrePro'] = $objPro[0]['nombrePro'];
                        $array[$i]['correoPro'] = $objPro[0]['correo'];
                    } else {
                        $array[$i]['nombrePro'] = 'No Disponible';
                        $array[$i]['correoPro'] = 'No Disponible';
                    }
                    $array[$i]['creacion'] = $element['creacion'];
                    $array[$i]['estado'] = $element['estado'];
                    $i++;
                }
                $results = $array;

        }


        echo json_encode($results);
    }else{
        echo 'ndata';
        //echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar devolver la información ", "type" => "error"),JSON_UNESCAPED_UNICODE);
    }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}