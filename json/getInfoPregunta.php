<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPregunta.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oPregunta       = new Pregunta($db);
$opcion  = $_POST['opcion'];

if ($opcion == 'express'){


    $oPregunta->id   = $_POST['id'];
    $object 		= $oPregunta->getInfoExpress();
    if($object){
        echo json_encode($object);
    }else{
        echo 'ndata';
    }

}else if ($opcion == 'expressDetalle'){


    $oPregunta->id   = $_POST['id'];
    $object 		= $oPregunta->getInfoExpressDetalle();

    
    if($object){
        $obj = json_decode($object);

        $datos = $obj->msg;
        foreach ($datos as $key => $data) {
            $persona = ($data->tipo_respuesta == '1')? 'Usuario' : 'Abogado';
            
            echo '<div> <span style="font-weight: bolder;">'.$persona.': </span>'.
                '<span style="font-weight: 100;">'.$data->datetime.': </span><br>"'.$data->contenido.'"<br>'.
                ' </div>';
        }
    }else{
        echo 'ndata';
    }

}else if ($opcion == 'directa'){

    $oPregunta->id   = $_POST['id'];
    $object 		= $oPregunta->getInfoDirecta();
    if($object){
        echo json_encode($object);
    }else{
        echo 'ndata';
    }

}else if ($opcion == 'directaDetalle'){


    $oPregunta->id   = $_POST['id'];
    $object 		= $oPregunta->getInfoDirectaDetalle();

    
    if($object){
        $obj = json_decode($object);

        $datos = $obj->msg;
        foreach ($datos as $key => $data) {
            $persona = ($data->tipo_respuesta == '1')? 'Usuario' : 'Abogado';
            
            echo '<div> <span style="font-weight: bolder;">'.$persona.': </span>'.
                '<span style="font-weight: 100;">'.$data->datetime.': </span><br>"'.$data->contenido.'"<br>'.
                ' </div>';
        }
    }else{
        echo 'ndata';
    }

}