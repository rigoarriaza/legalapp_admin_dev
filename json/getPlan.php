<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oPlan       	= new Plan($db);
$oPlan->idplan  = $_POST['id'];
$object 		= $oPlan->get_plan();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}