<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuarioApp.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oUsuarioApp   	= new UsuarioApp($db);
$object 		= $oUsuarioApp->get_all_user();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idusuario']			= $element['idusuarioapp'];
        $array[$i]['nombre']			= $element['nombre'];
        $array[$i]['apellido']			= $element['apellido'];
        $array[$i]['creacion']			= $element['creacion'];
        $array[$i]['correo']			= $element['correo'];
        $array[$i]['carnet']			= $element['carnet'];
        $array[$i]['foto']  			= $element['foto'];
        $array[$i]['saldo']  			= $element['saldo'];
        $array[$i]['estado']			= $element['idestado'];
        $array[$i]['nombreEstado']		= $element['estado'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}