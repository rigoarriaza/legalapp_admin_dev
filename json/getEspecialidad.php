<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cEspecialidad.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oEspecialidad   	= new Especialidad($db);
$oEspecialidad->idespecialidad = $_POST['id'];
$object 		= $oEspecialidad->get_espe();
if($object){
	echo json_encode($object);
}else{
	echo 'ndata';
}