<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 		    = new Database();
$db 			    = $database->getConnection();
$oPlan       	    = new Plan($db);
$oPlan->idfirmas    = $_POST['id'];
$object 		    = $oPlan->get_currentProAvail();
$array		= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idusuarioprofesional']	    = $element['idusuarioprofesional'];
        $array[$i]['nombre']			        = $element['nombre'];
        $array[$i]['apellido']			        = $element['apellido'];
        $array[$i]['correo']			        = $element['correo'];
        $array[$i]['celular']			        = $element['celular'];
        $array[$i]['direccion']			        = $element['direccion'];
        $array[$i]['carnet']			        = $element['carnet'];
        $array[$i]['refencia']			        = $element['refencia'];
        $array[$i]['estadoverificacion']	    = $element['estadoverificacion'];
        $array[$i]['fotopersonal']	            = $element['fotopersonal'];
        $array[$i]['descripcionprofesional']    = $element['descripcionprofesional'];
        $array[$i]['creacion']                  = $element['creacion'];
        $array[$i]['idestadousuario']           = $element['idestadousuario'];
        $array[$i]['titulo']                    = $element['titulo'];
        $array[$i]['numero_registro']           = $element['numero_registro'];
        $array[$i]['destacado']                 = $element['destacado'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}