<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cFirma.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oData   			= new Firma($db);
$oData->idfirmas 	= $_POST['id'];
$object 			= $oData->get_one_firma();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}