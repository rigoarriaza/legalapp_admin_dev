<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 		    = new Database();
$db 			    = $database->getConnection();
$oPlan       	    = new Plan($db);
$oPlan->idfirmas    = $_POST['id'];
$object 		    = $oPlan->get_all_proAvail();
$array		= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idusuarioprofesional']	    = $element['idusuarioprofesional'];
        $array[$i]['fullName']			        = $element['fullName'];
        $array[$i]['correo']			        = $element['correo'];
        $array[$i]['idestadousuario']           = $element['idestadousuario'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}