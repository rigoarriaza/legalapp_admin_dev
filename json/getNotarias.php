<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cNotaria.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oNotaria   	= new Notaria($db);
$object 		= $oNotaria->get_all_notarias();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idnotaria']	= $element['idnotaria'];
        $array[$i]['nombre']			= $element['nombre'];
        $array[$i]['detalle']		= $element['detalle'];
        $array[$i]['estado']			= $element['estado'];
        $array[$i]['creacion']			= $element['creacion'];
        $array[$i]['latitud']		= $element['latitud'];
        $array[$i]['longitud']     	= $element['longitud'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}