<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuarioApp.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oUsuarioApp   	= new UsuarioApp($db);
$oUsuarioApp->idusuario = $_POST['id'];
$object 		= $oUsuarioApp->get_user();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}