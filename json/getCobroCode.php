<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

$milliseconds = round(microtime(true) * 1000);
$rand = mt_rand();
$generate_sha = sha1($milliseconds.$rand);
$return_array = array("newCode"=>$generate_sha);

    echo json_encode($return_array);
