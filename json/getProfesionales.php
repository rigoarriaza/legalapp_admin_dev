<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCobros.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oCobro 	    = new Cobros($db);
$object 		= $oCobro->get_all_pros_avail();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idusuarioprofesional']  = $element['idusuarioprofesional'];
        $array[$i]['nombreCompleto']		= $element['nombreCompleto'];
        $array[$i]['correo']		        = $element['correo'];
        $array[$i]['carnet']		        = $element['carnet'];
        $array[$i]['creacion']			    = $element['creacion'];
        $array[$i]['ganancia']			    = $element['ganancia'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}