<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cCobros.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oCobro 	    = new Cobros($db);
$oCobro->idusuarioprofesional = $_POST['id'];
$object 		= $oCobro->get_data_to_collect();
$array			= array();
if($object){
    $i=0;
    foreach ($object AS $row => $element) {
        $array[$i]['idlog_ganancia_profesional']  = $element['idlog_ganancia_profesional'];
        $array[$i]['idusuarioprofesional']		= $element['idusuarioprofesional'];
        $array[$i]['tipo']		        = $element['tipo'];
        $array[$i]['idpregunta']		        = $element['idpregunta'];
        if($array[$i]['tipo'] == '1'){
            $oCobro->idpregunta = $array[$i]['idpregunta'];
            $objectPE 		= $oCobro->get_data_PE();

            $array[$i]['tipoPregunta'] = 'Express';
            $array[$i]['titulo'] = $objectPE['titulo'];
            $array[$i]['contenido'] = $objectPE['contenido'];
            $array[$i]['creacion'] = $objectPE['creacion'];
            $array[$i]['usuarioApp'] = $objectPE['usuarioApp'];

        } else {
            $oCobro->idpregunta = $array[$i]['idpregunta'];
            $objectPE 		= $oCobro->get_data_PD();

            $array[$i]['tipoPregunta'] = 'Directa';
            $array[$i]['titulo'] = $objectPE['titulo'];
            $array[$i]['contenido'] = $objectPE['contenido'];
            $array[$i]['creacion'] = $objectPE['creacion'];
            $array[$i]['usuarioApp'] = $objectPE['usuarioApp'];

        }
        $array[$i]['timestamp']			    = $element['timestamp'];
        $array[$i]['valor']			    = $element['valor'];
        $i++;
    }
    echo json_encode($array);
}else{
    echo 'ndata';
}