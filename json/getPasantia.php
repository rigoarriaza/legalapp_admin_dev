<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPasantia.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oPasantia         	= new Pasantia($db);
$oPasantia->idpasantia = $_POST['id'];
$object 		= $oPasantia->get_pasa();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}