<?php
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cTransac.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oTransac   	= new Transac($db);


// set values
if(($_POST['mail'])!=''){
    $oTransac->mail = $_POST['mail'];
}

if(($_POST['fecInicio'])!=''){
    $oTransac->fecInicio = $_POST['fecInicio'];
}

if(($_POST['fecFin'])!=''){
    $oTransac->fecFin = $_POST['fecFin'];
}

if(($_POST['tipoPre'])!=''){
    $oTransac->tipoPre = $_POST['tipoPre'];
}

if(($_POST['estadoPre'])!=''){
    $oTransac->estadoPre = $_POST['estadoPre'];
}

try{
    $results = $oTransac->getSearchDebit();
    if($results){
        echo json_encode($results);
    }else{
        echo 'ndata';
        //echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al intentar devolver la información ", "type" => "error"),JSON_UNESCAPED_UNICODE);
    }



}catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}