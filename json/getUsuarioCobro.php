<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cUsuarioCobro.php';
$database 		= new Database();
$db 			= $database->getConnection();
$oUsuarioCobro   	= new UsuarioCobro($db);
$oUsuarioCobro->idusuario = $_POST['id'];
$object 		= $oUsuarioCobro->get_user();
if($object){
    echo json_encode($object);
}else{
    echo 'ndata';
}