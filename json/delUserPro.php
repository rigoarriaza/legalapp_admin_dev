<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once '../clases/cConexion.php';
include_once '../clases/cPlan.php';
$database 		                = new Database();
$db 			                = $database->getConnection();
$oPlan       	                = new Plan($db);
$oPlan->idusuarioprofesional    = $_POST['id'];
$object 		                = $oPlan->del_ProFirma();
if($object){
    echo json_encode(array("title" => "Operación realizada", "text" => "Profesional removido con éxito.", "type" => "success"),JSON_UNESCAPED_UNICODE);
}else{
    echo json_encode(array("title" => "Error", "text" => " Ocurrió un problema al realizar este proceso.", "type" => "error"),JSON_UNESCAPED_UNICODE);
}