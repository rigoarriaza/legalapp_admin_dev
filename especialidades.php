<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	session_start();
	include_once 'clases/cConexion.php';
	include_once 'clases/cEspecialidad.php';
	include_once 'clases/cUsuario.php';
	$database 			= new Database();
	$db 				= $database->getConnection();
	$oUsuario   		= new Usuario($db);
	$oEspecialidad   	= new Especialidad($db);
	
  if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
  }

  $estados 	= $oUsuario->getEstadosUsuario();
  $roles 	= $oUsuario->getRoles();

  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Especialidades <?=date('Y-m-d')?></title>
 <?php
require_once('headerHTML.php');
?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="especialidades.php">Especialidades</a> <a href="#" class="current">Agregar nueva especialidad</a> </div>
    <h1>Especialidades</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Especialidades</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" name="formespe" id="formespe" novalidate="novalidate">
              <input type="hidden" id="opt" name="opt" value="nespe"/>
              <input type="hidden" id="id" name="id" value=""/>
                <input type="hidden" id="oldfile" name="oldfile" value=""/>
			  <div class="control-group">
                <label class="control-label">Nombre </label>
                <div class="controls">
                  <input type="text" name="nombre" id="nombre" class="inputformtext">
                </div>
              </div>
                <div class="control-group" id="div_imagen2">
                    <label class="control-label">Imagen</label>
                    <div class="controls">
                        <input name="imagen" id="imagen" type="file"  class="span9"/>
                        <br>
                        <img src="" id="currentImagen" name="currentImagen" style="width:150px;">
                    </div>
                </div>
              <div class="control-group">
	              <label class="control-label">Descripci&oacute;n</label>
	              <div class="controls">
	                <textarea rows="8" name="descripcion" id="descripcion" class="span6"></textarea>
	              </div>
              </div>
               <div class="control-group">
                <label class="control-label">Estado</label>
                <div class="controls">
                  <select name="estado" id="estado">
						<option value="1" selected="selected">Activo</option>
						<option value="0" >Inactivo</option>
				</select>
                </div>
              </div>
			   <div class="form-actions">
                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid"><hr>
	  <div class="row-fluid">
		  <div class="span12">
			  <div class="widget-box">
				  <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
					<h5>Especialidades registradas</h5>
				  </div>
				  <div class="widget-content nopadding">
					<table class="table table-bordered data-table" id="table1">
					  <thead>
						<tr>
						  <th>ID</th>
						  <th>Nombre</th>
						  <th>Descripcion</th>
						  <th>Estado</th>
						  <th>Opciones</th>
						</tr>
					  </thead>
					  <tbody>
					  </tbody>
					</table>
				  </div>
				</div>
		  </div>
	  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script> 

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
var table;
$(document).ready(function(){
 // ADD active state to current option
 var currentSel = $('#3');
 if(!currentSel.hasClass('active')){
 		currentSel.addClass('active');
	}
    $('#configAccor').show();

	getespe(false);

    table = $('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "sDom": '<""l>t<"F"fp>',
        "aaSorting": [],
        "buttons": [
            'csv', 'excel'
        ]
    });

    table.api().buttons().container()
        .insertBefore( '#table1_filter' );


    $(document).on('submit', '#formespe', function() {
        $.ajax({
            url: "action/createespecialidad.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(){
            },
            success: function(data) {
                var parsed = JSON.parse(data);
                swal({
                    title: parsed.title,
                    text: parsed.text,
                    type: parsed.type,
                    confirmButtonText: "Ok"
                });
                if(parsed.type=='success'){
                    $('#formespe').trigger("reset");
                    $('#opt').val('nespe');
                    $('#btnaction').val('Guardar');
                }
                getespe(true);
                cancelaction();
            },
            error: function(e) {
                    swal({
                        title: "Error!",
                        text: e,
                        type: "error",
                        confirmButtonText: "Ok"
                    });
                }
            });
            return false;
        });
    });


function getespe(v){
	if(v){
		table.fnClearTable();
	}
	$.post("json/getEspecialidades.php", {
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			if(data!='ndata' && data!='error'){
				//alert(data);
				data = JSON.parse(data);
				imprimirtabla(data);
			}
		}
	});
}

function imprimirtabla(data){
	data.forEach(function(e){
		var id 	= e['idespecialidad'];
		var estadoLabel = '';
		if(e['estado'] === '1'){
			estadoLabel = 'Activo';
		}else{
			estadoLabel = 'Inactivo';
		}
		var txt ='<a onclick="get_info_espe('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
		table.fnAddData( [ 
			id,
			e['nombre'],
			e['descripcion'],
			estadoLabel,
			txt
		]); 
	});
}

function get_info_espe(id){
	$.post("json/getEspecialidad.php",{
		id:id
	}, function (data,status){
		if(status =='success'){
			if (data!='ndata' &&  data !=''){
				//console.log(data);
				data = JSON.parse(data);
				$('#opt').val('mespe');
				$('#id').val(data['idespecialidad']);
				$('#nombre').val(data['nombre']);
                $('#oldfile').val(data['imagen']);
                $("#currentImagen").attr("src",data['imagen']);
				$('#descripcion').val(data['descripcion']);
				$('#btnaction').val('Modificar');
				var estado = data['estado'];
				/*
				var valueEstado = '';
				if(estado === '1'){
					valueEstado = 'Activo';
				}else{
					valueEstado = 'Deshabilitado';
				}
				*/
				document.getElementById("estado").value  = estado;
			}
		}
	});
	return false;
}
function cancelaction(){
	$('#formespe').trigger("reset");
	$('#btnaction').val('');
	$('#btnaction').val('Guardar');
	$('#opt').val('nespe');
    $("#currentImagen").attr("src","");
}
</script>
</body>
</html>
