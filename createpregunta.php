<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	session_start();
	include_once 'clases/cConexion.php';
	include_once 'clases/cEspecialidad.php';
	include_once 'clases/cUsuario.php';
	$database 			= new Database();
	$db 				= $database->getConnection();
	$oUsuario   		= new Usuario($db);
	$oEspecialidad   	= new Especialidad($db);
	
  if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
  }

  $estados 	= $oUsuario->getEstadosUsuario();
  $roles 	= $oUsuario->getRoles();

  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Pregutnas de examen <?=date('Y-m-d')?></title>
 <?php
require_once('headerHTML.php');
?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="especialidades.php">Crear Preguntas</a> <a href="#" class="current">Agregar nueva pregunta</a> </div>
    <h1>Preguntas de examen</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Preguntas de Examen</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" name="form" id="form" novalidate="novalidate" enctype="multipart/form-data">
              <input type="hidden" id="opt" name="opt" value="nPregunta"/>
              <input type="hidden" id="id" name="id" value=""/>
			  <div class="control-group">
				<div class="control-group">
					<label class="control-label" >Seleccione examen</label>
					<div class="controls">
						<select class="form-control" name='examen' id='examen' onchange="getPreguntas(false);">
							<option value ='0'>Seleccione</option>
						</select>
					</div>
				</div>
                <label class="control-label">Ingrese la pregunta </label>
                <div class="controls">
                  <input type="text" name="pregunta" id="pregunta" class="span11">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Respuesta</label>
                <div class="controls">
                  <select name="respuesta" id="respuesta">
						<option value="1" selected="selected">Verdadera</option>
						<option value="0" >Falsa</option>
				</select>
                </div>
              </div>
               <div class="control-group">
                <label class="control-label">Estado</label>
                <div class="controls">
                  <select name="estado" id="estado">
						<option value="1" selected="selected">Activo</option>
						<option value="0" >Inactivo</option>
				</select>
                </div>
              </div>
			   <div class="form-actions">
                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid"><hr>
	  <div class="row-fluid">
		  <div class="span12">
			  <form enctype="multipart/form-data" method="post" name="formHidden" id="formHidden" >
			  <div class="widget-box ">
				  <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
					<h5>Preguntas Almacenadas</h5>
				  </div>
				  <div class="widget-content nopadding">
					<table class="table table-bordered data-table" id="table">
						  <thead>
							<tr>
							  <th>ID</th>
							  <th>Pregunta</th>
							  <th>Respuesta</th>
							  <th>Estado</th>
							  <th>Opciones</th>
							</tr>
						  </thead>
						  <tbody class="elements">						
						  </tbody>
					</table>
				  </div>
				</div>
				<input type="submit" value="Guardar Orden" class="btn btn-success">
			</form>
		  </div>
	  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script> 

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
var table;
$(document).ready(function(){
 // ADD active state to current option
 var currentSel = $('#5');
 if(!currentSel.hasClass('active')){
 		currentSel.addClass('active');
	}
    $('#configAccor').show();

	getExamenes();
	
	$( ".elements" ).sortable();
	$( ".elements" ).disableSelection();

	table = $('#table').dataTable({
		"order": [],
		"aaSorting": [],
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<""l>t<"F"fp>',
		"rowReorder": true,
		"bFilter" : false,
		"bPaginate": false,
        "buttons": [
            'csv', 'excel'
        ]
	});



	$(document).on('submit', '#form', function() {
		$.ajax({
			  url: "action/createpregunta.php",
			  type: "POST",
			  data:  new FormData(this),
			  contentType: false,
			  cache: false,
			  processData:false,
			  beforeSend : function(){
			  },
			  success: function(data) {
				  var parsed = JSON.parse(data);
				  swal({
				   title: parsed.title,
				   text: parsed.text,
				   type: parsed.type,
				   confirmButtonText: "Ok"
				  });
					getPreguntas();
				},
				error: function(e) {
				  swal({
				   title: "Error!",
				   text: e,
				   type: "error",
				   confirmButtonText: "Ok"
				  });
				}
			});
			return false;
	});
	$(document).on('submit', '#formHidden', function() {
		$.ajax({
			  url: "action/changeorder.php",
			  type: "POST",
			  data:  new FormData(this),
			  contentType: false,
			  cache: false,
			  processData:false,
			  beforeSend : function(){
			  },
			  success: function(data) {
				  var parsed = JSON.parse(data);
				  swal({
				   title: parsed.title,
				   text: parsed.text,
				   type: parsed.type,
				   confirmButtonText: "Ok"
				  });
					getPreguntas();
				},
				error: function(e) {
				  swal({
				   title: "Error!",
				   text: e,
				   type: "error",
				   confirmButtonText: "Ok"
				  });
				}
			});
			return false;
	});
});
function getExamenes(){
	$.post("json/getExamenes.php", {
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			data = JSON.parse(data);
			create_elemen(data);
		}
	});
}

function create_elemen(data){
	data.forEach(function (element) {
		$("#examen").append($('<option>', {
			value: element.idexamenpro,
			text: element.nombre
		}));
	});
}
function getPreguntas(){
	var id = $("#examen").val();
	table.fnClearTable();
	$.post("json/getPreguntasExamen.php", {
		id:id
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			if(data!='ndata' && data!='error'){
				//alert(data);
				data = JSON.parse(data);
				imprimirtabla(data);
			}
		}
	});
}

function imprimirtabla(data){
	data.forEach(function(e){
		var id 	= e['idpreguntasexamen'];
		var estadoLabel = '';
		if(e['estado'] === '1'){
			estadoLabel = 'Activo';
		}else{
			estadoLabel = 'Inactivo';
		}
		if(e['respuesta'] === '1'){
			respuesta = 'Verdadera';
		}else if(e['respuesta'] === '0'){
			respuesta = 'Falsa';
		}
		var txt ='<a onclick="get_info('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
		var txt1 ='<input type="hidden" name="pregun[]" value="'+id+'" />';
		table.fnAddData( [ 
			id,
			e['pregunta']+txt1,
			respuesta,
			estadoLabel,
			txt
		]); 
	});

    table.api().buttons().container()
        .insertBefore( '.widget-content' );
}

function get_info(id){
	$.post("json/getPreguntaExamen.php",{
		id:id
	}, function (data,status){ 
		if(status =='success'){
			if (data!='ndata' &&  data !=''){
				//console.log(data);
				data = JSON.parse(data);
				$('#opt').val('mPregunta');
				$('#id').val(data['idpreguntasexamen']);
				$('#pregunta').val(data['pregunta']);
				$('#btnaction').val('Modificar');
				var estado = data['estado'];
				document.getElementById("estado").value  = estado;
				var respuesta = data['respuesta'];
				document.getElementById("respuesta").value  = respuesta;
			}
		}
	});
	return false;
}
function cancelaction(){
	$('#formespe').trigger("reset");
	$('#btnaction').val('');
	$('#btnaction').val('Guardar');
	$('#opt').val('nespe');
}
</script>
</body>
</html>
