<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once 'clases/cConexion.php';
include_once 'clases/cUsuario.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oUsuario   		= new Usuario($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//$_SESSION['legalapp']['idusuario'] ID DE USUARIO!!

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Notarias <?=date('Y-m-d')?></title>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
            color: blue !important;
        }
        .control-label{
            font-weight: bold;
            color: black !important;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }

        .swal-modal {
            overflow: scroll;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="notaria.php">Notarias</a> <a href="#" class="current">Agregar/Modificar una Notaria</a> </div>
        <h1>Notarias</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Notarias</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formnota" id="formnota" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nnota"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Nombre </label>
                                <div class="controls">
                                    <input type="text" name="nombre" id="nombre" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Detalle</label>
                                <div class="controls">
                                    <textarea rows="8" name="detalle" id="detalle" class="span6"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Latitud </label>
                                <div class="controls">
                                    <input type="text" name="latitud" id="latitud" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Longitud </label>
                                <div class="controls">
                                    <input type="text" name="longitud" id="longitud" class="inputformtext">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado</label>
                                <div class="controls">
                                    <select name="estado" id="estado">
                                        <option value="1" selected="selected">Activo</option>
                                        <option value="0" >Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Notarias</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Estado</th>
                                <th>Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function(){
        // ADD active state to current option
        var currentSel = $('#17');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }

        $('#configAccor').show();

        getNota(false);

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );

        $(document).on('submit', '#formnota', function() {
            $.post("action/createNota.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        $('#formnota').trigger("reset");
                        $('#opt').val('nnota');
                        $('#btnaction').val('Guardar');
                    }
                    getNota(true);
                });
            return false;
        });
    });


    function getNota(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getNotarias.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idnotaria'];
            var estadoLabel = '';
            if(e['estado'] === '1'){
                estadoLabel = 'Activo';
            }else{
                estadoLabel = 'Inactivo';
            }
            var txt ='<a onclick="get_info_nota('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;" class="text-data">Modificar</a>';
            table.fnAddData( [
                id,
                e['nombre'],
                e['latitud'],
                e['longitud'],
                estadoLabel,
                e['creacion'],
                txt
            ]);
        });
    }

    function get_info_nota(id){
        $.post("json/getNotaria.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //console.log(data);
                    data = JSON.parse(data);
                    $('#opt').val('mnota');
                    $('#id').val(data['idnotaria']);
                    $('#nombre').val(data['nombre']);
                    $('#detalle').val(data['detalle']);
                    $('#latitud').val(data['latitud']);
                    $('#longitud').val(data['longitud']);
                    $('#btnaction').val('Modificar');
                    var estado = data['estado'];
                    /*
                     var valueEstado = '';
                     if(estado === '1'){
                     valueEstado = 'Activo';
                     }else{
                     valueEstado = 'Deshabilitado';
                     }
                     */
                    document.getElementById("estado").value  = estado;
                }
            }
        });
        return false;
    }
    function cancelaction(){
        $('#formnota').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#opt').val('nnota');
    }
</script>
</body>
</html>
