<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once 'clases/cConexion.php';
include_once 'clases/cAprobarCalificacion.php';
include_once 'clases/cUsuario.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oUsuario   		= new Usuario($db);
$oAprob          	= new AprobarC($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//$estados 	= $oUsuario->getEstadosUsuario();
//$roles 	= $oUsuario->getRoles();


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Calificaciones de usuarios <?=date('Y-m-d')?></title>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .ellipsis {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="aprobarCalificacion.php">Calificación de preguntas</a> <a href="#" class="current">Aprobar Calificación de preguntas</a> </div>
        <h1>Aprobar Calificación de preguntas</h1>
    </div>
    <div class="container-fluid conceal"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Aprobar Calificación de preguntas</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formaprob" id="formaprob" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="maprob"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Pregunta del usuario: </label>
                                <div class="controls">
                                    <p id="tituloPregunta" style="font-weight: bold"></p>
                                    <br>
                                    <p id="pregunta" style="font-style: italic"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Respuesta del profesional: </label>
                                <div class="controls">
                                    <p id="respuesta"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Profesional que responde: </label>
                                <div class="controls">
                                    <p id="profesional"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Creación de pregunta: </label>
                                <div class="controls">
                                    <p id="creacionPregunta"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Contestada en: </label>
                                <div class="controls">
                                    <p id="creacionRespuesta"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Calificación de usuario: </label>
                                <div class="controls">
                                    <p id="calificacionUsuario"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Comentario en calificación: </label>
                                <div class="controls">
                                    <p id="comentario"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado de calificación: </label>
                                <div class="controls">
                                    <select name="estado" id="estado">
                                        <option value="0">Pendiente</option>
                                        <option value="1" >Aprobado</option>
                                        <option value="2" >No aprobado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Calificaciones de los usuarios</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Comentario</th>
                                <th>Calificación</th>
                                <th>Fecha creación (aaaa-mm-dd hh:mm:ss)</th>
                                <th>Estado Actual</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function(){
        $('.conceal').hide();
        // ADD active state to current option
        var currentSel = $('#13');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#transacAccor').show();

        getCalificaciones(false);

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );


        $(document).on('submit', '#formaprob', function() {
            $.post("action/changeCalificacion.php", $(this).serialize())
                .done(function(data) {
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();
                    }
                    getCalificaciones(true);
                });
            return false;
        });
    });


    function getCalificaciones(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getCalificaciones.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idcalificacion_pregunta_express'];
            var estadoLabel = '';
            var estado = '';
            estado = e['estado'];

            if(estado === "0"){
                estadoLabel = 'Pendiente';
            }else if(estado == "1"){
                estadoLabel = 'Aprobado';
            }else{
                estadoLabel = 'No Aprobado';
            }

            var txt ='<a onclick="get_info_cali('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a> | ' +
                '<a onclick="setAprobado('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Aprobar</a>';
            table.fnAddData( [
                id,
                e['comentario'],
                e['calificacion'],
                e['creacion'],
                estadoLabel,
                txt
            ]);
        });
    }

    function get_info_cali(id){
        $('.conceal').show();
        $.post("json/getCalificacion.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //console.log(data);
                    data = JSON.parse(data);
                    $('#opt').val('maprob');
                    $('#id').val(data['idcalificacion_pregunta_express']);
                    $('#tituloPregunta').text(data['titulo']);
                    $('#pregunta').text(data['contenido']);
                    $('#respuesta').text(data['respuestacontenido']);
                    $('#profesional').text(data['nombreProfesional']);
                    $('#creacionPregunta').text(data['creacionPregunta']);
                    $('#creacionRespuesta').text(data['creacionRespuesta']);
                    $('#calificacionUsuario').text(data['calificacion']);
                    $('#comentario').text(data['comentario']);
                    $('#btnaction').val('Modificar');
                    var estado = data['estado'];
                     var valueEstado = '';

                    if(estado === '0'){
                        estadoLabel = 'Pendiente';
                    }if(estado === '1'){
                        estadoLabel = 'Aprobado';
                    }else{
                        estadoLabel = 'No Aprobado';
                    }

                    $("#estado").val(estado);
                }
            }
        });
        return false;
    }

    function setAprobado(id){
        $.post("json/setAprobado.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();
                    }
                    getCalificaciones(true);
                }
            }
        });
        return false;
    }

    function cancelaction(){
        $('.conceal').hide();
        $('#formaprob').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#opt').val('naprob');
    }
</script>
</body>
</html>
