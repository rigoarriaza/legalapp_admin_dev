<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once 'clases/cConexion.php';
include_once 'clases/cUsuario.php';
$database 			= new Database();
$db 				= $database->getConnection();
$oUsuario   		= new Usuario($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//$_SESSION['legalapp']['idusuario'] ID DE USUARIO!!

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Insertar saldo usuarios app <?=date('Y-m-d')?></title>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
            color: blue !important;
        }
        .control-label{
            font-weight: bold;
            color: black !important;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }

        .swal-modal {
            overflow: scroll;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="insertSaldo.php">Saldo a usuario</a> <a href="#" class="current">Insertar saldo al usuario</a> </div>
        <h1>Agregar Saldo</h1>
    </div>
    <div class="container-fluid conceal"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Saldo</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formsaldo" id="formsaldo" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="msaldo"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Nombre </label>
                                <div class="controls">
                                    <span id="nombre" name="nombre" class="text-data"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Correo </label>
                                <div class="controls">
                                    <span id="correo" name="correo" class="text-data"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Saldo Actual </label>
                                <div class="controls">
                                    <span id="saldo" name="saldo" class="text-data"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado Actual </label>
                                <div class="controls">
                                    <span id="estado" name="estado" class="text-data"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Monto saldo para agregar </label>
                                <div class="controls">
                                    <input type="text" name="montoSaldo" id="montoSaldo" class="inputformtext">
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Modificar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Saldo a usuario app</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Saldo Actual</th>
                                <th>Estado</th>
                                <th>Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    $('.conceal').hide();


    $(document).ready(function(){
        // ADD active state to current option
        var currentSel = $('#19');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }

        $('#transacAccor').show();

        $('.conceal').hide();

        getUsers(false);

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );

        $(document).on('submit', '#formsaldo', function() {
            $.post("action/updateSaldo.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        $('#formsaldo').trigger("reset");
                        $('.conceal').hide();
                        $('#opt').val('msaldo');
                    }
                    getUsers(true);
                });
            return false;
        });
    });


    function getUsers(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getUsuariosApp.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idusuario'];
            var estadoLabel = '';
            if(e['estado'] === '1'){
                estadoLabel = 'Pendiente activacion';
            } else if(e['estado'] === '2') {
                estadoLabel = 'Activo';
            } else {
                estadoLabel = 'Inactivo';
            }
            var txt ='<a onclick="get_info_user('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;" class="text-data">Agregar Saldo</a>';
            table.fnAddData( [
                id,
                e['nombre']+' '+e['apellido'],
                e['correo'],
                e['saldo'],
                estadoLabel,
                e['creacion'],
                txt
            ]);
        });
    }

    function get_info_user(id){
        $('.conceal').show();
        $.post("json/getUsuarioApp.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //console.log(data);
                    data = JSON.parse(data);
                    $('#opt').val('msaldo');
                    $('#id').val(data['idusuarioapp']);
                    $('#nombre').text(data['nombre']+' '+data['apellido']);
                    $('#correo').text(data['correo']);
                    $('#saldo').text(data['saldo']);
                    $('#estado').text(data['estado']);

                }
            }
        });
        return false;
    }
    function cancelaction(){
        $('#formsaldo').trigger("reset");
        $('.conceal').hide();
    }
</script>
</body>
</html>
