<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cUsuario.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//Enter modifications

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
        }
        .control-label{
            font-weight: bold;
        }
        .control-group{
            padding: 0 15px;
        }
        .widget-content h3{
            padding: 0 15px;
        }
        #firstBlock{
            float: left;
            margin-right: 72px;
        }

        .swal-modal {
            overflow: scroll;
        }
    </style>
    <title>Visor de Preguntas <?=date('Y-m-d')?></title>
    <?php
    require_once('headerHTML.php');
    ?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Transacciones</a> <a href="#" class="current">Vista de Preguntas</a> </div>
        <h1>Vista de preguntas</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Vista de preguntas</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formbusqueda" id="formbusqueda" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nplan"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Fecha de inicio de Preguntas (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/01/Y')?>" class="input-append date datepicker">
                                        <input id="fecInicio" name="fecInicio" type="text" value="<?=date('m/01/Y')?>"  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de fin de Preguntas (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/t/Y')?>" class="input-append date datepicker">
                                        <input id="fecFin" name="fecFin" type="text" value="<?=date('m/t/Y')?>"  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Buscar en...</label>
                                <div class="controls">
                                    <select name="tipoPre" id="tipoPre">
                                        <?php
                                            $tipoPreOpt = $_REQUEST['p'];
                                            $selExpress = '';
                                            $selDirect = '';
                                            if($tipoPreOpt == '1'){
                                                $selExpress = 'selected';
                                            } else {
                                                $selDirect = 'selected';
                                            }
                                        ?>
                                        <option value="1" <?=$selExpress?>>Pregunta Express</option>
                                        <option value="2" <?=$selDirect?>>Pregunta Directa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Buscar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Resultados de la búsqueda: <span id="searchType"></span></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <!--<th>Correo Usuario</th>-->
                                <th>Nombre Usuario</th>
                                <th>Título pregunta</th>
                                <!--<th>Correo Profesional</th>-->
                                <th>Nombre Profesional</th>
                                <th>Fecha/hora Creación</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="widget-box">
            <div class="conceal widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                <h5>Detalle de Pregunta</h5>
            </div>
            <div class="widget-content nopadding">
                <div class="conceal row-fluid">
                    <h3>Datos de Pregunta</h3>
                    <div id="firstBlock">
                        <div class="control-group">
                            <label class="control-label">Pregunta realizada por:</label>
                            <div class="controls">
                                <span id="correoUsuario" name="correoUsuario" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Fecha creación:</label>
                            <div class="controls">
                                <span id="creacion" name="creacion" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Fecha pago:</label>
                            <div class="controls">
                                <span id="fpago" name="fpago" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Fecha asignada:</label>
                            <div class="controls">
                                <span id="ftomada" name="ftomada" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Privacidad:</label>
                            <div class="controls">
                                <span id="privacidad" name="privacidad" class="text-data"></span>
                            </div>
                        </div>
                        
                    </div>
                    <div id="secondBlock">
                        <div class="control-group">
                            <label class="control-label">Respondida por:</label>
                            <div class="controls">
                                <span id="correoPro" name="correoPro" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado Pregunta:</label>
                            <div class="controls">
                                <span id="estadoPregunta" name="estadoPregunta" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado Pago:</label>
                            <div class="controls">
                                <span id="estadoPago" name="estadoPago" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Monto cobrado:</label>
                            <div class="controls">
                                <span id="cantidad" name="cantidad" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Calificación:</label>
                            <div class="controls">
                                <span id="calificacion" name="calificacion" class="text-data"></span>
                            </div>
                        </div>
                    </div>

                    
                </div>

                <div id="">
                        <div class="control-group">
                            <label class="control-label">Título de pregunta:</label>
                            <div class="controls">
                                <span id="tituloPregunta" name="tituloPregunta" class="text-data"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Pregunta realizada:</label>
                            <div class="controls">
                                <span id="contenido" name="contenido" class="text-data"></span>
                            </div>
                        </div>
                        <!--<input type="button" onclick="cancelComment();" value="Más Detalle" class="btn btn-danger">-->

                        <div id="detallePregunta" style="padding: 15px;"></div>
                </div>
                <!--<form class="conceal form-horizontal" method="post" name="formComment" id="formComment" novalidate="novalidate">
                    <input type="hidden" id="idtransac" name="idtransac" value="" />
                    <input type="hidden" id="type" name="type" value="" />
                    <div class="control-group">
                        <label class="control-label">Comentario</label>
                        <div class="controls">
                            <textarea rows="8" name="comentario" id="comentario" class="span6"></textarea>
                        </div>
                        <div class="form-actions" style="padding-left: 180px;">
                            <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                            <input type="button" onclick="cancelComment();" value="Cancelar" class="btn btn-danger">
                        </div>
                    </div>

                </form>-->
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>
<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    $(document).ready(function(){

        $('.conceal').hide();

        // ADD active state to current option
        var currentSel = $('#18');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#transacAccor').show();



        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );

        $(document).on('submit', '#formbusqueda', function() {
            $.post("json/getDetallePregunta.php", $(this).serialize())
                .done(function(data,status) {
                    //console.log(data);
                    if (status == 'success') {
                        data = data.replace(/^\s*|\s*$/g, "");
                        if(data!='ndata' && data!='error'){
                            //alert(data);
                            data = JSON.parse(data);
                            imprimirResultadosBusqueda(data);
                        }else{
                            swal({
                                title: 'Sin resultados',
                                text: 'No se encontraron resultados',
                                type: 'error',
                                confirmButtonText: "Ok"
                            });
                        }
                    }

                });
            return false;
        });

        $('#formbusqueda').submit();


        $(document).on('submit', '#formComment', function() {
            $.post("action/addCommentDeb.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();

                    }
                });
            return false;
        });

    });

    function cancelComment(){
        var id = $('#idtransac').val();
        var type = $('#type').val();
        var loadType;
        if(type === 'PE'){
            loadType = true;
        }else{
            loadType = false;
        }
        get_info_transac(id,loadType)
    }



    function imprimirResultadosBusqueda(data){

        table.fnClearTable();
        $('.conceal').hide();

        var tipoPreSel = document.getElementById('tipoPre');
        var tipoPre = tipoPreSel.options[tipoPreSel.selectedIndex].value;

        var id;

        if (tipoPre === '1') {
            $('#searchType').text(' Preguntas Express');
            data.forEach(function(e){
                id 	= e['idpregunta'];
                txt ='<a onclick="get_info_pregunta('+id+',true)" style="padding: 0px 5px 0px 0px; cursor: pointer;">Ver Info</a>';
                user ='<a onclick="gotoUser('+e['idusuarioApp']+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">'+e['nombreUsuario']+'</a>';
                pro ='<a onclick="gotoPro('+e['idusuarioprofesional']+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">'+e['nombrePro']+'</a>';
                
                var estado = getEstado(e['estado']);

                table.fnAddData( [
                    id,
                    // e['correoUsuario'],
                    user,
                    e['titulo'],
                    // e['correoPro'],
                    pro,
                    e['creacion'],
                    estado,
                    txt
                ]);
            });

        } else {
            $('#searchType').text(' Preguntas Directas');
            data.forEach(function(e){
                id 	= e['idpreguntadirecta'];
                txt ='<a onclick="get_info_pregunta('+id+',false)" style="padding: 0px 5px 0px 0px; cursor: pointer;">Ver Info</a>';
                user ='<a onclick="gotoUser('+e['idusuarioApp']+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">'+e['nombreUsuario']+'</a>';
                pro ='<a onclick="gotoPro('+e['idusuarioprofesional']+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">'+e['nombrePro']+'</a>';
                
                
                var estado = getEstado(e['estado']);

                table.fnAddData( [
                    id,
                    // e['correoUsuario'],
                    // e['nombreUsuario'],
                    user,
                    e['titulo'],
                    // e['correoPro'],
                    // e['nombrePro'],
                    pro,
                    e['creacion'],
                    estado,
                    txt
                ]);
            });
        }

    }
    function gotoUser(id){
        var path   =  "<?=$pageUrl; ?>";
        window.open(path+"usuarioapp.php?id="+id);
        // window.location.href = path+"usuarioapp.php?id="+id;
        
    }
    function gotoPro(id){
        var path   =  "<?=$pageUrl; ?>";
        window.open(path+"usuariopro.php?id="+id);
        // window.location.href = path+"usuariopro.php?id="+id;
    }
    function get_info_pregunta(id,type){
        $('.conceal').show();
        
        if(type){
            $.post("json/getInfoPregunta.php",{
                id:id, opcion: 'express'
            }, function (data,status){
                if(status =='success'){
                    if (data !== 'ndata' &&  data !== ''){
                        // alert(data);
                        data = JSON.parse(data);
                        $('#idtransac').val(id);
                        $('#type').val('PE');

                        //correoUsuario
                        $('#correoUsuario').text(data['usuario']);
                        //creacion
                        $('#creacion').text(data['creacion']);
                        //tituloPregunta
                        $('#tituloPregunta').text(data['titulo']);
                        //contenido
                        $('#contenido').text(data['contenido']);
                        //respuestaContenido
                        $('#respuestaContenido').text(data['respuestacontenido']);

                        $('#ftomada').text(data['datetimetomada']);
                        $('#fpago').text(data['datetimepago']);
                        //Pro
                        var profesional = "No Asignado";
                        if(data['profesional'] != null){
                            profesional = data['profesional'];
                        }
                        $('#correoPro').text(profesional);
                        //calificacion
                        if(data['calificacion'] == null ){
                            $('#calificacion').text('0');
                        }else{
                            $('#calificacion').text(data['calificacion']);
                        }
                        
                        //privacidad
                        var privacidad;

                        if(data['privacidad'] == '0'){
                            privacidad = 'Público';
                        }else if (data['privacidad'] == '1'){
                            privacidad = 'Privado';
                        }

                        $('#privacidad').text(privacidad);
                        //estadoPregunta
                        
                        var estadoPregunta = getEstado(data['estado']);
                        


                        $('#estadoPregunta').text(estadoPregunta);

                        //estadoPago

                        var estadoPago;

                        if(data['estadopago'] == '0'){
                            estadoPago = 'No Pagada';
                        }else if (data['estadopago'] == '1'){
                            estadoPago = 'Pagado';
                        }

                        $('#estadoPago').text(estadoPago);

                        //cantidad
                        $('#cantidad').text(data['cantidad']);

                        //$('#comentario').val(data['comentario']);
                        var comentario = data['comentario'];
                        var currentDate = moment().format();
                        if(comentario === null || comentario === undefined){
                            $('#comentario').val('------'+currentDate+'------');
                        }else{
                            $('#comentario').val(comentario+'\n\n'+'------'+currentDate+'------');
                        }

                        $.post("json/getInfoPregunta.php",{
                            id:id, opcion: 'expressDetalle'
                        }, function (data,status){
                            var content = document.getElementById("detallePregunta");
                            content.innerHTML = data;
                        });
                    }
                }
            });

        } else {
            $.post("json/getInfoPregunta.php",{
                id:id, opcion: 'directa'
            }, function (data,status){
                if(status =='success'){
                    if (data!='ndata' &&  data !=''){
                        // alert(data);
                        data = JSON.parse(data);
                        $('#idtransac').val(id);
                        $('#type').val('PD');

                        //correoUsuario
                        $('#correoUsuario').text(data['usuario']);
                        //creacion
                        $('#creacion').text(data['creacionPreguntaDir']);
                        //tituloPregunta
                        $('#tituloPregunta').text(data['titulopregunta']);
                        //contenido
                        $('#contenido').text(data['contenido']);
                        //privacidad
                        var privacidad;

                        if(data['privado'] == '0'){
                            privacidad = 'Público';
                        }else if (data['privado'] == '1'){
                            privacidad = 'Anónimo';
                        }

                        $('#privacidad').text(privacidad);
                        $('#fpago').text(data['datetimepago']);
                        $('#ftomada').text('N/A');

                        //Pro
                        var profesional = "No Asignado";
                        if(data['profesional'] != null){
                            profesional = data['profesional'];
                        }
                        $('#correoPro').text(profesional);
                        //calificacion
                        $('#calificacion').text('N/A');
                        //estadoPregunta
                        var estadoPregunta = getEstado(data['estado']);
                        $('#estadoPregunta').text(estadoPregunta);

                        //estadoPago

                        var estadoPago;
                        estadoPago = 'N/A';
                       if(data['estadopago'] == '0'){
                           estadoPago = 'No Pagada';
                       }else if (data['estadopago'] == '1'){
                           estadoPago = 'Pagado';
                       }
                        

                        $('#estadoPago').text(estadoPago);

                         //cantidad
                        $('#cantidad').text(data['valor']);

                        $.post("json/getInfoPregunta.php",{
                            id:id, opcion: 'directaDetalle'
                        }, function (data,status){
                            var content = document.getElementById("detallePregunta");
                            content.innerHTML = data;
                        });  

                    }
                }
            });
        }
        return false;
    }

    function getEstado(estado){
        var estadoPregunta;
        switch (estado) {
                            case '0':
                                estadoPregunta = "Cancelada";
                                break;
                            case '1':
                                estadoPregunta = "Por Pagar";
                                break;
                            case '2':
                                estadoPregunta = "Por Responder";
                                break;
                            case '3':
                                estadoPregunta = "Sin Respuesta";
                                break;
                            case '4':
                                estadoPregunta = "Recibiendo Respuesta";
                                break;
                            case '5':
                                estadoPregunta = "Contestada";
                                break;
                            case '6':
                                estadoPregunta = "Por Responder réplica";
                                break;
                            case '7':
                                estadoPregunta = "Finalizada";
                                break;
                            default:
                                estadoPregunta = "Estado no encontrado";
                                break
                        }
        return estadoPregunta;
    }
    function cancelaction(){
        setTimeout(function(){ window.location.reload(false); }, 2000);

    }

    function getExchange(idpreguntadirecta){
        var text = '';
        var fecha = '';
        var tipo_respuesta = '';
        var tipo_respuesta_text = '';
        var contenido = '';
        $.post("json/getExchange.php",{
            id:idpreguntadirecta
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //alert(data);
                    data = JSON.parse(data);
                    data.forEach(function(e){
                        fecha	= e['fecha'];
                        tipo_respuesta	= e['tipo_respuesta'];
                        contenido	= e['contenido'];

                        if (tipo_respuesta == '1') {
                            tipo_respuesta_text = 'Usuario';
                        } else {
                            tipo_respuesta_text = 'Profesional';
                        }

                        text = text+" Fecha: "+fecha+" --- Contesta:"+tipo_respuesta_text+". \n Respuesta: '"+contenido+"' \n --------------------- \n";

                    });
                    alert(text);
//                    swal({
//                        title: 'Respuestas',
//                        text: text,
//                        type: 'info',
//                        confirmButtonText: "Ok"
//                    });
                }
            }
        });

    }
</script>
</body>
</html>
