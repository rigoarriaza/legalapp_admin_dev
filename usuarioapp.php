<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cUsuarioApp.php';
include_once 'clases/cUsuario.php';
include_once 'clases/cPlan.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);
$oUsuarioApp       	= new UsuarioApp($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//TODO: Add log when a user change something or add something in here

//Enter modifications
$estados 	= $oUsuarioApp->getEstados();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Usuarios de la aplicación <?=date('Y-m-d')?></title>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Regresar" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Manejo de usuario de aplicación</a> <a href="#" class="current">Revisión de usuario</a> </div>
        <h1>Revisión y cambio de estado de usuario de aplicación móvil</h1>
    </div>
    <div class="container-fluid conceal"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Revisión de usuarios de la aplicación</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formapp" id="formapp" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value=""/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="conceal">
                                <h3>Datos de Usuario</h3>
                                <div class="control-group">
                                    <label class="control-label">Nombre de Usuario</label>
                                    <div class="controls">
                                        <span id="nombre" name="nombre" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Correo</label>
                                    <div class="controls">
                                        <span id="correo" name="correo" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Teléfono Móvil</label>
                                    <div class="controls">
                                        <span id="celular" name="celular" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Carnet</label>
                                    <div class="controls">
                                        <span id="carnet" name="carnet" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Saldo</label>
                                    <div class="controls">
                                        <span id="saldo" name="saldo" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Foto Personal</label>
                                    <div class="controls">
                                        <img id="fotoPer" name="fotoPer" src="" style="height: 150px; width: 150px;" />
                                        <p id="messageImg" name="messageImg" style="display: none; font-style: italic; font-size: smaller">No hay imagen para mostrar</p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Creación</label>
                                    <div class="controls">
                                        <span id="creacion" name="creacion" class="text-data"></span>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Estado de Usuario</label>
                                    <div class="controls">
                                        <select id="estadoUsuario" name="estadoUsuario">
                                            <option value="">--Seleccione--</option>
                                            <?php
                                            //TODO: hacer que no use meses activo, pero un post jquery para sacar los datos que necesita
                                            if($estados){
                                                forEach($estados as $val1){
                                                    echo '<option value="'.$val1['idestadousuario'].'">'.$val1['nombre'].'</option>';
                                                }
                                            }else{
                                                echo '<option value="">NO HAY DATOS</option>';
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                    <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Usuarios registrados</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre de usuario</th>
                                <th>Correo</th>
                                <th>Saldo</th>
                                <th>Carnet</th>
                                <th>Estado</th>
                                <th>Fecha de Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>


<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">

    function  getUserInfo(id) {
        var nombre              = "";
        var apellido            = "";
        var nombrecompleto      = "";
        var fotopersonal      = "";

        $.post("json/getUsuarioApp.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){

                    data = JSON.parse(data);

                    $("#opt").val('mapp');

                    $("#id").val(data['idusuarioapp']);
                    nombre      = data['nombre'];
                    apellido    = data['apellido'];
                    nombrecompleto = nombre+' '+apellido;
                    $("#nombre").text(nombrecompleto);
                    $("#correo").text(data['correo']);
                    $("#celular").text(data['celular']);
                    $("#carnet").text(data['carnet']);
                    $("#saldo").text(data['saldo']);

                    fotopersonal = data['foto'];

                    if(fotopersonal !== null){
                        $('#fotoPer').css('display','block');
                        $('#messageImg').css('display','none');
                        fotopersonal = 'http://legalbo.com/legalapp/IMGUSERS/'+fotopersonal;
                        $('#fotoPer').attr('src',fotopersonal);
                    }else{
                        $('#fotoPer').css('display','none');
                        $('#messageImg').css('display','block');
                    }


                    $("#creacion").text(data['creacion']);
                    //alert(data['estado']);
                    $("#estadoUsuario").select2('val', data['idestado']);

                    $('#btnaction').val('');
                    $('#btnaction').val('Modificar');

                }
            }
        });

        $('.conceal').css('display','block');
        $('html, body').animate({
            scrollTop: $("#formapp").offset().top
        }, 1000);

    }

    var table;

    $(document).ready(function(){

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );


        var iduser = <?=isset($_REQUEST['id']) ? $_REQUEST['id'] : 0 ; ?>;

        if ( iduser != 0 ){
            // $('.conceal').css('display','none');

            getUserInfo(iduser);
        }else{
            getusuarios(false);
            $('.conceal').css('display','none');
        }

        

        



        // ADD active state to current option
        var currentSel = $('#1A');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#usuarioAccor').show();



        //Submitting

        $(document).on('submit', '#formapp', function() {
            $.post("action/changeUsuarioApp.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();
                        getusuarios(true);

                    }
                });
            return false;
        });
    });

    function getusuarios(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getUsuariosApp.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idusuario'];
            var txt ='<a onclick="getUserInfo('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
            table.fnAddData( [
                id,
                e['nombre']+' '+e['apellido'],
                e['correo'],
                e['saldo'],
                e['carnet'],
                e['nombreEstado'],
                e['creacion'],
                txt
            ]);
        });
    }

    function cancelaction(){
        $('.conceal').css('display','none');
        $('#formapp').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#id').val('');
        $('#saldo').val('');
        $('#fotoPer').attr('src','');
        $('#carnet').val('');
        $('#celular').val('');
        $('#correo').val('');
        $('#nombre').val('');
        $('#estadoUsuario ').select2('val', '');
        $('#opt').val('');
    }
</script>
</body>
</html>
