<?php
    session_start();
    include_once 'clases/cConexion.php';
    include_once 'clases/cUsuario.php';
    $database   = new Database();
    $db         = $database->getConnection();
    $oUsuario   = new Usuario($db);
  
  if ( $oUsuario->is_loggedin() ) {
    header("Location: index.php");
    exit();
  }
  
?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Legal App</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/matrix-login.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/sweetalert.css">

    </head>
    <body>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" >
				 <div class="control-group normal_text"> <h3><img src="img/makeIconLegal.png" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" placeholder="Correo" name="mail" id="mail" class="input" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="Contraseña" name="pass" class="input" id="pass" />
                        </div>
                    </div>
                </div>
                <div id="btn-login" class="form-actions">
                    <span class="pull-right"><a type="submit" class="btn btn-success" /> Ingresar</a></span>
                </div>
            </form>
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/matrix.login.js"></script> 
        <script src="js/sweetalert.min.js"></script>
		<script>
			$('.input').keypress(function (e) {
			  if (e.which == 13) {
				$('#btn-login').trigger("click");
				return false;    //<---- Add this line
			  }
			});
		</script>
    </body>

</html>
