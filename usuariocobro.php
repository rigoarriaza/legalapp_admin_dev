<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
include_once 'clases/cConexion.php';
include_once 'clases/cUsuario.php';
$database 	= new Database();
$db 		= $database->getConnection();
$oUsuario   = new Usuario($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Usuarios para cobros <?=date('Y-m-d')?></title>

    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 320px;
        }
        h3, #messageFirma{
            margin-left: 30px;
        }
        .text-data{
            padding: 15px 0;
        }
        .controls{
            margin-top: 5px;
        }
        input{
            width: 320px;
        }
        #userFound{
            margin-left: 15px;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Usuarios para cobro</a> <a href="#" class="current">Administración de usuarios</a> </div>
        <h1>Usuarios Para Cobro</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Usuarios Cobro</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formusuarioscobro" id="formusuarioscobro" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nuser"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Nombre de usuario </label>
                                <div class="controls">
                                    <input type="text" name="user" id="user" class="inputformtext"><span id="userFound" class="label"></span>
                                    <span id="currentUser" name="currentUser" class="text-data"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Código</label>
                                <div class="controls">
                                    <input type="text" name="secret" id="secret" class="inputformtext" readonly="readonly">
                                    <input type="button" name="generator" id="generator" class="btn btn-primary" value="Generar Código">
                                </div>
                            </div>
                            <!--
                            <div class="control-group">
                                <label class="control-label">Código Actual</label>
                                <div class="controls">
                                    <span id="actualcod" name="actualcod" class="text-data">N/A</span>
                                </div>
                            </div>
                            -->
                            <div class="control-group">
                                <label class="control-label">Organización</label>
                                <div class="controls">
                                    <input type="text" name="org" id="org" class="inputformtext">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Estado</label>
                                <div class="controls">
                                    <select name="estado" id="estado">
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Creación</label>
                                <div class="controls">
                                    <span id="creation" name="creation" class="text-data">N/A</span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Usuarios registrados</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre de usuario</th>
                                <th>Organización</th>
                                <th>Estado</th>
                                <th>Fecha de Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;
    $(document).ready(function(){
        // ADD active state to current option
        var currentSel = $('#1B');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#usuarioAccor').show();

        $('#generator').click(function () {
            $.post("json/getCobroCode.php", {
            }, function (data, status) {
                if (status == 'success') {
                    data = data.replace(/^\s*|\s*$/g, "");
                    if(data!='ndata' && data!='error'){
                        data = JSON.parse(data);
                        $('#secret').val(data['newCode']);
                    }
                }
            });
        });



        var timer = null;
        $('#user').keydown(function(){
            clearTimeout(timer);
            timer = setTimeout(checkUsername, 1000)
        });



        getusuarios(false);

        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );


        $(document).on('submit', '#formusuarioscobro', function() {
            $.post("action/createusuariocobro.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();
                    }
                    getusuarios(true);
                });
            return false;
        });
    });


    function checkUsername() {
        var curString = $('#user').val();
        $.post("json/getPosibleUserCobro.php", {
            curString:curString
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    $("#userFound").removeClass();
                    $('#userFound').addClass('label label-important');
                    $('#userFound').text('Usuario ya existe! por favor intentelo de nuevo');
                    $('#btnaction').prop('disabled', true);
                }else{
                    $("#userFound").removeClass();
                    $('#userFound').addClass('label label-success');
                    $('#userFound').text('Usuario disponible');
                    $('#btnaction').prop('disabled', false);
                }
            }
        });
    }

    function getusuarios(v){
        if(v){
            table.fnClearTable();
        }
        $.post("json/getUsuariosCobros.php", {
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    //alert(data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idusuario'];
            var txt ='<a onclick="get_info_user('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
            var estado = e['estado'];
            if(estado === '1'){
                estado = 'Activo';
            }else{
                estado = 'Inactivo';
            }
            table.fnAddData( [
                id,
                e['user'],
                e['org'],
                estado,
                e['creacion'],
                txt
            ]);
        });
    }

    function get_info_user(id){
        $.post("json/getUsuarioCobro.php",{
            id:id
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //alert(data);
                    data = JSON.parse(data);
                    $('#opt').val('muser');
                    $('#id').val(data['idusuario_cobro']);
                    $('#user').hide();
                    $('#currentUser').text(data['username']);
                    $('#secret').val(data['secret_key']);
                    $('#org').val(data['organizacion']);
                    $('#creation').text(data['fecha_creacion']);

                    var estado = data['estado'];
                    document.getElementById("estado").value  = estado;

                    $("#userFound").text('');
                    $("#userFound").removeClass();
                    $("#userFound").addClass('label');

                    $('#btnaction').val('Modificar');


                }
            }
        });
        return false;
    }
    function cancelaction(){
        $('#formusuarioscobro').trigger("reset");
        $('#currentUser').text('');
        $('#user').show();
        $('#creation').text('');
        $("#userFound").text('');
        $("#userFound").removeClass();
        $("#userFound").addClass('label');
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#opt').val('nuser');
    }
</script>
</body>
</html>
