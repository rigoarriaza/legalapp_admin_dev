<?php
class Plan {

    public $idplan;

    public $nombre;
    public $costo;
    public $mesesActivo;
    public $descPlan;
    public $tipoPlan;
    public $numEspecialidad;
    public $numCuentas;
    public $ubicacion;
    public $biblioteca;
    public $estado;
    public $webName;
    public $nombrePago;
    public $idlogtransaccional;

    public $idfirmas;
    public $fechacompra;
    public $fechavencimiento;

    public $idmembresiafirma;
    public $idmembresia;
    public $idusuarioprofesional;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_plan() {
        try {

            $query = "SELECT idplan, nombreplan, costo, estado, mesesactivo, aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca, creacion FROM plan WHERE idplan = :idplan;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            
            echo $e->getMessage();
            
        }
    }

    public function setTransaction(){
        try {

            $idref = sha1('CMSPLAN'.time());
            $planDetails = $this->get_plan();
            $comentario = "---".date('Y-m-d H:i:s')."---<br><br> Actualización de plan. Se coloco Plan: ".$planDetails['nombreplan']." Creado por: ".$this->webName;

            // query to insert record
            $query = "INSERT INTO logtransaccionalprofesional (tipotransac,idreferencia,valortransac,estado,idusuarioprofesional,idplan,personapago,lugar,agencia,operador,comentario) 
						VALUES ('CMS',:idref,:valortransac,2,:idusuarioprofesional,:idplan,:nombrePago,'CMS','CMS',:operador,:comentario);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":idref", $idref);
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->bindParam(":valortransac", $planDetails['costo']);
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":nombrePago", $this->nombrePago);
            $stmt->bindParam(":operador", $this->webName);
            $stmt->bindParam(":comentario", $comentario);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }

        } catch (PDOException $e) {
            echo 'setTransaction';
            echo $e->getMessage();

        }
    }

    public function get_all_proAvail() {
        try {

            $query = "SELECT idusuarioprofesional, CONCAT(nombre,' ',apellido) AS fullName, correo,idestadousuario 
                      FROM usuarioprofesional 
                       WHERE idusuarioprofesional NOT IN 
                       (SELECT idusuarioprofesional FROM detallefirmaprofesional) AND idestadousuario <> '3';";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function get_currentProAvail() {
        try {

            $query = "SELECT idusuarioprofesional, nombre, apellido, correo, celular, direccion,carnet,refencia,
                      estadoverificacion, fotopersonal, descripcionprofesional, creacion, idestadousuario, titulo,
                       numero_registro, destacado FROM usuarioprofesional 
                       WHERE idusuarioprofesional IN 
                       (SELECT idusuarioprofesional 
                       FROM detallefirmaprofesional WHERE idfirmas = :idfirmas);";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function get_asignacion() {
        try {

            $query = "SELECT idmembresiafirma,idplan,idfirmas,fechacompra,fechavencimiento,estado,creacion 
FROM membresiafirma WHERE idfirmas = :idfirma;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idfirma", $this->idfirmas);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function get_asignacionPro() {
        try {

            $query = "SELECT idmembresia,idplan,fechacompra,fechavencimiento,estado,creacion 
FROM membresia WHERE idusuarioprofesional = :idusuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function get_planFirmaAndPro() {
        try {

            $query = "SELECT dfp.idfirmas,f.nombre,f.logo,p.nombreplan,p.costo,p.mesesactivo FROM detallefirmaprofesional AS dfp 
                      INNER JOIN firma AS f ON dfp.idfirmas=f.idfirmas
                      INNER JOIN  membresiafirma AS mf ON f.idfirmas = mf.idfirmas
                      INNER JOIN plan AS p ON mf.idplan = p.idplan
                      WHERE dfp.idusuarioprofesional = :idusuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function create_asignacionPro(){
        try
        {
            // query to insert record
            $query = "INSERT INTO membresia (idplan,idusuarioprofesional,fechacompra,fechavencimiento,estado,idlogtransaccional) 
						VALUES (:idplan,:idusuarioprofesional,:fechacompra,:fechavencimiento,:estado,:idlogtransaccional);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":idlogtransaccional", $this->idlogtransaccional);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_asignacionPro(){
        try
        {
            $extraBit = "";
            if($this->idlogtransaccional !== null && $this->idlogtransaccional !== '') {
                $extraBit = ', idlogtransaccional='.$this->idlogtransaccional;
            }
            // query to insert record
            $query = "UPDATE membresia SET idplan=:idplan, idusuarioprofesional=:idusuarioprofesional, fechacompra=:fechacompra, 
                      fechavencimiento=:fechavencimiento, estado=:estado".$extraBit." WHERE idmembresia =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idmembresia);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


    // get all planes
    public function get_all_planes(){
        try{
            $query 	= "SELECT idplan, nombreplan, costo, estado, mesesactivo, 
                       aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca,creacion FROM plan;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes
    public function get_all_planes2(){
        try{
            $query  = "SELECT idplan, nombreplan, costo, estado, mesesactivo, 
                       aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca,creacion FROM plan WHERE aplicacion_plan='2';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes for usuarios profesionales
    public function get_all_planespro(){
        try{
            $query  = "SELECT idplan, nombreplan, costo, estado, mesesactivo, 
                       aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca,creacion FROM plan WHERE aplicacion_plan='1';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get months in a plan
    public function get_mes(){
        try{
            $query 	= "SELECT mesesactivo FROM plan WHERE idplan = :id;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":id", $this->idplan);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function create_planFirma(){
        try
        {
            // query to insert record
            $query = "INSERT INTO plan (nombreplan, costo, estado, mesesactivo, aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca) 
						VALUES (:nombreplan,:costo,:estado,:mesesactivo,:aplicacion_plan,:especialidades,
						:numero_cuentas,:tiempo_cobro,:ubicacion,:biblioteca);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":nombreplan", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":numero_cuentas", $this->numCuentas);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);



            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function create_asignarProAFirma(){
        try
        {
            // query to insert record
            $query = "INSERT INTO detallefirmaprofesional (idfirmas,idusuarioprofesional) 
						VALUES (:idfirmas,:idusuarioprofesional);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);

            // execute query
            if($stmt->execute()){
                //Since this is successfull, proceed to get plan and assign it to usuarioprofesional
                $stmt = null;
                //Let's get the information for the plan the firma has
                $infoPlan = $this->get_asignacion();
                if($infoPlan){
                    //Set Data to either save it to user or update to the current plan the user has
                    $idplan = $infoPlan['idplan'];
                    $fechacompra = $infoPlan['fechacompra'];
                    $fechavencimiento = $infoPlan['fechavencimiento'];
                    $estado = $infoPlan['estado'];
                    $asignacionUsuario = $this->get_asignacionPro();
                    if($asignacionUsuario){
                        $this->idmembresia = $asignacionUsuario['idmembresia'];
                        $this->idplan = $idplan;
                        $this->fechacompra = $fechacompra;
                        $this->fechavencimiento = $fechavencimiento;
                        $this->estado = $estado;
                        $exeModAsignacionPro = $this->mod_asignacionPro();
                        if($exeModAsignacionPro){
                            return $exeModAsignacionPro;
                        }else{
                            return false;
                        }
                    }else{
                        $this->idplan = $idplan;
                        $this->fechacompra = $fechacompra;
                        $this->fechavencimiento = $fechavencimiento;
                        $this->estado = $estado;
                        $exeCreate_asignacionPro = $this->create_asignacionPro();
                        if($exeCreate_asignacionPro){
                            return $exeCreate_asignacionPro;
                        }else{
                            return false;
                        }
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

     public function create_asignacion(){
        try
        {
            // query to insert record
            $query = "INSERT INTO membresiafirma (idplan,idfirmas,fechacompra,fechavencimiento,estado) 
						VALUES (:idplan,:idfirmas,:fechacompra,:fechavencimiento,:estado);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }



    public function create_planPro(){
        
            try{
            // query to insert record
            $query = "INSERT INTO plan (nombreplan, costo, estado, mesesactivo, aplicacion_plan,especialidades,
                      tiempo_cobro, ubicacion, biblioteca) 
                        VALUES (:nombreplan,:costo,:estado,:mesesactivo,:aplicacion_plan,:especialidades,:tiempo_cobro,:ubicacion,:biblioteca);";

            // prepare query
            $stmt   = $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":nombreplan", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


    public function mod_planFirma(){
        try
        {
            // query to insert record
            $query = "UPDATE plan SET nombreplan=:nombre,costo=:costo,mesesactivo=:mesesactivo,
                      estado=:estado,aplicacion_plan=:aplicacion_plan,
                      especialidades=:especialidades,numero_cuentas=:numero_cuentas,
                      tiempo_cobro=:tiempo_cobro,ubicacion=:ubicacion,
                      biblioteca=:biblioteca WHERE idplan =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idplan);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":numero_cuentas", $this->numCuentas);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_asignacion(){
        try
        {
            // query to insert record
            $query = "UPDATE membresiafirma SET idplan=:idplan, idfirmas=:idfirmas, fechacompra=:fechacompra, 
                      fechavencimiento=:fechavencimiento, estado=:estado WHERE idmembresiafirma =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idmembresiafirma);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            $this->insertPlanToPros();

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function create_asignarPlan(){
        try
        {
            // query to insert record
            $query = "INSERT INTO membresiafirma (idplan, idfirmas, fechacompra, fechavencimiento, estado) 
						VALUES (:idplan,:idfirmas,:fechacompra,:fechavencimiento,:estado);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            $this->insertPlanToPros();

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


    //ASSIGN PLAN TO PRO USERS WITHIN FIRMAS
    // get all firmas
    public function insertPlanToPros(){
        try{
            $query 	= "SELECT idusuarioprofesional FROM detallefirmaprofesional WHERE idfirmas = :idfirma;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idfirma", $this->idfirmas);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

            if ($results){
                foreach ($results as $value) {
                    $this->idusuarioprofesional = $value['idusuarioprofesional'];
                    $query2 = "SELECT idmembresia FROM membresia WHERE idusuarioprofesional = :idpro AND estado = 1;";
                    $stmt2 = $this->conn->prepare( $query2 );
                    $stmt2->bindParam(":idpro", $this->idusuarioprofesional);
                    $stmt2->execute();
                    $results2 = $stmt2->fetch(PDO::FETCH_ASSOC);

                    if($results2) {
                        $this->idmembresia = $results2['idmembresia'];
                        $this->mod_asignacionPro();
                        return true;
                    } else {
                        $this->create_asignacionPro();
                        return true;
                    }


                }
            }
            return false;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }



    public function mod_planPro(){
     try
        {
            // query to insert record
            $query = "UPDATE plan SET nombreplan=:nombre,costo=:costo,mesesactivo=:mesesactivo,
                      estado=:estado,aplicacion_plan=:aplicacion_plan,
                      especialidades=:especialidades,tiempo_cobro=:tiempo_cobro,ubicacion=:ubicacion,
                      biblioteca=:biblioteca WHERE idplan =:id";

            // prepare query
            $stmt   = $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idplan);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    // get all firmas
    public function get_all_firmas(){
        try{
            $query 	= "SELECT idfirmas, nombre, estado, logo FROM firma WHERE estado = '1';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all firmas
    public function get_all_firmasWaPlan(){
        try{
            $query 	= "SELECT idfirmas, nombre, estado, logo FROM firma 
                       WHERE estado = '1' 
                       AND idfirmas IN (SELECT idfirmas FROM membresiafirma WHERE estado = '1');";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function del_ProFirma(){
        try
        {
            // query to delete detalleusuarioprofesional record
            $query = "DELETE FROM detallefirmaprofesional WHERE idusuarioprofesional = :id;";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuarioprofesional);


            // execute query
            if($stmt->execute()){
                $stmt = null;
                $query = "DELETE FROM membresia WHERE idusuarioprofesional = :id;";

                // prepare query
                $stmt 	= $this->conn->prepare($query);
                // bind values
                $stmt->bindParam(":id", $this->idusuarioprofesional);

                if($stmt->execute()){
                    return true;
                }else{
                  return false;
                }
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}
?>