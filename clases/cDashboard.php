<?php

class Dashboard {

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function getStatistcs() {
        try {
            $result = array();
            //Total de usuarios profesionales
            $query = "SELECT COUNT(idusuarioprofesional) AS 'UsuariosProActivos' 
                              FROM usuarioprofesional WHERE estadoverificacion = '1';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['UsuariosProActivos'] = $row['UsuariosProActivos'];

            $query  = null;
            $stmt   = null;
            $row    = null;


            //Total de usuarios profesionales en estado pendientes
            $query = "SELECT COUNT(idusuarioprofesional) AS 'UsuariosProPendientes' 
                              FROM usuarioprofesional WHERE estadoverificacion = '0';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['UsuariosProPendientes'] = $row['UsuariosProPendientes'];

            $query  = null;
            $stmt   = null;
            $row    = null;


            //Total de usuarios "persona natural"
            $query = "SELECT COUNT(idusuarioapp) AS 'UsuariosAppActivos' 
                      FROM usuarioapp WHERE idestadousuario = 2;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['UsuariosAppActivos'] = $row['UsuariosAppActivos'];

            $query  = null;
            $stmt   = null;
            $row    = null;


            //Total de usuarios "persona natural" estado pendiente
            $query = "SELECT COUNT(idusuarioapp) AS 'UsuariosAppPendientes' 
                      FROM usuarioapp WHERE idestadousuario = 1;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['UsuariosAppPendientes'] = $row['UsuariosAppPendientes'];

            $query  = null;
            $stmt   = null;
            $row    = null;

            //Total de preguntas express pendientes de contestar
            $query = "SELECT count(idpregunta) AS 'PreguntasExpPendientes' 
                      FROM legal_app.pregunta WHERE  estado = 0;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['PreguntasExpPendientes'] = $row['PreguntasExpPendientes'];

            $query  = null;
            $stmt   = null;
            $row    = null;

            //Total de preguntas directas pendientes de contestar
            $query = "SELECT count(idpreguntadirecta) AS 'PreguntasDirPendientes'
                      FROM legal_app.preguntadirecta WHERE estado = 0;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['PreguntasDirPendientes'] = $row['PreguntasDirPendientes'];

            $query  = null;
            $stmt   = null;
            $row    = null;

            //Total de cotizaciones pendientes de contestar
            $query = "SELECT count(idcotizaciones) AS 'cotiPendiente' 
                      FROM legal_app.cotizacionusuario WHERE estado = 0;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['cotiPendiente'] = $row['cotiPendiente'];

            $query  = null;
            $stmt   = null;
            $row    = null;


            //Monto total recaudado al mes actual.

            $startDate = date('Y-m-01');
            $endDate = date('Y-m-t');

            $query = "SELECT sum((porcentaje/100)*valor_neto) AS 'gananciaApp' 
                      FROM legal_app.log_ganancia_legalapp
                      WHERE timestamp BETWEEN '".$startDate." 00:00:01' AND '".$endDate." 23:59:59';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['gananciaApp'] = $row['gananciaApp'];
            //$result['gananciaApp'] = $query;

            $query  = null;
            $stmt   = null;
            $row    = null;

            //Monto total recaudado al mes actual.

            $startDate = date('Y-m-01');
            $endDate = date('Y-m-t');

            $query = "SELECT sum((porcentaje/100)*valor_neto) AS 'gananciaApp' 
                      FROM legal_app.log_ganancia_legalapp
                      WHERE timestamp BETWEEN '".$startDate." 00:00:01' AND '".$endDate." 23:59:59';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['gananciaApp'] = $row['gananciaApp'];
            //$result['gananciaApp'] = $query;

            $query  = null;
            $stmt   = null;
            $row    = null;

            //Get Memberships paid by pro members

            $startDate = date('Y-m-01');
            $endDate = date('Y-m-t');

            $query = "SELECT sum(valortransac) AS 'gananciaMember' 
                      FROM legal_app.logtransaccionalprofesional
                      WHERE creacion BETWEEN '".$startDate." 00:00:01' AND '".$endDate." 23:59:59';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $result['gananciaMember'] = $row['gananciaMember'];
            //$result['gananciaApp'] = $query;

            $query  = null;
            $stmt   = null;
            $row    = null;


            //Get Money from Users

            $startDate = date('Y-m-01');
            $endDate = date('Y-m-t');

            $query = "SELECT sum(cantidad) AS 'gananciaPE' 
                      FROM legal_app.log_debito_usuarioapp
                      WHERE datetime BETWEEN '".$startDate." 00:00:01' AND '".$endDate." 23:59:59';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $gananciaPE = $row['gananciaPE'];
            //$result['gananciaApp'] = $query;

            $query  = null;
            $stmt   = null;
            $row    = null;


            $query = "SELECT sum(valor) AS 'gananciaPD' 
                      FROM legal_app.log_debito_usuarioapp_pd
                      WHERE timestamp BETWEEN '".$startDate." 00:00:01' AND '".$endDate." 23:59:59';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $gananciaPD = $row['gananciaPD'];

            //JOIN AND PRESENT

            $result['gananciaPreguntas'] = $gananciaPD + $gananciaPE;

            $query  = null;
            $stmt   = null;
            $row    = null;




            //DEVOLVER DATOS
            return $result;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    // get all planes
    public function get_all_userpro(){
        try{
            $query 	= "SELECT * FROM usuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes
    public function get_all_planes2(){
        try{
            $query  = "SELECT idplan, nombreplan, costo, estado, mesesactivo, 
                       aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca,creacion FROM plan WHERE aplicacion_plan='2';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function mod_pro(){
        try
        {
            // query to insert record
            $query = "UPDATE usuarioprofesional SET idestadousuario=:idestadousuario,destacado=:destacado,
                      estadoverificacion=:estadoverificacion WHERE idusuarioprofesional =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuarioprofesional);
            // bind values
            $stmt->bindParam(":idestadousuario", $this->estadoUsuario);
            $stmt->bindParam(":destacado", $this->destacado);
            $stmt->bindParam(":estadoverificacion", $this->estadoVerificacion);

            // execute query
            if($stmt->execute()){
                if($this->estadoVerificacion == '1') {
                    $mensaje = "<br>Tu cuenta está Habilitada <br>
                                <br> El equipo de Legal App, te damos la Bienvenida nuevamente a la comunidad más grande de abogados de Bolivia.<br>
                                Ya puedes empezar a ganar con tu negocio en casa. Elíje el plan que se adapte a tus necesidades y contestale a tus clientes.
                                <br>Nuestros mejores deseos,
                                <br><br>F. El equipo de LegalBo";
                    $getUser = $this->get_usuariopro();
                    $para = strtolower($getUser['correo']);
                    $asunto = "Bienvenido, Usted ha sido aceptado en Legalbo";
                    $mailPHP 	= new PHPMailer();
                    //indico a la clase que use SMTP
                    $mailPHP->IsSMTP();
                    //permite modo debug para ver mensajes de las cosas que van ocurriendo
                    //$mailPHP->SMTPDebug = 2;
                    //Debo de hacer autenticaci�n SMTP
                    $mailPHP->SMTPAuth = true;
                    $mailPHP->SMTPSecure = "TLS";
                    //indico el servidor de Gmail para SMTP
                    $mailPHP->Host = "mail.smtp2go.com";
                    //indico el puerto que usa Gmail
                    $mailPHP->Port = 2525;
                    //indico un usuario / clave de un usuario de gmail
                    $mailPHP->Username = "";
                    $mailPHP->Password = '';
                    $mailPHP->SetFrom('', '');
                    $mailPHP->Subject = utf8_decode($asunto);
                    $mailPHP->MsgHTML($mensaje);
                    //indico destinatario
                    $address = $para;
                    $mailPHP->AddAddress($address, strtolower(trim($register['correo'])));
                    if(!$mailPHP->Send()) {
                        return false;
                    } else {
                        return true;
                    }
                }
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_asignacion(){
        try
        {
            // query to insert record
            $query = "UPDATE membresiafirma SET idplan=:idplan, idfirmas=:idfirmas, fechacompra=:fechacompra, 
                      fechavencimiento=:fechavencimiento, estado=:estado WHERE idmembresiafirma =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idmembresiafirma);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_planPro(){
        try
        {
            // query to insert record
            $query = "UPDATE plan SET nombreplan=:nombre,costo=:costo,mesesactivo=:mesesactivo,
                      estado=:estado,aplicacion_plan=:aplicacion_plan,
                      especialidades=:especialidades,tiempo_cobro=:tiempo_cobro,ubicacion=:ubicacion,
                      biblioteca=:biblioteca WHERE idplan =:id";

            // prepare query
            $stmt   = $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idplan);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    // get all firmas
    public function get_all_firmas(){
        try{
            $query 	= "SELECT idfirmas, nombre, estado, logo FROM firma WHERE estado = '1';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


}
?>