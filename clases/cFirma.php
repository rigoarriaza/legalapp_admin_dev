<?php
class Firma {

    public $idfirmas;
    public $estado;

    public $logofirma;


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_one_firma() {
        try {

            $query = "SELECT idfirmas,nombre,logo,estado FROM firma WHERE idfirmas=:idfirmas;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function get_all_firmas(){
        try{
            $query 	= "SELECT idfirmas,nombre,logo,estado FROM firma;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function create_firma(){
        try
        {
            // query to insert record
            $query = "INSERT INTO firma (nombre,logo,estado) 
						VALUES (:nombre,:logofirma,:estado);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":logofirma", $this->logofirma);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function modify_firma(){
        try
        {
            // query to insert record
            $query = "UPDATE firma SET nombre=:nombre,logo=:logofirma,estado=:estado 
						WHERE idfirmas=:idfirmas";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":logofirma", $this->logofirma);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}