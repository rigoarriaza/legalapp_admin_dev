<?php
class Transac {

    public $idtransaccionalapp;
    public $idtransaccional;

    public $mail;
    public $fecInicio;
    public $fecFin;

    //transactions
    public $tipoTrans;
    public $estadoTrans;
    public $tipoUsuario;

    //Debits
    public $tipoPre;
    public $estadoPre;


    //Transac Comments
    public $comentario;
    public $idtransac;
    public $type;

    //updateSaldo

    public $montoSaldo;
    public $idusuarioapp;



    public $costo;
    public $mesesActivo;
    public $descPlan;
    public $tipoPlan;
    public $numEspecialidad;
    public $numCuentas;
    public $ubicacion;
    public $biblioteca;
    public $estado;

    public $idfirmas;
    public $fechacompra;
    public $fechavencimiento;

    public $idmembresiafirma;
    public $idmembresia;
    public $idusuarioprofesional;


    public $idlog_debito_usuarioapp;
    public $idlog_debito_usuarioapp_pd;
    public $idpreguntadirecta;




    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function getSearch() {
        $query = "";
        $addText = "";
        try {
            if ($this->tipoUsuario == "1") {
                $query = "SELECT lta.idlogtransaccionalapp,lta.tipotrasac,lta.idreferencia,lta.valortransac,lta.estado,
                          lta.idusuarioapp, ua.correo,ua.nombre,ua.apellido,lta.creacion
                           FROM legal_app.logtransaccionalapp AS lta
                           INNER JOIN legal_app.usuarioapp AS ua
                           ON lta.idusuarioapp = ua.idusuarioapp
                            WHERE lta.idlogtransaccionalapp IS NOT NULL ";

                if($this->mail != ''){
                    $mail = $this->mail;
                    $addText = $addText."AND lta.idusuarioapp IN (SELECT idusuarioapp FROM usuarioapp WHERE correo LIKE '%$mail%') ";
                }

                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText."AND lta.creacion BETWEEN '$fecInicio' AND '$fecFin'";

                }

                if($this->tipoTrans != '' && $this->tipoTrans != '0'){
                    $tipoTrans = $this->tipoTrans;
                    $addText = $addText."AND lta.tipotrasac = '$tipoTrans' ";
                }

                if($this->estadoTrans != '' && $this->estadoTrans != 'na'){
                    $estadoTrans = $this->estadoTrans;
                    $addText = $addText."AND lta.estado = $estadoTrans";
                }

                $query = $query.$addText;
                //return $query;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );



            } else {
                $query = "SELECT ltp.idlogtransaccional,ltp.tipotransac,ltp.idreferencia,ltp.valortransac,ltp.estado,
                          ltp.idusuarioprofesional,up.nombre, up.apellido, up.correo,ltp.creacion
                           FROM legal_app.logtransaccionalprofesional AS ltp
                           INNER JOIN legal_app.usuarioprofesional AS up
                           ON ltp.idusuarioprofesional = up.idusuarioprofesional
                            WHERE ltp.idlogtransaccional IS NOT NULL ";

                if($this->mail != ''){
                    $mail = $this->mail;
                    $addText = $addText."AND up.correo LIKE '%$mail%' ";
                    $addText = $addText."AND ltp.idusuarioprofesional IN (SELECT idusuarioprofesional FROM usuarioprofesional WHERE correo LIKE '%$mail%') ";

                }

                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText."AND ltp.creacion BETWEEN '$fecInicio' AND '$fecFin'";

                }

                if($this->tipoTrans != '' && $this->tipoTrans != '0'){
                    $tipoTrans = $this->tipoTrans;
                    $addText = $addText."AND ltp.tipotransac = '$tipoTrans' ";
                }

                if($this->estadoTrans != '' && $this->estadoTrans != 'na'){
                    $estadoTrans = $this->estadoTrans;
                    $addText = $addText."AND ltp.estado = '$estadoTrans'";
                }

                $query = $query.$addText;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            }

            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function getSearchDebit() {
        $query = "";
        $addText = "";
        try {
            if ($this->tipoPre == "1") {
                $query = "SELECT ldue.datetime AS 'creacion', ldue.cantidad, ua.correo AS 'correoUsuario', p.estado AS 'estadoPregunta', p.titulo, ldue.idlog_debito_usuarioapp,
                          up.correo AS 'correoPro', rp.estado_pago AS 'estadoPago', p.idpregunta, rp.idrespuestapregunta, ua.idusuarioapp, up.idusuarioprofesional
                          FROM legal_app.log_debito_usuarioapp AS ldue
                          INNER JOIN legal_app.usuarioapp AS ua 
                          ON ldue.idusuarioapp = ua.idusuarioapp
                          INNER JOIN legal_app.pregunta AS p 
                          ON ldue.idpreguntae = p.idpregunta
                          INNER JOIN legal_app.respuestapregunta AS rp
                          ON p.idpregunta = rp.idpregunta
                          INNER JOIN legal_app.usuarioprofesional AS up
                          ON rp.idusuarioprofesional = up.idusuarioprofesional WHERE ldue.idlog_debito_usuarioapp IS NOT NULL ";

                if($this->mail != ''){
                    $mail = $this->mail;
                    $addText = $addText."AND p.idusuarioApp IN (SELECT idusuarioapp FROM usuarioapp WHERE correo LIKE '%$mail%') ";
                }

                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText."AND ldue.datetime BETWEEN '$fecInicio' AND '$fecFin'";

                }

                if($this->estadoPre != '' && $this->estadoPre != 'na'){
                    $estadoPre = $this->estadoPre;
                    $addText = $addText."AND p.estado = $estadoPre";
                }

                $query = $query.$addText;
                //return $query;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );



            } else {
                $query = "SELECT ldupd.idusuarioapp, ldupd.idpreguntadirecta, ldupd.valor AS 'cantidad', pd.titulopregunta AS 'titulo', ldupd.timestamp AS 'creacion',
                          ua.correo AS 'correoUsuario', up.correo AS 'correoPro', pd.estado AS 'estadoPregunta', pd.pago_usuario AS 'estadoPago', ldupd.idlog_debito_usuarioapp_pd
                          FROM legal_app.log_debito_usuarioapp_pd AS ldupd
                          INNER JOIN legal_app.preguntadirecta AS pd
                          ON ldupd.idpreguntadirecta = pd.idpreguntadirecta
                          INNER JOIN legal_app.usuarioapp AS ua
                          ON ldupd.idusuarioapp = ua.idusuarioapp
                          INNER JOIN legal_app.usuarioprofesional AS up
                          ON pd.idusuarioprofesional = up.idusuarioprofesional
                          WHERE ldupd.idlog_debito_usuarioapp_pd IS NOT NULL ";

                if($this->mail != ''){
                    $mail = $this->mail;
                    $addText = $addText."AND up.correo LIKE '%$mail%' ";
                    $addText = $addText."AND pd.idusuarioprofesional IN (SELECT idusuarioprofesional FROM usuarioprofesional WHERE correo LIKE '%$mail%') ";

                }

                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText."AND ldupd.timestamp BETWEEN '$fecInicio' AND '$fecFin'";

                }

                if($this->estadoPre != '' && $this->estadoPre != 'na'){
                    $estadoPre = $this->estadoPre;
                    $addText = $addText."AND pd.estado = '$estadoPre'";
                }

                $query = $query.$addText;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            }

            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function getSearchPreguntas() {
        $query = "";
        $addText = "";
        try {
            if ($this->tipoPre == "1") {
                $query = "SELECT idpregunta,titulo,idusuarioApp,idusuarioprofesional,creacion, estado
                          FROM pregunta
                          WHERE idpregunta IS NOT NULL ";

                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText."AND creacion BETWEEN '$fecInicio' AND '$fecFin' ORDER BY creacion DESC";

                }

                $query = $query.$addText;
                //return $query;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

            } else {
                $query = "SELECT idpreguntadirecta,titulopregunta as 'titulo',idusuarioapp,idusuarioprofesional, creacionPreguntaDir AS 'creacion',estado
                            FROM preguntadirecta
                            WHERE idpreguntadirecta IS NOT NULL ";


                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText."AND creacionPreguntaDir BETWEEN '$fecInicio' AND '$fecFin' ORDER BY creacionPreguntaDir DESC";

                }

                $query = $query.$addText;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );


            }

            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    function getDetailsUser($iduser){
        try {

            $query = "SELECT correo, CONCAT(nombre,' ', apellido) AS nombreUsuario FROM usuarioapp WHERE idusuarioapp = :idusuarioapp";

            //return $query;
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioapp", $iduser);
            $stmt->execute();
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    function getDetailsPro($iduser){
        try {

            $query = "SELECT correo, CONCAT(nombre,' ', apellido) AS nombrePro 
                      FROM usuarioprofesional WHERE idusuarioprofesional = :idusuarioprofesional";

            //return $query;
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $iduser);
            $stmt->execute();
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }


    public function getDetailExpress() {
        try {

        $query = "SELECT ldue.datetime AS 'creacion', ldue.cantidad, ua.correo AS 'correoUsuario', 
                  p.estado AS 'estadoPregunta', p.titulo, ldue.idlog_debito_usuarioapp,
                  up.correo AS 'correoPro', rp.estado_pago AS 'estadoPago', p.idpregunta, 
                  rp.idrespuestapregunta, ua.idusuarioapp, up.idusuarioprofesional, ldue.comentario,
                  p.contenido, p.privacidad, rp.respuestacontenido, rp.calificacion, rp.pago_usuario
                  FROM legal_app.log_debito_usuarioapp AS ldue
                  INNER JOIN legal_app.usuarioapp AS ua 
                  ON ldue.idusuarioapp = ua.idusuarioapp
                  INNER JOIN legal_app.pregunta AS p 
                  ON ldue.idpreguntae = p.idpregunta
                  INNER JOIN legal_app.respuestapregunta AS rp
                  ON p.idpregunta = rp.idpregunta
                  INNER JOIN legal_app.usuarioprofesional AS up
                  ON rp.idusuarioprofesional = up.idusuarioprofesional WHERE ldue.idlog_debito_usuarioapp = :idlog_debito_usuarioapp";

                //return $query;
                $stmt = $this->conn->prepare( $query );
                $stmt->bindParam(":idlog_debito_usuarioapp", $this->idlog_debito_usuarioapp);
                $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function getDetailDirecta() {
        try {

            $query = "SELECT ldupd.idusuarioapp, ldupd.idpreguntadirecta, ldupd.valor AS 'cantidad', 
                      pd.titulopregunta AS 'titulo', 
                      ldupd.timestamp AS 'creacion', ua.correo AS 'correoUsuario', up.correo AS 'correoPro', 
                      pd.estado AS 'estadoPregunta', ldupd.idlog_debito_usuarioapp_pd, ldupd.comentario,
                      pd.contenido, pd.privado AS 'privacidad', pd.pago_usuario, pd.idpreguntadirecta 
                          FROM legal_app.log_debito_usuarioapp_pd AS ldupd
                          INNER JOIN legal_app.preguntadirecta AS pd
                          ON ldupd.idpreguntadirecta = pd.idpreguntadirecta
                          INNER JOIN legal_app.usuarioapp AS ua
                          ON ldupd.idusuarioapp = ua.idusuarioapp
                          INNER JOIN legal_app.usuarioprofesional AS up
                          ON pd.idusuarioprofesional = up.idusuarioprofesional
                          WHERE ldupd.idlog_debito_usuarioapp_pd = :idlog_debito_usuarioapp_pd ";

            //return $query;
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idlog_debito_usuarioapp_pd", $this->idlog_debito_usuarioapp_pd);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function getExchange() {
        try {

            $query = "SELECT datetime AS 'fecha', tipo_respuesta, contenido FROM legal_app.detalle_preguntadirecta
                          WHERE idpreguntadirecta = :idpreguntadirecta; ";

            //return $query;
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idpreguntadirecta", $this->idpreguntadirecta);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );

            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function get_Transaccion_App() {
        try {

            $query = "SELECT lta.idlogtransaccionalapp,lta.tipotrasac,lta.idreferencia,lta.valortransac,ua.correo, lta.estado, lta.comentario,
                      CONCAT(ua.nombre,' ',ua.apellido) AS nombreCompleto,lta.creacion,lta.personapago,lta.lugar,lta.agencia,
                      lta.operador
                      FROM legal_app.logtransaccionalapp AS lta
                      INNER JOIN legal_app.usuarioapp AS ua ON 
                      lta.idusuarioapp = ua.idusuarioapp
                      WHERE lta.idlogtransaccionalapp = :idtransaccionalapp;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idtransaccionalapp", $this->idtransaccionalapp);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }


    public function get_Transaccion_Pro() {
        try {

            $query = "SELECT ltp.idlogtransaccional,ltp.tipotransac,ltp.idreferencia,ltp.valortransac,up.correo, ltp.estado, ltp.comentario,
                      CONCAT(up.nombre,' ',up.apellido) AS nombreCompleto,ltp.creacion,ltp.personapago, ltp.lugar,ltp.agencia,
                      ltp.operador
                      FROM legal_app.logtransaccionalprofesional AS ltp
                      INNER JOIN legal_app.usuarioprofesional AS up ON 
                      ltp.idusuarioprofesional = up.idusuarioprofesional
                      WHERE ltp.idlogtransaccional = :idtransaccional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idtransaccional", $this->idtransaccional);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function updateComment(){
        try
        {
            if ($this->type == 'app') {
            // query to insert record
                $query = "UPDATE legal_app.logtransaccionalapp SET comentario=:comentario WHERE idlogtransaccionalapp=:id";

            } else {
                // query to insert record
                $query = "UPDATE legal_app.logtransaccionalprofesional SET comentario=:comentario WHERE idlogtransaccional=:id";
            }

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idtransac);
            // bind values
            $stmt->bindParam(":comentario", $this->comentario);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function updateCommentDebit(){
        try
        {
            if ($this->type == 'PE') {
                // query to insert record
                $query = "UPDATE legal_app.log_debito_usuarioapp SET comentario=:comentario WHERE idlog_debito_usuarioapp=:id";

            } else {
                //echo 'PREGUNTA DIRECTA';
                // query to insert record
                $query = "UPDATE legal_app.log_debito_usuarioapp_pd SET comentario=:comentario WHERE idlog_debito_usuarioapp_pd=:id";
            }

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idtransac);
            // bind values
            $stmt->bindParam(":comentario", $this->comentario);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function updateSaldoUser(){
        try
        {
               // query to insert record
                $query = "UPDATE legal_app.usuarioapp SET saldo = saldo + :saldo WHERE idusuarioapp=:id";

            $truSaldo  = (int)$this->montoSaldo;
            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuarioapp);
            // bind values
            $stmt->bindParam(":saldo", $truSaldo);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


}
?>