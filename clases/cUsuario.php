<?php
class Usuario {
	public $mail;
	public $pass;
	public $idusuario;
	
	public $nombre;
	public $apellido;
	public $idrol;
	public $estado;
	public $password;
	


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	public function doLogin() {
		try
		{
			// query to insert record
			$query = "SELECT idusuarioweb,nombre,apellido,correo,idestadousuario,idrolweb
					FROM usuarioweb WHERE correo=:correo AND password=:pass AND idestadousuario=2;";

			// prepare query
			$stmt = $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":correo", $this->mail);
			$stmt->bindParam(":pass",$this->pass);
			// execute query
			$stmt->execute();
			$data = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($stmt->rowCount() == 1) {
                    $_SESSION['legalapp']['idusuario']     			= trim($data['idusuarioweb']);
					$_SESSION['legalapp']['nombre']   				= trim($data['nombre']);
					$_SESSION['legalapp']['apellido']   			= trim($data['apellido']);
                    $_SESSION['legalapp']['nombreCompleto']         = $_SESSION['legalapp']['nombre'].' '.$_SESSION['legalapp']['apellido'];
					$_SESSION['legalapp']['correo']   				= trim($data['correo']);
					$_SESSION['legalapp']['estado']   				= trim($data['idestadousuario']);
					$_SESSION['legalapp']['idrol']   				= trim($data['idrolweb']);
					$_SESSION['legalapp']['user_session']   		= true;
					return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}

	}

    public function is_loggedin() {
        if(isset($_SESSION['legalapp']['user_session'] )) {
            return true;
        }else{
			session_destroy();
			return false;
		}
    }
	public function logout() {
        session_destroy();
        unset($_SESSION['legalapp']['user_session']);
        return true;
    }

    public function get_user() { 
        try {

            $query = "SELECT u.idusuarioweb,u.nombre,u.idrolweb,u.apellido,u.creacion,u.correo,
            				u.creacion, u.idestadousuario as idestado, eu.nombre as estado, r.nombre AS rol 
            			FROM usuarioweb as u 
            			inner join rolweb as r on r.idrolweb=u.idrolweb 
            			inner join estadousuario as eu on eu.idestadousuario = u.idestadousuario
						WHERE u.idusuarioweb= :idusuario";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	
	// get user
	public function get_all_user(){
		try{
			$query 	= "SELECT u.idusuarioweb,u.nombre,u.apellido,u.creacion,u.correo,
			eu.idestadousuario as idestado,eu.nombre as estado,u.creacion,r.nombre AS rol 
			FROM usuarioweb as u inner join rolweb as r on r.idrolweb=u.idrolweb 
			INNER JOIN estadousuario as eu on u.idestadousuario=eu.idestadousuario";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_user(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO usuarioweb (nombre,apellido,correo,idestadousuario,idrolweb,password) 
						VALUES (:nombre,:apellido,:correo,:estado,:idrol,:password);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":apellido", $this->apellido);
			$stmt->bindParam(":correo", $this->mail);
			$stmt->bindParam(":estado", $this->estado);
			$stmt->bindParam(":idrol", $this->idrol);
			$stmt->bindParam(":password", $this->password);

			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_verificacion_mail(){
        try {

            $query = "SELECT *
                    FROM usuarioweb
                    WHERE correo=:mail";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":mail", $this->mail);
            $stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				return true;
			}else{
				return false;
			}
        } catch (PDOException $e) {
			return false;
        }
    }
	
	public function modify_user(){
		try
		{
			// query to insert record
			$query = "UPDATE usuarioweb SET nombre=:nombre,idrolweb=:idrol,apellido=:apellido, correo=:mail,
						idestadousuario=:estado WHERE idusuarioweb =:id";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":id", $this->idusuario);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":apellido", $this->apellido);
			$stmt->bindParam(":idrol", $this->idrol);
			$stmt->bindParam(":estado", $this->estado);
			$stmt->bindParam(":mail", $this->mail);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	public function modify_user2(){
		try
		{
			// query to insert record
			$query = "UPDATE usuarioweb SET nombre=:nombre,password=:password
						,idrolweb=:idrol,apellido=:apellido,idestadousuario=:estado WHERE idusuarioweb =:id";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":id", $this->idusuario);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":password", $this->password);
			$stmt->bindParam(":apellido", $this->apellido);
			$stmt->bindParam(":idrol", $this->idrol);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}

		public function getEstadosUsuario() {
		try
		{
			// query to insert record
			$query = "SELECT idestadousuario, nombre FROM estadousuario;";

			// prepare query
			$stmt = $this->conn->prepare($query);
			// execute query
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch(PDOException $ex) {

			echo $ex->getMessage();
			return false;
		}

	}

		public function getRoles() {
		try
		{
			// query to insert record
			$query = "SELECT idrolweb, nombre FROM rolweb WHERE estado = '1';";

			// prepare query
			$stmt = $this->conn->prepare($query);
			// execute query
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch(PDOException $ex) {

			echo $ex->getMessage();
			return false;
		}

	}
}
?>