<?php
class UsuarioCobro {
    public $idusuario;
    public $possibleUser;

    public $user;
    public $secret;
    public $org;
    public $estado;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }



    public function get_user() {
        try {

            $query 	= "SELECT idusuario_cobro, username,secret_key,organizacion,estado,fecha_creacion
                        FROM legal_app.usuario_cobro WHERE idusuario_cobro= :idusuario";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function get_possible() {
        try {

            $query 	= "SELECT username
                        FROM legal_app.usuario_cobro WHERE username= :current";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":current", $this->possibleUser);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    // get user
    public function get_all_userCobro(){
        try{
            $query 	= "SELECT idusuario_cobro, username,secret_key,organizacion,estado,fecha_creacion
                        FROM legal_app.usuario_cobro ";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function modify_usuariocobro(){
        try
        {
            // query to insert record
            $query = "UPDATE legal_app.usuario_cobro SET estado=:estado, secret_key=:secret, organizacion=:org WHERE idusuario_cobro =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuario);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":org", $this->org);
            $stmt->bindParam(":secret", $this->secret);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function create_usuariocobro(){
        try
        {
            // query to insert record
            $query = "INSERT INTO legal_app.usuario_cobro (username, secret_key, organizacion, estado) 
						VALUES (:usuario,:secret,:org,:estado);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":usuario", $this->user);
            $stmt->bindParam(":secret", $this->secret);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":org", $this->org);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}
?>