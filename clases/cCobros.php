<?php


class Cobros {


    public $idusuarioprofesional;

    public $idpregunta;
    public $estadoUsuario;
    public $estadoVerificacion;
    public $idWeb;
    public $comentario;


    public $mail;
    public $fecInicio;
    public $fecFin;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function executePayment(){
        try
        {
            //Saving comment despite result
            $this->updateComment();
            $error = false;
            //get the data to be placed in new table: function get_data_to_collect

            $dataToCollect = $this->get_data_to_collect();
            //vforeach ($object AS $row => $element) {
            foreach ($dataToCollect as $row => $element){

                /*
                 * idusuarioprofesional
                 * idlog_ganancia_profesional
                 * idusuarioweb
                 * */

                $query = "INSERT INTO  log_ganancias_entregadas_profesional (idusuarioprofesional, idlog_ganancia_profesional, idusuarioweb) 
                          VALUES (:idpro,:idlogG,:idweb)";


                // prepare query
                $stmt 	= $this->conn->prepare($query);
                // bind values
                $stmt->bindParam(":idpro", $this->idusuarioprofesional);
                // bind values
                $stmt->bindParam(":idweb", $this->idWeb);
                $stmt->bindParam(":idlogG", $element['idlog_ganancia_profesional']);

                // execute query
                if($stmt->execute()){

                }else{
                    $error = true;
                }
            }

            if($error){
//                $query2 = "DELETE FROM  log_ganancias_entregadas_profesional WHERE idusuarioprofesional = :id";
//
//                // prepare query
//                $stmt2 	= $this->conn->prepare($query2);
//                // bind values
//                $stmt2->bindParam(":id", $this->idusuarioprofesional);
//
//                $stmt2->execute();

                return false;
            } else {

                return true;
            }

        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


    public function updateComment(){
        try
        {

            $query = "UPDATE  usuarioprofesional SET comentariosPago=:comentario WHERE idusuarioprofesional=:id";


            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuarioprofesional);
            // bind values
            $stmt->bindParam(":comentario", $this->comentario);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function get_data_PE() {
        try {

            $query = "SELECT p.idpregunta, p.titulo,p.contenido,p.estado,p.creacion,ua.correo AS 'usuarioApp'
                      FROM  pregunta AS p
                      INNER JOIN  usuarioapp AS ua
                      ON p.idusuarioApp = ua.idusuarioapp 
                      WHERE p.idpregunta = :idpregunta ;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idpregunta", $this->idpregunta);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function get_data_PD() {
        try {

            $query = "SELECT pd.idpreguntadirecta, pd.titulopregunta AS 'titulo',pd.contenido,pd.estado,pd.creacionPreguntaDir AS 'creacion',
                      ua.correo AS 'usuarioApp'
                      FROM  preguntadirecta AS pd
                      INNER JOIN  usuarioapp AS ua
                      ON pd.idusuarioApp = ua.idusuarioapp 
                      WHERE pd.idpreguntadirecta = :idpregunta ;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idpregunta", $this->idpregunta);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    public function get_detail_pro() {
        try {

            $query = "SELECT up.idusuarioprofesional, CONCAT(up.nombre, ' ',up.apellido) AS 'nombreCompleto',
                      up.correo, up.carnet, up.fotopersonal, up.creacion, up.comentariosPago 
                      FROM  usuarioprofesional AS up
                      WHERE up.idestadousuario = 2 AND up.idusuarioprofesional = :idusuarioprofesional ;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

    // get all planes
    public function get_all_pros_avail(){
        try{
            $query 	= "SELECT up.idusuarioprofesional, CONCAT(up.nombre, ' ',up.apellido) AS 'nombreCompleto',
                       up.correo, up.carnet, up.creacion, SUM(valor) AS 'ganancia'
                       FROM  usuarioprofesional AS up
                       INNER JOIN  log_ganancia_profesional AS lgp
                       ON up.idusuarioprofesional = lgp.idusuarioprofesional
                       WHERE up.idestadousuario = 2 AND lgp.idlog_ganancia_profesional 
                       NOT IN (SELECT idlog_ganancia_profesional FROM  log_ganancias_entregadas_profesional) 
                       GROUP BY up.idusuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes
    public function get_data_to_collect(){
        try{
            $query  = "SELECT idlog_ganancia_profesional, idusuarioprofesional, tipo, idpregunta, timestamp, valor 
                       FROM  log_ganancia_profesional 
                       WHERE idlog_ganancia_profesional 
                       NOT IN (SELECT idlog_ganancia_profesional FROM  log_ganancias_entregadas_profesional)
                       AND idusuarioprofesional = :idusuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes
    public function get_historic_data(){
        try{
            $query  = "SELECT lgp.idlog_ganancia_profesional, lgp.idusuarioprofesional, lgp.tipo, lgp.idpregunta, 
                       lgp.timestamp, lgp.valor, CONCAT(uw.nombre,' ',uw.apellido) AS 'nombreWeb', uw.correo AS 'correoWeb',
                       lep.creacion AS 'entregado'
                       FROM  log_ganancia_profesional AS lgp
                       INNER JOIN  log_ganancias_entregadas_profesional AS lep
                       ON lgp.idlog_ganancia_profesional = lep.idlog_ganancia_profesional
                       INNER JOIN  usuarioweb AS uw
                       ON lep.idusuarioweb = uw.idusuarioweb
                       WHERE lgp.idlog_ganancia_profesional 
                       IN (SELECT idlog_ganancia_profesional FROM  log_ganancias_entregadas_profesional)
                       AND lgp.idusuarioprofesional = :idusuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes
    public function get_all_planes2(){
        try{
            $query  = "SELECT idplan, nombreplan, costo, estado, mesesactivo, 
                       aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca,creacion FROM plan WHERE aplicacion_plan='2';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function mod_pro(){
        try
        {
            // query to insert record
            $query = "UPDATE usuarioprofesional SET idestadousuario=:idestadousuario,destacado=:destacado,
                      estadoverificacion=:estadoverificacion WHERE idusuarioprofesional =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuarioprofesional);
            // bind values
            $stmt->bindParam(":idestadousuario", $this->estadoUsuario);
            $stmt->bindParam(":destacado", $this->destacado);
            $stmt->bindParam(":estadoverificacion", $this->estadoVerificacion);

            // execute query
            if($stmt->execute()){
//                if($this->estadoVerificacion == '1') {
//                    $mensaje = "<br>Tu cuenta está Habilitada <br>
//                                <br> El equipo de Legal App, te damos la Bienvenida nuevamente a la comunidad más grande de abogados de Bolivia.<br>
//                                Ya puedes empezar a ganar con tu negocio en casa. Elíje el plan que se adapte a tus necesidades y contestale a tus clientes.
//                                <br>Nuestros mejores deseos,
//                                <br><br>F. El equipo de LegalBo";
//                    $getUser = $this->get_usuariopro();
//                    $para = strtolower($getUser['correo']);
//                    $asunto = "Bienvenido, Usted ha sido aceptado en Legalbo";
//                    $mailPHP 	= new PHPMailer();
//                    //indico a la clase que use SMTP
//                    $mailPHP->IsSMTP();
//                    //permite modo debug para ver mensajes de las cosas que van ocurriendo
//                    //$mailPHP->SMTPDebug = 2;
//                    //Debo de hacer autenticaci�n SMTP
//                    $mailPHP->SMTPAuth = true;
//                    $mailPHP->SMTPSecure = "TLS";
//                    //indico el servidor de Gmail para SMTP
//                    $mailPHP->Host = "mail.smtp2go.com";
//                    //indico el puerto que usa Gmail
//                    $mailPHP->Port = 2525;
//                    //indico un usuario / clave de un usuario de gmail
//                    $mailPHP->Username = "";
//                    $mailPHP->Password = '';
//                    $mailPHP->SetFrom('', '');
//                    $mailPHP->Subject = utf8_decode($asunto);
//                    $mailPHP->MsgHTML($mensaje);
//                    //indico destinatario
//                    $address = $para;
//                    $mailPHP->AddAddress($address, strtolower(trim($register['correo'])));
//                    if(!$mailPHP->Send()) {
//                        return false;
//                    } else {
//                        return true;
//                    }
//                }
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_asignacion(){
        try
        {
            // query to insert record
            $query = "UPDATE membresiafirma SET idplan=:idplan, idfirmas=:idfirmas, fechacompra=:fechacompra, 
                      fechavencimiento=:fechavencimiento, estado=:estado WHERE idmembresiafirma =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idmembresiafirma);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_planPro(){
        try
        {
            // query to insert record
            $query = "UPDATE plan SET nombreplan=:nombre,costo=:costo,mesesactivo=:mesesactivo,
                      estado=:estado,aplicacion_plan=:aplicacion_plan,
                      especialidades=:especialidades,tiempo_cobro=:tiempo_cobro,ubicacion=:ubicacion,
                      biblioteca=:biblioteca WHERE idplan =:id";

            // prepare query
            $stmt   = $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idplan);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    // get all firmas
    public function get_all_firmas(){
        try{
            $query 	= "SELECT idfirmas, nombre, estado, logo FROM firma WHERE estado = '1';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function getSearchCobros() {
        $query = "";
        $addText = "";
        try {
            // if ($this->tipoPre == "1") {
                $query = "SELECT ge.*, sum(gp.valor) as monto, u.nombre, u.apellido, u.correo  FROM `log_ganancias_entregadas_profesional` ge 
                            join log_ganancia_profesional gp on ge.idlog_ganancia_profesional = gp.idlog_ganancia_profesional
                            join usuarioprofesional u on ge.idusuarioprofesional = u.idusuarioprofesional  
                            where ge.idlog_ganancias_entregadas_profesional IS NOT NULL ";

                if($this->mail != ''){
                    $mail = $this->mail;
                    $addText = $addText." AND u. correo LIKE '%$mail%' ";
                }

                if($this->fecInicio != '' && $this->fecFin != ''){
                    $fecInicio = $this->fecInicio;
                    $fecFin = $this->fecFin;

                    $fec1 = explode('/',$fecInicio);
                    $fec2 = explode('/',$fecFin);

                    $fecInicio  = $fec1[2].'-'.$fec1[0].'-'.$fec1[1].' 00:00:01';
                    $fecFin     = $fec2[2].'-'.$fec2[0].'-'.$fec2[1].' 23:59:59';

                    $addText = $addText." AND ge.creacion BETWEEN '$fecInicio' AND '$fecFin'";

                }

                $query = $query.$addText.' group by ge.creacion;';
                //return $query;
                $stmt = $this->conn->prepare( $query );
                $stmt->execute();
                $results = $stmt->fetchAll( PDO::FETCH_ASSOC );


            return $results;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }

}
?>