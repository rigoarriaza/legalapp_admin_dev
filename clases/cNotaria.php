<?php
class Notaria {

    public $idnotaria;

    public $nombre;
    public $detalle;
    public $latitud;
    public $longitud;
    public $estado;




    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_nota() {
        try {

            $query = "SELECT idnotaria, nombre, detalle, estado, latitud, longitud, creacion 
                      FROM legal_app.notaria WHERE idnotaria = :idnotaria;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idnotaria", $this->idnotaria);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    // get user
    public function get_all_notarias(){
        try{
            $query 	= "SELECT idnotaria, nombre, detalle, estado, latitud, longitud, creacion FROM legal_app.notaria;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function create_nota(){
        try
        {
            // query to insert record
            $query = "INSERT INTO legal_app.notaria (nombre,detalle,estado,latitud,longitud) 
						VALUES (:nombre,:detalle,:estado,:latitud,:longitud);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":detalle", $this->detalle);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":latitud", $this->latitud);
            $stmt->bindParam(":longitud", $this->longitud);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


    public function modify_nota(){
        try
        {
            // query to insert record
            $query = "UPDATE notaria SET nombre=:nombre,detalle=:detalle,
						estado=:estado, latitud = :latitud, longitud=:longitud WHERE idnotaria =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idnotaria);
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":detalle", $this->detalle);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":latitud", $this->latitud);
            $stmt->bindParam(":longitud", $this->longitud);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}
?>