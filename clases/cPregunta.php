<?php
class Pregunta {
	
	public $id;




    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	
    public function getInfoExpress() { 
        try {

            $query = "SELECT p.*, CONCAT(u.nombre, ' ', u.apellido) AS usuario, CONCAT(pro.nombre,' ', pro.apellido) as profesional, deb.cantidad
                    FROM pregunta p join usuarioapp u on p.idusuarioApp = u.idusuarioapp LEFT JOIN 
                    usuarioprofesional pro on p.idusuarioprofesional= pro.idusuarioprofesional LEFT JOIN 
                    log_debito_usuarioapp deb on p.idpregunta = deb.idpreguntae 
                    where p.idpregunta=:id";

                
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":id", $this->id);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }	
	
    public function getInfoDirecta() { 
        try {

            $query = "SELECT p.*, CONCAT(u.nombre, ' ', u.apellido) AS usuario, CONCAT(pro.nombre,' ', pro.apellido) as profesional, deb.valor 
                    FROM preguntadirecta p join usuarioapp u on p.idusuarioApp = u.idusuarioapp 
                    LEFT JOIN usuarioprofesional pro on p.idusuarioprofesional= pro.idusuarioprofesional 
                    LEFT JOIN log_debito_usuarioapp_pd deb on p.idpreguntadirecta = deb.idpreguntadirecta where p.idpreguntadirecta=:id";

                
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":id", $this->id);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }	

    public function getInfoExpressDetalle() { 
        try {

            $query = "SELECT * FROM detalle_preguntaexpress where idpregunta=:id";

                
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":id", $this->id);
            $stmt->execute();
            
            $i = 0;
            if ( $stmt->rowCount() > 0 ) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $array[$i]['contenido']   	 	= trim($row['contenido']);
                    $array[$i]['tipo_respuesta']   	 	= trim($row['tipo_respuesta']);
                    $array[$i]['datetime']   	 	= trim($row['datetime']);
                    $i++;
                }
                return '{"msg": '.json_encode($array).'}';
            }else{
                return false;
            }

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }
    public function getInfoDirectaDetalle() { 
        try {

            $query = "SELECT * FROM detalle_preguntadirecta where idpreguntadirecta=:id";

                
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":id", $this->id);
            $stmt->execute();
            
            $i = 0;
            if ( $stmt->rowCount() > 0 ) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $array[$i]['contenido']   	 	= trim($row['contenido']);
                    $array[$i]['tipo_respuesta']   	 	= trim($row['tipo_respuesta']);
                    $array[$i]['datetime']   	 	= trim($row['datetime']);
                    $i++;
                }
                return '{"msg": '.json_encode($array).'}';
            }else{
                return false;
            }

        } catch (PDOException $e) {

            echo $e->getMessage();

        }
    }
	
	
	// get user
	public function get_all_espe(){
		try{
			$query 	= "SELECT idespecialidad, nombre, descripcion, estado FROM especialidad;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_espe(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO especialidad (nombre,descripcion,estado,imagen) 
						VALUES (:nombre,:descripcion,:estado,:imagen);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":descripcion", $this->descripcion);
			$stmt->bindParam(":estado", $this->estado);
			$stmt->bindParam(":imagen", $this->imagen);

			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	public function modify_espe(){
		try
		{
			// query to insert record
			$query = "UPDATE especialidad SET nombre=:nombre,descripcion=:descripcion,
						estado=:estado,imagen=:imagen WHERE idespecialidad =:id";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":id", $this->idespecialidad);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":descripcion", $this->descripcion);
			$stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":imagen", $this->imagen);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>