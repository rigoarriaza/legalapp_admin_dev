<?php


// para envios de correo
require '../PHPmailer/PHPMailerAutoload.php';
require '../PHPmailer/class.phpmailer.php';

class Usuariopro {


    public $idusuarioprofesional;

    public $destacado;
    public $estadoUsuario;
    public $estadoVerificacion;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_usuariopro() {
        try {

            $query = "SELECT idusuarioprofesional, nombre, apellido, correo, celular, direccion,carnet,refencia,
                      estadoverificacion, fotopersonal, descripcionprofesional, creacion, idestadousuario, titulo, documento,
                       numero_registro, destacado FROM usuarioprofesional WHERE idusuarioprofesional = :idusuarioprofesional;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioprofesional", $this->idusuarioprofesional);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            
            echo $e->getMessage();
            
        }
    }

    // get all planes
    public function get_all_userpro(){
        try{
            $query 	= "SELECT * FROM usuarioprofesional ORDER BY creacion DESC ;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // get all planes
    public function get_all_planes2(){
        try{
            $query  = "SELECT idplan, nombreplan, costo, estado, mesesactivo, 
                       aplicacion_plan,especialidades,numero_cuentas,
                      tiempo_cobro, ubicacion, biblioteca,creacion FROM plan WHERE aplicacion_plan='2';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function mod_pro(){
        try
        {
            // query to insert record
            $query = "UPDATE usuarioprofesional SET idestadousuario=:idestadousuario,destacado=:destacado,
                      estadoverificacion=:estadoverificacion WHERE idusuarioprofesional =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuarioprofesional);
            // bind values
            $stmt->bindParam(":idestadousuario", $this->estadoUsuario);
            $stmt->bindParam(":destacado", $this->destacado);
            $stmt->bindParam(":estadoverificacion", $this->estadoVerificacion);

            // execute query
            if($stmt->execute()){
//                if($this->estadoVerificacion == '1') {
//                    $mensaje = "<br>Tu cuenta está Habilitada <br>
//                                <br> El equipo de Legal App, te damos la Bienvenida nuevamente a la comunidad más grande de abogados de Bolivia.<br>
//                                Ya puedes empezar a ganar con tu negocio en casa. Elíje el plan que se adapte a tus necesidades y contestale a tus clientes.
//                                <br>Nuestros mejores deseos,
//                                <br><br>F. El equipo de LegalBo";
//                    $getUser = $this->get_usuariopro();
//                    $para = strtolower($getUser['correo']);
//                    $asunto = "Bienvenido, Usted ha sido aceptado en Legalbo";
//                    $mailPHP 	= new PHPMailer();
//                    //indico a la clase que use SMTP
//                    $mailPHP->IsSMTP();
//                    //permite modo debug para ver mensajes de las cosas que van ocurriendo
//                    //$mailPHP->SMTPDebug = 2;
//                    //Debo de hacer autenticaci�n SMTP
//                    $mailPHP->SMTPAuth = true;
//                    $mailPHP->SMTPSecure = "TLS";
//                    //indico el servidor de Gmail para SMTP
//                    $mailPHP->Host = "mail.smtp2go.com";
//                    //indico el puerto que usa Gmail
//                    $mailPHP->Port = 2525;
//                    //indico un usuario / clave de un usuario de gmail
//                    $mailPHP->Username = "";
//                    $mailPHP->Password = '';
//                    $mailPHP->SetFrom('', '');
//                    $mailPHP->Subject = utf8_decode($asunto);
//                    $mailPHP->MsgHTML($mensaje);
//                    //indico destinatario
//                    $address = $para;
//                    $mailPHP->AddAddress($address, strtolower(trim($register['correo'])));
//                    if(!$mailPHP->Send()) {
//                        return false;
//                    } else {
//                        return true;
//                    }
//                }
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_asignacion(){
        try
        {
            // query to insert record
            $query = "UPDATE membresiafirma SET idplan=:idplan, idfirmas=:idfirmas, fechacompra=:fechacompra, 
                      fechavencimiento=:fechavencimiento, estado=:estado WHERE idmembresiafirma =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idmembresiafirma);
            // bind values
            $stmt->bindParam(":idplan", $this->idplan);
            $stmt->bindParam(":idfirmas", $this->idfirmas);
            $stmt->bindParam(":fechacompra", $this->fechacompra);
            $stmt->bindParam(":fechavencimiento", $this->fechavencimiento);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function mod_planPro(){
     try
        {
            // query to insert record
            $query = "UPDATE plan SET nombreplan=:nombre,costo=:costo,mesesactivo=:mesesactivo,
                      estado=:estado,aplicacion_plan=:aplicacion_plan,
                      especialidades=:especialidades,tiempo_cobro=:tiempo_cobro,ubicacion=:ubicacion,
                      biblioteca=:biblioteca WHERE idplan =:id";

            // prepare query
            $stmt   = $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idplan);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":costo", $this->costo);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":mesesactivo", $this->mesesActivo);
            $stmt->bindParam(":tiempo_cobro", $this->descPlan);
            $stmt->bindParam(":aplicacion_plan", $this->tipoPlan);
            $stmt->bindParam(":especialidades", $this->numEspecialidad);
            $stmt->bindParam(":ubicacion", $this->ubicacion);
            $stmt->bindParam(":biblioteca", $this->biblioteca);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    // get all firmas
    public function get_all_firmas(){
        try{
            $query 	= "SELECT idfirmas, nombre, estado, logo FROM firma WHERE estado = '1';";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


}
?>