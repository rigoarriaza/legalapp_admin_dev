<?php
class Empresa {
	public $idempresa;
	public $nombre_empresa;
	public $direccion;
	public $departamento;
	public $municipio;
	public $telefono1;
	public $telefono2;
	public $nit;
	public $numero_registro;
	public $giro;
	public $tipo_documento;
	public $idusuario;
	


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_empresa() { 
        try {

            $query = "SELECT idempresa,nombre_empresa,direccion,departamento,municipio,telefono1,telefono2,nit,numero_registro,giro,tipo_documento FROM empresa
						WHERE idempresa= :idempresa";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idempresa", $this->idempresa);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	
	// get user
	public function get_all_empresa(){
		try{
			$query 	= "SELECT idempresa,nombre_empresa,direccion,departamento,municipio,telefono1,telefono2,nit,numero_registro,giro,tipo_documento FROM empresa";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_empresa(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO empresa (nombre_empresa,direccion,departamento,municipio,telefono1,telefono2,nit,numero_registro,giro,tipo_documento,idusuario) 
						VALUES (:nombre_empresa,:direccion,:departamento,:municipio,:telefono1,:telefono2,:nit,:numero_registro,:giro,:tipo_documento,:idusuario);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre_empresa", $this->nombre_empresa);
			$stmt->bindParam(":direccion", $this->direccion);
			$stmt->bindParam(":departamento", $this->departamento);
			$stmt->bindParam(":municipio", $this->municipio);
			$stmt->bindParam(":telefono1", $this->telefono1);
			$stmt->bindParam(":telefono2", $this->telefono2);
			$stmt->bindParam(":nit", $this->nit);
			$stmt->bindParam(":numero_registro", $this->numero_registro);
			$stmt->bindParam(":giro", $this->giro);
			$stmt->bindParam(":tipo_documento", $this->tipo_documento);
			$stmt->bindParam(":idusuario", $this->idusuario);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_verificacion_empresa(){
        try {

            $query = "SELECT *
                    FROM empresa
                    WHERE nombre_empresa=:nombre_empresa";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":nombre_empresa", $this->nombre_empresa);
            $stmt->execute(); 
			if ( $stmt->rowCount() > 0 ) {
				return true;
			}else{
				return false;
			}
        } catch (PDOException $e) {
			return false;
        }
    }
	
	public function modify_empresa(){
		try
		{
			// query to insert record
			$query = "UPDATE empresa SET nombre_empresa=:nombre_empresa,direccion=:direccion,departamento=:departamento,municipio=:municipio,telefono1=:telefono1,telefono2=:telefono2,nit=:nit,
						numero_registro=:numero_registro,giro=:giro,tipo_documento=:tipo_documento WHERE idempresa =:id";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			$pass	= sha1($this->password);
			// bind values
			$stmt->bindParam("id", $this->idempresa);
			$stmt->bindParam("nombre_empresa", $this->nombre_empresa);
			$stmt->bindParam("direccion", $this->direccion);
			$stmt->bindParam("departamento", $this->departamento);
			$stmt->bindParam("municipio", $this->municipio);
			$stmt->bindParam("telefono1", $this->telefono1);
			$stmt->bindParam("telefono2", $this->telefono2);
			$stmt->bindParam("nit", $this->nit);
			$stmt->bindParam("numero_registro", $this->numero_registro);
			$stmt->bindParam("giro", $this->giro);
			$stmt->bindParam("tipo_documento", $this->tipo_documento);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
}
?>