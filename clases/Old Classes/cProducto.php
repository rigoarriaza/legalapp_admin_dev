<?php
class Producto {
	
	public $idproducto;
	public $nombre;
	public $codigo;
	public $cantidad;
	public $estado;
	public $password;
	


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	
    public function get_product() { 
        try {

            $query = "SELECT idproducto,nombre,codigo,cantidad,estado FROM producto WHERE idproducto= :idproducto";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idproducto", $this->idproducto);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	
	// get user
	public function get_all_product(){
		try{
			$query 	= "SELECT idproducto,nombre,codigo,cantidad,estado FROM producto";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_product(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO producto (nombre,codigo,cantidad,estado) VALUES (:nombre,:codigo,:cantidad,:estado);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":codigo", $this->codigo);
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_verificacion_product(){
        try {

            $query = "SELECT *
                    FROM producto
                    WHERE nombre=:nombre";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->execute();
			if ( $stmt->rowCount() > 0 ) {
				return true;
			}else{
				return false;
			}
        } catch (PDOException $e) {
			return false;
        }
    }
	
	public function modify_product(){
		try
		{
			// query to insert record
			$query = "UPDATE producto SET nombre=:nombre,codigo=:codigo,estado=:estado ,cantidad=:cantidad WHERE idproducto=:idproducto;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			$pass	= sha1($this->password);
			// bind values
			$stmt->bindParam(":idproducto", $this->idproducto);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":codigo", $this->codigo);
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
}
?>