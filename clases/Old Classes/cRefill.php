<?php
class Refil {
	public $idusuario;
	public $idrefill;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


	public function getRefil_vendedor() { 
        try {

            $query = "SELECT idrefill,fecha,idusuario,comentario,estado FROM refill WHERE idusuario=:idusuario";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	 
	public function getDetalleRefil_vendedor() { 
       try {

            $query = "SELECT dd.iddetalle_refill_producto,dd.cantidad,dd.idproducto,p.nombre,dd.idrefill
						FROM detalle_refill_producto AS dd
						INNER JOIN producto AS p ON dd.idproducto=p.idproducto WHERE dd.idrefill=:idrefill;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idrefill", $this->idrefill);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function update_estado_refil(){
		try 
		{
			// query to insert record
			$query = "UPDATE refill SET estado=:estado WHERE idrefill=:idrefill;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idrefill", $this->idrefill);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>