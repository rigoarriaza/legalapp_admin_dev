<?php
class Compra {
	public $idusuario;
	public $iddevolucion;
	public $iddetalle_devolucion;
	public $idcompra;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


	public function getAllComprasUsuario() { 
        try {

            $query = "SELECT idcompra,fecha,valor,detalle,factura,idusuario,estado FROM compra WHERE idusuario=:idusuario";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	 
	public function getDetalleCompraProductoVendedor() { 
       try {

            $query = "SELECT dc.id_detalle_compra2_producto,dc.cantidad,dc.idproducto ,p.nombre
						FROM detalle_compra2_producto AS dc INNER JOIN producto AS p on p.idproducto=dc.idproducto
						WHERE dc.idcompra=:idcompra;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idcompra", $this->idcompra);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function update_estado_detalledev(){
		try 
		{
			// query to insert record
			$query = "UPDATE detalle_devolucion SET estado=:estado WHERE iddetalle_devolucion=:iddetalle_devolucion;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":iddetalle_devolucion", $this->iddetalle_devolucion);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>