<?php
class Venta {
	public $idusuario;
	public $idventa_vendedor;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


	public function getAllVentasUsuario() { 
        try {

            $query = "SELECT v.idventa_vendedor,v.idusuario,v.fecha,v.idempresa,v.total,v.estado,e.nombre_empresa 
						FROM venta_vendedor AS v INNER JOIN empresa as e ON e.idempresa = v.idempresa WHERE v.idusuario=:idusuario;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) { 
          echo $e->getMessage();
        }
    }
	 
	public function getDetalleVentaProductoVendedor() { 
       try {

            $query = "SELECT dc.id_detalle_venta_empresa,dc.cantidad,dc.idproducto ,p.nombre
						FROM detalle_venta_prod_empresa AS dc INNER JOIN producto AS p on p.idproducto=dc.idproducto
						WHERE dc.idventa_vendedor=:idventa_vendedor;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idventa_vendedor", $this->idventa_vendedor);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
}
?>