<?php
class Ruta {
	public $idempresa;
	public $idruta;
	public $idusuario;
	public $iddetalle_empresa_ruta;
	


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_usuarios() { 
        try {

            $query = "SELECT idusuario,nombre,apellido FROM usuario WHERE idrol = 2 AND estado =1;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	// get user
	public function get_all_empresa(){
		try{
			$query 	= "SELECT idempresa,nombre_empresa FROM empresa
						WHERE idempresa NOT IN(SELECT idempresa FROM detalle_empresa_ruta);";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}// get user
	
	public function get_detalle_vendedor_ruta(){
		try{
			$query 	= "SELECT der.iddetalle_empresa_ruta,e.idempresa,e.nombre_empresa FROM detalle_empresa_ruta AS der INNER JOIN empresa AS e ON der.idempresa= e.idempresa INNER JOIN ruta AS r ON r.idruta = der.idruta WHERE r.idusuario=:idusuario";			
			$stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idusuario", $this->idusuario);
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
			echo $e->getMessage();
        }
	}
	
	public function create_rutas(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO ruta (idusuario, nombre_ruta, estado) VALUES (:idusuario,'nombre',1);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idusuario", $this->idusuario);

			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function create_empresa_ruta(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO detalle_empresa_ruta (idempresa,idruta) VALUES(:idempresa,:idruta);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idempresa", $this->idempresa);
			$stmt->bindParam(":idruta", $this->idruta);

			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function delete_empresa_ruta(){
		try
		{
			// query to insert record
			$query = "DELETE FROM detalle_empresa_ruta WHERE iddetalle_empresa_ruta=:iddetalle_empresa_ruta;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam("iddetalle_empresa_ruta", $this->iddetalle_empresa_ruta);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function verificar_ruta_usuario() { 
        try {

            $query = "SELECT idruta FROM ruta WHERE idusuario = :idusuario;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam("idusuario", $this->idusuario);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['idruta'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function verificar_ruta_empresa() { 
        try {
            $query = "SELECT iddetalle_empresa_ruta FROM detalle_empresa_ruta WHERE idempresa = :idempresa AND idruta = :idruta;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam("idempresa", $this->idempresa);
			$stmt->bindParam("idruta", $this->idruta);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['iddetalle_empresa_ruta'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

}
?>