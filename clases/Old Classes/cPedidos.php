<?php
class Pedidos {
	public $idinventario;
	public $cantidad;
	public $nombre;
	public $idproducto;
	public $fecha;
	public $idusuario;
	public $idasignacion_vendedor;
	public $idventa;
	public $estado;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_peticiones_venta() { 
        try {

            $query = "SELECT v.idventa,v.fecha,v.tipo_documento,v.estado,v.idusuario,v.estado,v.pago, u.nombre, u.apellido, u.telefono, u.correo_electronico
						FROM venta AS v
						INNER JOIN usuario AS u
						ON u.idusuario = v.idusuario ORDER BY v.fecha ASC;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	public function get_peticiones_venta2() { 
        try {

            $query = "SELECT v.idventa,v.fecha,v.tipo_documento,v.estado,v.idusuario,v.estado,v.pago, u.nombre, u.apellido, u.telefono, u.correo_electronico
						FROM venta AS v
						INNER JOIN usuario AS u
						ON u.idusuario = v.idusuario WHERE v.fecha BETWEEN :fecha1 AND :fecha2 ORDER BY v.fecha ASC;";
            $stmt = $this->conn->prepare( $query );
			$fecha1  = $this->fecha. ' 00:00:00';
			$fecha2  = $this->fecha. ' 23:59:59';
			$stmt->bindParam(":fecha1", $fecha1);
			$stmt->bindParam(":fecha2", $fecha2);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function get_productos() { 
        try {

            $query = "SELECT idproducto,cantidad,nombre,codigo FROM producto;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function get_detalle_venta(){
		try{
			$query 	= "SELECT dcp.iddetalle_compra_producto,dcp.cantidad,dcp.idventa,dcp.idproducto,p.nombre,p.precio,p.tamanio
						FROM detalle_compra_producto AS dcp INNER JOIN producto AS p 
						ON p.idproducto=dcp.idproducto WHERE idventa=:idventa;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idventa", $this->idventa);
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
			echo $e->getMessage();
        }
	}
	
	public function update_estado_pedido(){
		try 
		{
			// query to insert record
			$query = "UPDATE venta SET estado=:estado WHERE idventa=:idventa;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idventa", $this->idventa);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	public function update_estado_pedido_pago(){
		try 
		{
			// query to insert record
			$query = "UPDATE venta SET pago=1 WHERE idventa=:idventa;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idventa", $this->idventa);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	
	public function get_detalle_inventario_fecha(){
		try{
			$query 	= "SELECT i.idproducto, i.cantidad, p.nombre, i.precio
						FROM inventario AS i
						INNER JOIN producto AS p ON p.idproducto = i.idproducto WHERE i.fecha=:fecha";			
			$stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":fecha", $this->fecha);
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
			echo $e->getMessage();
        }
	}
	
	public function create_inventario(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO inventario (cantidad,fecha,idproducto,precio) VALUES (:cantidad,:fecha,:idproducto,:precio);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":fecha", $this->fecha);
			$stmt->bindParam(":idproducto", $this->idproducto);
			$stmt->bindParam(":precio", $this->precio);
			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function update_stock_total(){
		try 
		{
			// query to insert record
			$query = "UPDATE producto SET cantidad = cantidad+:cantidad WHERE idproducto=:idproducto;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":idproducto", $this->idproducto);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function update_stock_total2(){
		try 
		{
			// query to insert record
			$query = "UPDATE producto SET cantidad = cantidad-:cantidad WHERE idproducto=:idproducto;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":idproducto", $this->idproducto);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function update_stock_inventario(){
		try 
		{
			// query to insert record
			$query = "UPDATE inventario SET cantidad = cantidad+:cantidad WHERE idinventario=:idinventario;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":idinventario", $this->idinventario);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}

	public function verificar_inventario_fecha() { 
        try {

            $query = "SELECT idinventario FROM inventario WHERE fecha = :fecha AND idproducto =:idproducto;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam("fecha", $this->fecha);
			$stmt->bindParam("idproducto", $this->idproducto);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['idinventario'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function verificar_inventario_fecha2() { 
        try {

            $query = "SELECT cantidad FROM inventario WHERE fecha = :fecha AND idproducto =:idproducto;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam("fecha", $this->fecha);
			$stmt->bindParam("idproducto", $this->idproducto);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['cantidad'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function get_productos_cantidad() { 
        try {

            $query = "SELECT cantidad FROM producto WHERE idproducto=:idproducto;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idproducto", $this->idproducto);
			 $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['cantidad'];
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function get_inventario_vendedor() { 
        try {

            $query = "SELECT dvp.cantidad
						FROM asignacion_vendedor AS a INNER JOIN detalle_vendedor_producto AS dvp ON a.idasignacion_vendedor=dvp.idasignacion_vendedor
						WHERE a.idusuario=:idusuario AND a.fecha=:fecha AND dvp.idproducto=:idproducto;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam("fecha", $this->fecha);
			$stmt->bindParam("idproducto", $this->idproducto);
			$stmt->bindParam("idusuario", $this->idusuario);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['cantidad'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function get_asignacion_vendedor() { 
        try {

            $query = "SELECT idasignacion_vendedor FROM asignacion_vendedor WHERE idusuario=:idusuario AND fecha=:fecha;;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam("fecha", $this->fecha);
			$stmt->bindParam("idusuario", $this->idusuario);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['idasignacion_vendedor'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function create_asignacion_vendedor(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO asignacion_vendedor (fecha,idusuario,estado) VALUES(:fecha,:idusuario,:estado);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idusuario", $this->idusuario);
			$stmt->bindParam(":fecha", $this->fecha);
			$stmt->bindParam(":estado", $this->estado);
			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function create_asignacion_detalle(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO detalle_vendedor_producto (cantidad,idproducto,idasignacion_vendedor,precio) VALUES(:cantidad,:idproducto,:idasignacion_vendedor,:precio);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":idasignacion_vendedor", $this->idasignacion_vendedor);
			$stmt->bindParam(":idproducto", $this->idproducto);
			$stmt->bindParam(":precio", $this->precio);
			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_productos_cantidad_v() { 
        try {

            $query = "SELECT cantidad FROM detalle_vendedor_producto WHERE idproducto=:idproducto AND idasignacion_vendedor=:idasignacion_vendedor;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idproducto", $this->idproducto);
			$stmt->bindParam(":idasignacion_vendedor", $this->idasignacion_vendedor);
			 $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['cantidad'];
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	public function update_stock_producto_vendedor(){
		try 
		{
			// query to insert record
			$query = "UPDATE detalle_vendedor_producto SET cantidad = cantidad+:cantidad WHERE idasignacion_vendedor=:idasignacion_vendedor AND idproducto=:idproducto;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":cantidad", $this->cantidad);
			$stmt->bindParam(":idproducto", $this->idproducto);
			$stmt->bindParam(":idasignacion_vendedor", $this->idasignacion_vendedor);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_verificacion_detalle_prod_ven() { 
        try {

            $query = "SELECT iddetalle_vendedor_producto FROM detalle_vendedor_producto WHERE idproducto=:idproducto AND idasignacion_vendedor=:idasignacion_vendedor;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idproducto", $this->idproducto);
			$stmt->bindParam(":idasignacion_vendedor", $this->idasignacion_vendedor);
			 $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row['iddetalle_vendedor_producto'];
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
}
?>