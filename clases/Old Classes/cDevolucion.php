<?php
class Devolucion {
	public $idusuario;
	public $iddevolucion;
	public $iddetalle_devolucion;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


	public function getDevoluciones_vendedor() { 
        try {

            $query = "SELECT 
						d.iddevolucion,d.fecha,d.estado,d.idasignacion_vendedor
						FROM devolucion AS d INNER JOIN asignacion_vendedor AS a ON a.idasignacion_vendedor = d.idasignacion_vendedor
						WHERE a.idusuario=:idusuario;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	 
	public function getDetalleDevolucion_vendedor() { 
       try {

            $query = "SELECT dd.iddetalle_devolucion,dd.cantidad,dd.estado,dd.aprobacion,dd.idproducto,p.nombre,dd.iddevolucion
						FROM detalle_devolucion AS dd
						INNER JOIN producto AS p ON dd.idproducto=p.idproducto WHERE dd.iddevolucion=:iddevolucion;";
            $stmt = $this->conn->prepare( $query );
			$stmt->bindParam(":iddevolucion", $this->iddevolucion);
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function update_estado_detalledev(){
		try 
		{
			// query to insert record
			$query = "UPDATE detalle_devolucion SET estado=:estado WHERE iddetalle_devolucion=:iddetalle_devolucion;";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":iddetalle_devolucion", $this->iddetalle_devolucion);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>