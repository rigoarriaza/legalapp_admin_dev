<?php
class Pasantia {

    public $idpasantia;

    public $nombre;
    public $descripcion;
    public $estado;
    public $idusuarioweb;
    public $firma;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_pasa() {
        try {

            $query = "SELECT p.idpasantia, p.nombre, p.descripcion, p.estado, p.creacion,p.idusuarioweb, p.idfirma, f.nombre AS 'nombreFirma' 
                      FROM pasantia AS p 
                      INNER JOIN firma f on p.idfirma = f.idfirmas
                      WHERE p.idpasantia = :idpasantia;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idpasantia", $this->idpasantia);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function get_usuarioweb($id) {
        try {

            $query = "SELECT CONCAT(nombre,' ',apellido) AS nombreWeb 
                      FROM usuarioweb WHERE idusuarioweb = :idusuarioweb;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuarioweb", $id);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    // get user
    public function get_all_pasas(){
        try{
            $query 	= "SELECT p.idpasantia, p.nombre, p.descripcion, p.estado, p.creacion, p.idusuarioweb, p.idfirma, f.nombre AS 'nombreFirma'
                       FROM pasantia AS p 
                       INNER JOIN firma f on p.idfirma = f.idfirmas;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function create_pasa(){
        try
        {
            // query to insert record
            $query = "INSERT INTO pasantia (nombre,descripcion,estado,idusuarioweb,idfirma) 
						VALUES (:nombre,:descripcion,:estado,:idusuarioweb,:idfirma);";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":descripcion", $this->descripcion);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":idusuarioweb", $this->idusuarioweb);
            $stmt->bindParam(":idfirma", $this->firma);

            // execute query
            if($stmt->execute()){
                return $this->conn->lastInsertId();
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }


    public function modify_pasa(){
        try
        {
            // query to insert record
            $query = "UPDATE pasantia SET nombre=:nombre,descripcion=:descripcion,
						estado=:estado, idusuarioweb = :idusuarioweb, idfirma=:idfirma WHERE idpasantia =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idpasantia);
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":descripcion", $this->descripcion);
            $stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":idusuarioweb", $this->idusuarioweb);
            $stmt->bindParam(":idfirma", $this->firma);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}
?>