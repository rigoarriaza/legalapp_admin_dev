<?php
class Database{
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "legal_app_dev";
    //private $username = "go_parts_bd";
    private $username = "legalappuser";
    //private $password = "Goparts*2016";
    private $password = "legalapp123";
    public $conn;

    // get the database connection
    public function getConnection(){

        $this->conn = null;

        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
?>