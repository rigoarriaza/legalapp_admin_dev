<?php
class Examen {
	
	public $idexamenpro;
	public $idespecialidad;
	public $icono;
	public $nombre;
	public $descripcion;
	public $estado;
	
	public $idpreguntasexamen;
	public $pregunta;
	public $respuesta;
	public $orden;
	

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	
    public function get_one_examen() { 
        try {

            $query = "SELECT e.idexamenpro,e.nombre,e.descripcion,e.creacion,e.idespecialidad,e.icono,e.estado
			FROM examenpro AS e  WHERE e.idexamenpro=:idexamenpro;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idexamenpro", $this->idexamenpro);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function get_all_examen(){
		try{
			$query 	= "SELECT e.idexamenpro,e.nombre,e.descripcion,e.creacion,e.idespecialidad,e.icono ,e.estado, es.nombre AS especialidad
						FROM examenpro AS e INNER JOIN especialidad AS es ON es.idespecialidad=e.idespecialidad;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	public function get_all_especialidades(){
		try{
			$query 	= "SELECT idespecialidad,nombre,descripcion,estado FROM especialidad WHERE estado =1;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_examen(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO examenpro (nombre,descripcion,idespecialidad,icono,estado) 
						VALUES (:nombre,:descripcion,:idespecialidad,:icono,:estado);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":descripcion", $this->descripcion);
			$stmt->bindParam(":idespecialidad", $this->idespecialidad);
			$stmt->bindParam(":icono", $this->icono);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function modify_examen(){
		try
		{
			// query to insert record
			$query = "UPDATE examenpro SET nombre=:nombre,descripcion=:descripcion,idespecialidad=:idespecialidad,icono=:icono,estado=:estado 
						WHERE idexamenpro=:idexamenpro";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idexamenpro", $this->idexamenpro);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":descripcion", $this->descripcion);
			$stmt->bindParam(":idespecialidad", $this->idespecialidad);
			$stmt->bindParam(":icono", $this->icono);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_one_pregunta(){
		try {

            $query = "SELECT idpreguntasexamen,pregunta,respuesta,estado,idexamenpro FROM preguntasexamen 
						WHERE idpreguntasexamen=:idpreguntasexamen;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idpreguntasexamen", $this->idpreguntasexamen);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	public function get_all_preguntas(){
		try {

            $query = "SELECT idpreguntasexamen,pregunta,respuesta,estado,idexamenpro FROM preguntasexamen 
						WHERE idexamenpro=:idexamenpro ORDER BY orden ASC;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idexamenpro", $this->idexamenpro);
            $stmt->execute();
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
	}

	public function create_pregunta(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO preguntasexamen (pregunta,respuesta,estado,idexamenpro) 
						VALUES (:pregunta,:respuesta,:estado,:idexamenpro);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":pregunta", $this->pregunta);
			$stmt->bindParam(":respuesta", $this->respuesta);
			$stmt->bindParam(":idexamenpro", $this->idexamenpro);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function modify_pregunta(){
		try
		{
			// query to insert record
			$query = "UPDATE preguntasexamen SET pregunta=:pregunta,respuesta=:respuesta,estado=:estado ,idexamenpro=:idexamenpro 
						WHERE idpreguntasexamen=:idpreguntasexamen";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idpreguntasexamen", $this->idpreguntasexamen);
			$stmt->bindParam(":pregunta", $this->pregunta);
			$stmt->bindParam(":respuesta", $this->respuesta);
			$stmt->bindParam(":estado", $this->estado);
			$stmt->bindParam(":idexamenpro", $this->idexamenpro);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	public function setPositionPregunta(){
		try
		{
			// query to insert record
			$query = "UPDATE preguntasexamen SET orden=:orden 
						WHERE idpreguntasexamen=:idpreguntasexamen";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idpreguntasexamen", $this->idpreguntasexamen);
			$stmt->bindParam(":orden", $this->orden);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>