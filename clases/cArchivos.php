<?php
class Archivo {
	
	public $idarchivo;
	public $idespecialidad;
	public $icono;
	public $nombre;
	public $descripcion;
	public $estado;
	
	public $url;
	public $idtipoarchivo;
	public $orden;
	

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	
    public function get_one_archivo() { 
        try {

            $query = "SELECT idarchivo,nombre,url,estado,idespecialidad,idtipoarchivo FROM archivo WHERE idarchivo=:idarchivo;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idarchivo", $this->idarchivo);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function get_all_archivo(){
		try{
			$query 	= "SELECT a.idarchivo,a.nombre,a.url,a.estado,a.idespecialidad,a.idtipoarchivo,es.nombre AS especialidad,ta.nombre AS tipoarchivo
						FROM archivo AS a INNER JOIN especialidad AS es ON es.idespecialidad=a.idespecialidad 
						INNER JOIN tipoarchivo AS ta ON ta.idtipoarchivo= a.idtipoarchivo;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
		
	public function create_archivo(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO archivo (nombre,url,estado,idespecialidad,idtipoarchivo) 
						VALUES (:nombre,:url,:estado,:idespecialidad,:idtipoarchivo);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":idespecialidad", $this->idespecialidad);
			$stmt->bindParam(":idtipoarchivo", $this->idtipoarchivo);
			$stmt->bindParam(":url", $this->url);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function modify_archivo(){
		try
		{
			// query to insert record
			$query = "UPDATE archivo SET nombre=:nombre,url=:url,idespecialidad=:idespecialidad,idtipoarchivo=:idtipoarchivo,estado=:estado 
						WHERE idarchivo=:idarchivo";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idarchivo", $this->idarchivo);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":url", $this->url);
			$stmt->bindParam(":idespecialidad", $this->idespecialidad);
			$stmt->bindParam(":idtipoarchivo", $this->idtipoarchivo);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function get_all_tiposdearchivo(){
		try{
			$query 	= "SELECT idtipoarchivo,nombre,estado FROM tipoarchivo WHERE estado =1;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	public function get_one_tipoarchivo() { 
        try {

            $query = "SELECT idtipoarchivo,nombre,imagen,estado FROM tipoarchivo WHERE idtipoarchivo=:idtipoarchivo;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idtipoarchivo", $this->idtipoarchivo);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

	public function get_all_tipoarchivo(){
		try{
			$query 	= "SELECT idtipoarchivo,nombre,imagen,estado FROM tipoarchivo;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_tipoarchivo(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO tipoarchivo (nombre,imagen,estado) 
						VALUES (:nombre,:icono,:estado);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":icono", $this->icono);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){ 
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	public function modify_tipoarchivo(){
		try
		{
			// query to insert record
			$query = "UPDATE tipoarchivo SET nombre=:nombre,imagen=:icono,estado=:estado 
						WHERE idtipoarchivo=:idtipoarchivo";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":idtipoarchivo", $this->idtipoarchivo);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":icono", $this->icono);
			$stmt->bindParam(":estado", $this->estado);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>