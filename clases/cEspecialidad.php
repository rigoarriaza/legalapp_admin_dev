<?php
class Especialidad {
	
	public $idespecialidad;
	
	public $nombre;
	public $descripcion;
	public $estado;
	public $imagen;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

	
    public function get_espe() { 
        try {

            $query = "SELECT idespecialidad, nombre, descripcion, estado, imagen FROM especialidad WHERE idespecialidad = :idespecialidad;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idespecialidad", $this->idespecialidad);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
			return $row;

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }
	
	
	// get user
	public function get_all_espe(){
		try{
			$query 	= "SELECT idespecialidad, nombre, descripcion, estado FROM especialidad;";			
			$stmt = $this->conn->prepare( $query );
			$stmt->execute();
			$results = $stmt->fetchAll( PDO::FETCH_ASSOC );
			return $results;
		} catch (PDOException $e) {
          echo $e->getMessage();
        }
	}
	
	public function create_espe(){
		try 
		{
			// query to insert record
			$query = "INSERT INTO especialidad (nombre,descripcion,estado,imagen) 
						VALUES (:nombre,:descripcion,:estado,:imagen);";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":descripcion", $this->descripcion);
			$stmt->bindParam(":estado", $this->estado);
			$stmt->bindParam(":imagen", $this->imagen);

			// execute query
			if($stmt->execute()){ 
				return $this->conn->lastInsertId();
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
	
	public function modify_espe(){
		try
		{
			// query to insert record
			$query = "UPDATE especialidad SET nombre=:nombre,descripcion=:descripcion,
						estado=:estado,imagen=:imagen WHERE idespecialidad =:id";

			// prepare query
			$stmt 	= $this->conn->prepare($query);
			// bind values
			$stmt->bindParam(":id", $this->idespecialidad);
			$stmt->bindParam(":nombre", $this->nombre);
			$stmt->bindParam(":descripcion", $this->descripcion);
			$stmt->bindParam(":estado", $this->estado);
            $stmt->bindParam(":imagen", $this->imagen);

			// execute query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}
	}
	
}
?>