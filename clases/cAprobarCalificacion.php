<?php
class AprobarC {

    public $idcalificacion_pregunta_express;

    public $idrespuestapregunta;
    public $calificacion;
    public $comentario;
    public $estado;
    public $creacion;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }


    public function get_cali() {
        try {

            $query = "SELECT e.idcalificacion_pregunta_express, e.calificacion, e.comentario, e.estado, e.creacion,
                      rp.respuestacontenido,rp.creacion AS 'creacionRespuesta', p.titulo, p.contenido, p.creacion AS 'creacionPregunta',
                      CONCAT(up.nombre,' ',up.apellido) AS 'nombreProfesional'
                      FROM legal_app.calificacion_pregunta_express as e
                      INNER JOIN legal_app.respuestapregunta AS rp
                      ON e.idrespuestapregunta = rp.idrespuestapregunta
                      INNER JOIN legal_app.pregunta AS p 
                      ON rp.idpregunta = p.idpregunta
                      INNER JOIN legal_app.usuarioprofesional as up
                      ON rp.idusuarioprofesional = up.idusuarioprofesional
                      WHERE e.idcalificacion_pregunta_express = :idcalificacion_pregunta_express;";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idcalificacion_pregunta_express", $this->idcalificacion_pregunta_express);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    // get user
    public function get_all_cali(){
        try{
            $query 	= "SELECT idcalificacion_pregunta_express, idrespuestapregunta, 
                        calificacion, comentario, estado, creacion
                        FROM calificacion_pregunta_express ORDER BY estado;";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }



    public function modify_estadoCali(){
        try
        {
            // query to insert record
            $query = "UPDATE legal_app.calificacion_pregunta_express SET estado=:estado WHERE idcalificacion_pregunta_express =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idcalificacion_pregunta_express);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

}
?>