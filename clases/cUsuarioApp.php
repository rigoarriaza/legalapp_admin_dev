<?php
class UsuarioApp {
    public $idusuario;
    public $estado;



    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }



    public function get_user() {
        try {

            $query = "SELECT u.idusuarioapp,u.nombre,u.apellido,u.creacion,u.correo, u.idestadousuario as idestado, 
                        eu.nombre as estado, u.carnet,u.foto,u.saldo,u.celular
            			FROM usuarioapp as u 
            			inner join estadousuario as eu on eu.idestadousuario = u.idestadousuario
						WHERE u.idusuarioapp= :idusuario";
            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam(":idusuario", $this->idusuario);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    // get user
    public function get_all_user(){
        try{
            $query 	= "SELECT u.idusuarioapp,u.nombre,u.apellido,u.creacion,u.correo, u.idestadousuario as idestado, 
                        eu.nombre as estado, u.carnet,u.foto,u.saldo,u.celular 
                        FROM usuarioapp as u
                        INNER JOIN estadousuario as eu on u.idestadousuario=eu.idestadousuario";
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function modify_user(){
        try
        {
            // query to insert record
            $query = "UPDATE usuarioapp SET idestadousuario=:estado WHERE idusuarioapp =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuario);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }
    public function modify_user2(){
        try
        {
            // query to insert record
            $query = "UPDATE usuarioweb SET nombre=:nombre,password=:password
						,idrolweb=:idrol,apellido=:apellido,idestadousuario=:estado WHERE idusuarioweb =:id";

            // prepare query
            $stmt 	= $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":id", $this->idusuario);
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":password", $this->password);
            $stmt->bindParam(":apellido", $this->apellido);
            $stmt->bindParam(":idrol", $this->idrol);
            $stmt->bindParam(":estado", $this->estado);

            // execute query
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }
    }

    public function getEstadosUsuario() {
        try
        {
            // query to insert record
            $query = "SELECT idestadousuario, nombre FROM estadousuario;";

            // prepare query
            $stmt = $this->conn->prepare($query);
            // execute query
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch(PDOException $ex) {

            echo $ex->getMessage();
            return false;
        }

    }

    public function getEstados() {
        try
        {
            // query to insert record
            $query = "SELECT idestadousuario, nombre FROM estadousuario;";

            // prepare query
            $stmt = $this->conn->prepare($query);
            // execute query
            $stmt->execute();
            $results = $stmt->fetchAll( PDO::FETCH_ASSOC );
            return $results;
        } catch(PDOException $ex) {

            echo $ex->getMessage();
            return false;
        }

    }
}
?>