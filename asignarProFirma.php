<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cPlan.php';
include_once 'clases/cUsuario.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);
$oPlan             	= new Plan($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//TODO: Add log when a user change something or add something in here

//Enter modifications
$firmas 	= $oPlan->get_all_firmasWaPlan();
//$proAvail 	= $oPlan->get_all_proAvail();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Asignacion de pros a firmas <?=date('Y-m-d')?></title>

    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 320px;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Regresar" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Plan</a> <a href="#" class="current">Asignar Profesional a una Firma</a> </div>
        <h1>Asignación de Profesionales a Firmas</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Asignar Profesional a firma</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formProFirma" id="formProFirma" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nprofirma"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <p id="messageFirma" name="messagefirma" style="display: block; font-style: italic; font-size: smaller">
                                    <b>Nota</b>: Es importante que recuerde que en el selector de firmas solo aparecen aquellas que ya tienen un plan asignado,
                                    puesto que lo contrario impide asignarle un plan a un usuario, en base al plan comprado por la firma de abogados.
                                    <br>
                                    <br>
                                    Asimismo, la lista de profesionales en el selector corresponde a aquellos que aun no pertencen a un firma,
                                    si se desea cambiar a un profesional de firma primero tiene que eliminarse de la firma actual para colocarlo en este.
                                    Recuerde que puede encontrar a que firma pertenece un usuario yendo a la opción 'Usuario profesional' de este sistema.
                                </p>
                                <br>
                                <label class="control-label">Seleccione Firma</label>
                                <div class="controls">
                                    <select id="firma" name="firma">
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        if($firmas){
                                            forEach($firmas as $val1){
                                                echo '<option value="'.$val1['idfirmas'].'">'.$val1['nombre'].'</option>';
                                            }
                                        }else{
                                            echo '<option value="--">NO HAY DATOS</option>';
                                        }

                                        ?>
                                    </select>
                                    <p id="messageFirma" name="messagefirma" style="display: none; font-style: italic; font-size: smaller"></p>
                                </div>
                            </div>
                            <div class="control-group conceal">
                                <label class="control-label">Seleccione Profesional </label>
                                <div class="controls">
                                    <select id="pro" name="pro">
                                        <option value="">--Seleccione--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                        <h5>Usuarios para Firma Seleccionada</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="table1">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Celular</th>
                                <th>Verificación</th>
                                <th>Estado</th>
                                <th>Fecha de Creación</th>
                                <th>Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>


<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    function  getAvailPro() {
        var firma           = $("#firma").val();

        $.post("json/getAvailPro.php",{
            id:firma
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){

                    data = JSON.parse(data);

                    data.forEach(function(e){
                        var id 	    = e['idusuarioprofesional'];
                        var name    = e['fullName'];
                        var mail    = e['correo'];

                        var data = {
                            id: id,
                            text: name+'('+mail+')'
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $('#pro').append(newOption);

                    });
                }
            }
        });


    }

    $(document).ready(function(){

        //$('.conceal').css('display','none');


        //add the option to load the plan right on nthe change of firm

        $('#firma').on('change', function() {

            getProfesionales(true);
            getAvailPro();
        });

        $('#plan').on('change', function() {
            var fechaIni = $('#fecInicio').val();
            if(fechaIni !== ''){
                calculateDate();
            }
        });

        // ADD active state to current option
        var currentSel = $('#12');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#proAccor').show();

        getProfesionales(false);


        table = $('#table1').dataTable({
            "sPaginationType": "full_numbers",
            "sDom": '<""l>t<"F"fp>',
            "aaSorting": [],
            "buttons": [
                'csv', 'excel'
            ]
        });



        //Submitting

        $(document).on('submit', '#formProFirma', function() {
            $.post("action/createProFirma.php", $(this).serialize())
                .done(function(data) {
                    //console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();

                    }
                    getProfesionales(true);
                });
            return false;
        });
    });


    function getProfesionales(v){
        if(v){
            table.fnClearTable();
        }
        var firma           = $("#firma").val();

        $.post("json/getCurrentPro.php",{
            id:firma
        }, function (data, status) {
            if (status == 'success') {
                data = data.replace(/^\s*|\s*$/g, "");
                if(data!='ndata' && data!='error'){
                    console.log('Data de Current Users: ',data);
                    data = JSON.parse(data);
                    imprimirtabla(data);
                }
            }
        });
    }

    function imprimirtabla(data){
        data.forEach(function(e){
            var id 	= e['idusuarioprofesional'];
            var txt ='<a onclick="delUser('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Eliminar</a>';
            var estadoVerificacion = e['estadoverificacion'];

            if(estadoVerificacion === '1'){
                estadoVerificacion = "Verificado";
            }else {
                estadoVerificacion = "No Verificado";
            }
            var estadoActual = e['idestadousuario'];
            if(estadoActual === '1'){
                estadoActual = "Pendiente";
            }else if(estadoActual === '2'){
                estadoActual = "Activo";
            }else {
                estadoActual = "Inactivo";
            }

            table.fnAddData([
                e['nombre']+' '+e['apellido'],
                e['correo'],
                e['celular'],
                estadoVerificacion,
                estadoActual,
                e['creacion'],
                txt
            ]);
        });

        table.api().buttons().container()
            .insertBefore( '#table1_filter' );
    }

    function delUser(id){
        //TODO: DEL the user from this firm
        if (confirm('¿Está seguro que desea eliminar este usuario de esta firma?')) {
            $.post("json/delUserPro.php",{
                id:id
            }, function (data,status){
                if(status =='success'){
                    if (data!='ndata' &&  data !=''){
                        var parsed = JSON.parse(data);
                        swal({
                            title: parsed.title,
                            text: parsed.text,
                            type: parsed.type,
                            confirmButtonText: "Ok"
                        });
                        if(parsed.type=='success'){
                            cancelaction();

                        }
                        getProfesionales(true);
                    }
                }
            });
        }
        return false;
    }

    function cancelaction(){
        $('#formProFirma').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $("#firma").select2('val', '');
        $("#pro").select2('val', '');
        $("#pro").empty();
        var data = {
            id: '',
            text: '--Seleccione--'
        };

        var newOption = new Option(data.text, data.id, false, false);
        $('#pro').append(newOption);
        getProfesionales(true);
    }
</script>
</body>
</html>
