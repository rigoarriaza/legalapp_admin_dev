<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	session_start();
	include_once 'clases/cConexion.php';
	include_once 'clases/cUsuario.php';
	$database 	= new Database();
	$db 		= $database->getConnection();
	$oUsuario   = new Usuario($db);
	
  if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
  }

  $estados 	= $oUsuario->getEstadosUsuario();
  $roles 	= $oUsuario->getRoles();

  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Usuarios Web <?=date('Y-m-d')?></title>

    <?php
require_once('headerHTML.php');
?>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Usuarios</a> <a href="#" class="current">registro</a> </div>
    <h1>Usuarios</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Usuarios</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" name="formusuarios" id="formusuarios" novalidate="novalidate">
              <input type="hidden" id="opt" name="opt" value="nuser"/>
              <input type="hidden" id="id" name="id" value=""/>
			  <div class="control-group">
                <label class="control-label">Nombre </label>
                <div class="controls">
                  <input type="text" name="nombre" id="nombre" class="inputformtext">
                </div>
              </div>
			  <div class="control-group">
                <label class="control-label">Apellido</label>
                <div class="controls">
                  <input type="text" name="apellido" id="apellido" class="inputformtext">
                </div>
              </div>
			  <div class="control-group">
                <label class="control-label">Correo electr&oacute;nico</label>
                <div class="controls">
                  <input type="text" name="mail" id="mail" class="inputformtext">
                </div>
              </div>
			  <div class="control-group">
                <label class="control-label">Contraseña</label>
                <div class="controls">
                  <input type="password" name="pass" id="pass" class="inputformtext">
                </div>
              </div>
			  <div class="control-group">
                <label class="control-label"> Confirmar contraseña</label>
                <div class="controls">
                  <input type="password" name="passconfirm" id="passconfirm" class="inputformtext">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Rol</label>
                <div class="controls">
                  <select name="rol" id="rol">
						<?php
						if($roles){
							forEach($roles as $val1){
								echo '<option value="'.$val1['idrolweb'].'">'.$val1['nombre'].'</option>';
							}
						}

						?>
				</select>
				</select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Estado</label>
                <div class="controls">
                  <select name="estado" id="estado">
						<?php
						if($estados){
							forEach($estados as $val){
								echo '<option value="'.$val['idestadousuario'].'">'.$val['nombre'].'</option>';
							}
						}

						?>
				</select>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid"><hr>
	  <div class="row-fluid">
		  <div class="span12">
			  <div class="widget-box">
				  <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
					<h5>Usuarios registrados</h5>
				  </div>
				  <div class="widget-content nopadding">
					<table class="table table-bordered data-table" id="table1">
					  <thead>
						<tr>
						  <th>ID</th>
						  <th>Nombre de usuario</th>
						  <th>Correo</th>
						  <th>Rol</th>
						  <th>Estado</th>
						  <th>Fecha de Creación</th>
						  <th>Opciones</th>
						</tr>
					  </thead>
					  <tbody>
					  </tbody>
					</table>
				  </div>
				</div>
		  </div>
	  </div>
  </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script src="js/matrix.js"></script> 

<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
var table;
$(document).ready(function(){
 // ADD active state to current option
 var currentSel = $('#2');
 if(!currentSel.hasClass('active')){
 		currentSel.addClass('active');
	}
    $('#usuarioAccor').show();

	getusuarios(false);

    table = $('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "sDom": '<""l>t<"F"fp>',
        "aaSorting": [],
        "buttons": [
            'csv', 'excel'
        ]
    });

    table.api().buttons().container()
        .insertBefore( '#table1_filter' );


	$(document).on('submit', '#formusuarios', function() {
		$.post("action/createusuario.php", $(this).serialize())
			.done(function(data) { 
				//console.log(data);
				var parsed = JSON.parse(data);
				swal({   
					title: parsed.title,   
					text: parsed.text,   
					type: parsed.type,   
					confirmButtonText: "Ok" 
				});
				if(parsed.type=='success'){
					$('#formusuarios').trigger("reset");
					$('#opt').val('nuser');
					$('#btnaction').val('Guardar');
				}
				getusuarios(true);
			});
		return false;
	});
});


function getusuarios(v){
	if(v){
		table.fnClearTable();
	}
	$.post("json/getUsuarios.php", {
	}, function (data, status) {
		if (status == 'success') {
			data = data.replace(/^\s*|\s*$/g, "");
			if(data!='ndata' && data!='error'){
				//alert(data);
				data = JSON.parse(data);
				imprimirtabla(data);
			}
		}
	});
}

function imprimirtabla(data){
	data.forEach(function(e){
		var id 	= e['idusuario'];
		var txt ='<a onclick="get_info_user('+id+')" style="padding: 0px 5px 0px 0px; cursor: pointer;">Modificar</a>';
		table.fnAddData( [ 
			id,
			e['nombre']+' '+e['apellido'],
			e['correo'],
			e['rol'],
			e['nombreEstado'],
			e['creacion'],
			txt
		]); 
	});
}

function get_info_user(id){
	$.post("json/getUsuario.php",{
		id:id
	}, function (data,status){
		if(status =='success'){
			if (data!='ndata' &&  data !=''){
				//alert(data);
				data = JSON.parse(data);
				$('#opt').val('muser');
				$('#id').val(data['idusuarioweb']);
				$('#nombre').val(data['nombre']);
				$('#apellido').val(data['apellido']);
				$('#mail').val(data['correo']);
				$('#btnaction').val('Modificar');
				var rol = data['idrolweb'];
				document.getElementById("rol").value  = rol;
				var estado = data['idestado'];
				document.getElementById("estado").value  = estado;
			}
		}
	});
	return false;
}
function cancelaction(){
	$('#formusuarios').trigger("reset");
	$('#btnaction').val('');
	$('#btnaction').val('Guardar');
	$('#opt').val('nuser');
}
</script>
</body>
</html>
