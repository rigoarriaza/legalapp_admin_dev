<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

include_once 'clases/cConexion.php';
include_once 'clases/cPlan.php';
include_once 'clases/cUsuario.php';

$database 	        = new Database();
$db 		        = $database->getConnection();
$oUsuario           = new Usuario($db);
$oPlan             	= new Plan($db);

if (!$oUsuario->is_loggedin() ) {
    header("Location: login.php");
    exit();
}

//TODO: Add log when a user change something or add something in here

//Enter modifications
$firmas 	= $oPlan->get_all_firmas();
$planes 	= $oPlan->get_all_planes2();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    require_once('headerHTML.php');
    ?>
    <style>
        .select2-container {
            width: 220px;
        }
    </style>
</head>
<body>


<?php
require_once('header.php');
?>

<?php
require_once('menu.php');
?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Regresar" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Plan</a> <a href="#" class="current">Asignar plan</a> </div>
        <h1>Asignación de Planes Para Firmas</h1>
    </div>
    <div class="container-fluid"><hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>Asignar Plan</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form class="form-horizontal" method="post" name="formasignarplan" id="formasignarplan" novalidate="novalidate">
                            <input type="hidden" id="opt" name="opt" value="nasignacion"/>
                            <input type="hidden" id="id" name="id" value=""/>
                            <div class="control-group">
                                <label class="control-label">Seleccione Firma</label>
                                <div class="controls">
                                    <select id="firma" name="firma">
                                        <option value="">--Seleccione--</option>
                                        <?php
                                        if($firmas){
                                            forEach($firmas as $val1){
                                                echo '<option value="'.$val1['idfirmas'].'">'.$val1['nombre'].'</option>';
                                            }
                                        }else{
                                            echo '<option value="--">NO HAY DATOS</option>';
                                        }

                                        ?>
                                    </select>
                                    <p id="messageFirma" name="messagefirma" style="display: none; font-style: italic; font-size: smaller"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Seleccione Plan</label>
                                <div class="controls">
                                    <select id="plan" name="plan">
                                        <?php
                                        //TODO: hacer que no use meses activo, pero un post jquery para sacar los datos que necesita
                                        if($planes){
                                            forEach($planes as $val1){
                                                echo '<option value="'.$val1['idplan'].'">'.$val1['nombreplan'].' ['.$val1['mesesactivo'].' Mes(es)]</option>';
                                            }
                                        }else{
                                            echo '<option value="">NO HAY DATOS</option>';
                                        }

                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de Compra (mm/dd/yyyy)</label>
                                <div class="controls">
                                    <div  data-date="<?=date('m/d/Y')?>" class="input-append date datepicker">
                                        <input id="fecInicio" name="fecInicio" type="text" value=""  data-date-format="mm/dd/yyyy" class="span11" readonly="readonly">
                                        <span class="add-on"><i class="icon-th"></i></span> </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Fecha de finalización (Se calcula automáticamente)</label>
                                <div class="controls">
                                    <input type="text" name="fecFin" id="fecFin" class="inputformtext" readonly="readonly">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Estado</label>
                                <div class="controls">
                                    <select name="estado" id="estado">
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="submit" id="btnaction" value="Guardar" class="btn btn-success">
                                <input type="button" onclick="cancelaction();" value="Cancelar" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
    <div id="footer" class="span12"> 2017 &copy; LegalApp.</div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.uniform.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/matrix.js"></script>
<script src="js/matrix.form_common.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/moment.js"></script>


<script src="js/sweetalert.min.js"></script>


<script type="text/javascript">
    var table;

    function  calculateDate() {
        var dateIni = $("#fecInicio").val();
        var parts =dateIni.split('/');
        //please put attention to the month (parts[0]), Javascript counts months from 0:
        // January - 0, February - 1, etc
        var mydate = new Date(parts[2],parts[0]-1,parts[1]);
        var lafecha = moment(mydate);
        var idplanTemp = $("#plan").val();
        var meses = '';
        // GET the amount of months in the plan
        $.post("json/getMes.php",{
            id:idplanTemp
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){
                    //alert(data);
                    data = JSON.parse(data);

                    meses = data['mesesactivo'];
                    meses = parseInt(meses);

                    var dateFin = lafecha.add(meses,'months');
                    dateFin = moment(dateFin).format('MM/DD/YYYY');
                    $('#fecFin').val(dateFin);
                }
            }
        });


    }

    function  getAsignacion() {
        var firma           = $("#firma").val();
        var plan            = '';
        var id              = '';
        var fecInicio       = '';
        var estado          = '';
        var creacion        = '';

        $.post("json/getAsignacion.php",{
            id:firma
        }, function (data,status){
            if(status =='success'){
                if (data!='ndata' &&  data !=''){

                    data = JSON.parse(data);

                    $("#opt").val('masignacion');

                    $("#id").val(data['idmembresiafirma']);
                    $("#plan").select2('val', data['idplan']);
                    fecInicio = data['fechacompra'];

                    //var partsInicio = fecInicio.split('-');
                    //please put attention to the month (parts[0]), Javascript counts months from 0:
                    // January - 0, February - 1, etc
                    //var fecInicio = new Date(partsInicio[0],partsInicio[1]-1,partsInicio[2]);
                    var fecInicio = new Date(fecInicio);

                    fecInicio = moment(fecInicio).format('MM/DD/YYYY');

                    $("#fecInicio").val(fecInicio);
                    creacion = data['creacion'];

                    calculateDate();

                    $("#estado").select2('val', data['estado']);

                    $('#messageFirma').css('display','block');
                    $("#messageFirma").text('Plan existente para esta firma, creado el: '+creacion);

                    $('#btnaction').val('');
                    $('#btnaction').val('Modificar');

                }else {
                    $('#btnaction').val('');
                    $('#btnaction').val('Guardar');
                    $('#id').val('');
                    $('#plan ').select2('val', '');
                    $('#fecInicio').val('');
                    $('#fecFin').val('');
                    $('#estado').val('1');
                    $('#opt').val('nasignacion');
                    $('#messageFirma').css('display','block');
                    $("#messageFirma").text('Nuevo plan para esta firma.');
                }
            }
        });


    }

    $(document).ready(function(){


        //add the option to load the plan right on nthe change of firm

        $('#firma').on('change', function() {

            getAsignacion();
        });


        $('.datepicker').on("changeDate", function(e) {
            calculateDate();
        });

        $('#plan').on('change', function() {
            var fechaIni = $('#fecInicio').val();
            if(fechaIni !== ''){
                calculateDate();
            }
        });

        // ADD active state to current option
        var currentSel = $('#8');
        if(!currentSel.hasClass('active')){
            currentSel.addClass('active');
        }
        $('#proAccor').show();


        //Submitting

        $(document).on('submit', '#formasignarplan', function() {
            $.post("action/createasignacion.php", $(this).serialize())
                .done(function(data) {
                    console.log(data);
                    var parsed = JSON.parse(data);
                    swal({
                        title: parsed.title,
                        text: parsed.text,
                        type: parsed.type,
                        confirmButtonText: "Ok"
                    });
                    if(parsed.type=='success'){
                        cancelaction();

                    }
                });
            return false;
        });
    });


    function cancelaction(){
        $('#formplan').trigger("reset");
        $('#btnaction').val('');
        $('#btnaction').val('Guardar');
        $('#id').val('');
        $("#firma").select2('val', '');
        $('#plan ').select2('val', '');
        $('#plan ').select2('val', '');
        $('#fecInicio').val('');
        $('#fecFin').val('');
        $('#estado ').select2('val', '');
        $('#opt').val('nasignacion');
        $('#messageFirma').css('display','none');
    }
</script>
</body>
</html>
